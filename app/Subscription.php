<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Plan;
class Subscription extends Model
{
    protected $table = 'subscription';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'package', 'amount', 'start_date', 'end_date', 'subscription_id', 'status','package_detail','customer_stripe_id','plan_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    protected $dates = [];

    public function packageDetail(){
        return $this->hasOne('App\Plan','id','plan_id');
    }
	public function user(){
        return $this->hasOne('App\User','id','user_id');
    }
}
