<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Role;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use SoftDeletes;
    use HasRoles;
    

    /**
     * The attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'phone', 'image', 'gender', 'dob', 'state_id', 
        'role_id', 'plan_id', 'batch_id', 'status','first_name','last_name','age',
        'school_name','school_district_no','state','fire_base_token','parent_id','stripe_id',
		'status','device_type','badge_status','country_code','stripe_acount_type','phone_verified',
		'badge_approve_by','regi_refrence'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $appends = ['dob_formated','child_age','thumb_image','user_image','full_name','registration_date'];

    public function hasRole($role)
    {
        return $this->roles()->get()->contains('label',$role);
    }

    

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

   public function getDobFormatedAttribute()
    {
        if($this->dob != "" && $this->dob){
            return \Carbon\Carbon::parse($this->dob)->format(session('setting.date_format',\config('admin.setting.date_format_on_app')));
        }
        return $this->dob;
    }

	public function getRegistrationDateAttribute()
    {
        return \Carbon\Carbon::parse($this->created_at)->format('jS M Y ');
        
    }
	
    public function setDobAttribute($value)
    {
        if($value != ""){
            if(date('Y', strtotime($value))  == 1970){
                $dates = explode('-',$value);
                return $this->attributes['dob'] = $dates[2].'-'.$dates[0].'-'.$dates[1];
            }else{
                return $this->attributes['dob'] = \Carbon\Carbon::parse($value)->format('Y-m-d');
            }
        }
    }

    public function getChildAgeAttribute()
    {
        if($this->dob != "")
        {
            $date = Carbon::createFromFormat('Y-m-d',$this->dob);
            return Carbon::parse($date)->age;
        }

        return $this->dob;
       
    }

    public function getThumbImageAttribute()
    {
		if($this->image && \File::exists(public_path()."/uploads/user/thumbnail/".$this->image)){
			$path = url("/")."/"."uploads/user/";
			return $path."thumbnail/".$this->image;
		}else{
			return "";
		}
		
        
    }

	public function getFullNameAttribute()
    {
		return $this->first_name." ".$this->last_name;
		
    }
	
    public function getUserImageAttribute()
    {
		if($this->image && \File::exists(public_path()."/uploads/user/".$this->image)){
			$path = url("/")."/"."uploads/user";
			return $path.'/'.$this->image;
		}else{
			return "";
		}
		
        $path = url("/")."/"."uploads/user";
        return ($this->image)?$path.'/'.$this->image:'';
    }

    public function getBatchIdAttribute()
    {
        if($this->status == 2)
        {
                return "N/A";       
        } 
        return $this->attributes['batch_id'];
    }
    public function parent()
    {
        return $this->hasOne('App\User', 'id', 'parent_id');
    }
	public function childs()
    {
        return $this->hasMany('App\User', 'parent_id', 'id');
    }
	public function childorders()
    {
        return $this->hasMany('App\Order', 'child_id', 'id');
	}
	public function refefile()
    {
        return $this->hasMany('App\Refefile', 'ref_field_id', 'id')->where('refe_table_field_name', 'user_id');
    }
	public function cards()
    {
        return $this->hasMany('App\Cards','stripe_id', 'stripe_id');
    }
	public function verifyUser()
    {
        return $this->hasMany('App\VerifyUser');
    }
	
	public function employee()
    {
        return $this->hasOne('App\Employee');
    }
	public function approver()
    {
        return $this->belongsTo('App\User', 'badge_approve_by');
    }
	
}

