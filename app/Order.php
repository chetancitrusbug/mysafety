<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    // public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orders';

    protected $fillable = ['parent_id','child_id','plan_id','start_date' ,'expiry_date','auth_response','order_no','amount','trans_status','next_pay','status','transaction_id','subscription_id','token','one_time_fee','one_time_charge_id','one_time_charge_ob','unsubscription_ob','plan_ob'];
    
	protected $appends = ['order_created','start_date_formated','expiry_date_formated','expiry_no_date'];
    
	
	
	
	
    public function getOrderCreatedAttribute()
    {
        if($this->created_at != ""){
            return \Carbon\Carbon::parse($this->created_at)->format(session('setting.date_format',\config('admin.setting.date_format_on_app')));
        }
		return $this->created_at;
    }
	public function getStartDateFormatedAttribute()
    {
        if($this->start_date != "" && $this->start_date){
            return \Carbon\Carbon::parse($this->start_date)->format(session('setting.date_format',\config('admin.setting.date_format_on_app')));
        }
        return $this->start_date;
    }
	public function getExpiryNoDateAttribute($value)
    {
        if(!$value || $value == ""){
            return "Lifetime";
        }
        return $value;
    }
	public function getExpiryDateFormatedAttribute()
    {
        if($this->expiry_date != "" && $this->expiry_date){
            return \Carbon\Carbon::parse($this->expiry_date)->format(session('setting.date_format',\config('admin.setting.date_format_on_app')));
        }
        return "Lifetime";
    }
	public function child()
    {
        return $this->hasOne('App\User', 'id', 'child_id');
    }
	public function plan()
    {
        return $this->hasOne('App\Plan', 'id', 'plan_id');
    }
	public function billing()
    {
        return $this->hasMany('App\Billingcycle', 'order_id', 'id');
    }
	/*public function planactual()
    {
        return json_decode($this->plan_ob);
    }*/
	public function parent()
    {
        return $this->hasOne('App\User', 'id', 'parent_id');
    }
	
	public static function getNextOrderNumber()
    {
        // Get the last created order
        $lastOrder =Order::orderBy('created_at', 'desc')->first();

        if ($lastOrder != null)
            $number = $lastOrder->id;
        else 
            $number =0;

        return 'PO' . sprintf('%05d', intval($number) + 1);
    }
}
