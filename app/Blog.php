<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model
{
    use SoftDeletes;
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    // public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'blog';

    protected $fillable = ['image','title','detail','post_date' ,'status','slug','sort_desc','type','CreatedAtbb','meta_keywords'];
    protected $appends = ['created','previous_slug','next_slug'];

    public function getPostDateAttribute($value)
    {
        if($value != ""){
            return \Carbon\Carbon::parse($value)->format(session('setting.date_format','M j, Y'));
        }
        return $value;
    }

    public function setPostDateAttribute($value)
    {
        if($value != ""){
            if(date('Y', strtotime($value))  == 1970){
                $dates = explode('-',$value);
                return $this->attributes['post_date'] = $dates[2].'-'.$dates[0].'-'.$dates[1];
            }else{
                return $this->attributes['post_date'] = \Carbon\Carbon::parse($value)->format('Y-m-d');
            }
        }
    }
    public function getCreatedAttribute()
    {
        if($this->created_at != ""){
            return \Carbon\Carbon::parse($this->created_at)->format('M j, Y');
        }
        return $this->created_at;
    }
    public function getPreviousSlugAttribute()
    {
        $previous_slug="";
        if($this->id != ""){
            $blog_previous_id = Blog::where('id', '<', $this->id)->where('type','=',$this->type)->where('status','=',1)->max('id');
            if($blog_previous_id != null)
            {
                $previous_slug=Blog::find($blog_previous_id);
                return $previous_slug->slug;
            }
            else
            {
                return $previous_slug;
            }
        
        }
        return $this->id;
    }
    public function getNextSlugAttribute()
    {
        $next_slug="";
        if($this->id != ""){
            $blog_next_id = Blog::where('id', '>', $this->id)->where('type','=',$this->type)->where('status','=',1)->min('id');
            if($blog_next_id != null)
            {
                $next_slug=Blog::find($blog_next_id);
                return $next_slug->slug;
            }
            else
            {
                return $next_slug;
            }
        
        }
        return $this->id;
    }
}
