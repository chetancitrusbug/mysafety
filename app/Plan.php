<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plan extends Model
{
    use SoftDeletes;
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    // public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'plan';

    protected $fillable = ['title','description','type','actual_amount','show_amount','stripe_product_id','status','plan_uid'];
	
	protected $appends = ['one_time_fee','features_ob'];
	
	
	public function getOneTimeFeeAttribute()
    {
        return \config('admin.one_time_fee');
        
    }
	public function getFeaturesObAttribute()
    {
        $path = [];
		if($this->features && $this->features != ""){
			$path = json_decode($this->features,true);
		}
        return $path;
    }
	public function getShowAmountAttribute($value)
    {
        $path = $value;
		if($value <= 0){
			$path = "";
		}
        return $path;
    }
}
