<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

require storage_path('package/vendor/autoload.php');

use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;

use App\User;
use App\VerifyUser;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      //  $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$users = User::all();
		
		foreach($users as $u){
			echo "<br>".$u->full_name." <pre>";
			if($u->email && $u->email !=""){
				echo " | email = ".$u->email;
				foreach($u->roles as $r){
					echo " | ".$r->name;
					if($r->name == "Child"){
						//$r->delete();
						$u->roles()->detach($r);
					}
				}
			}else{
				echo " | dob = ".$u->dob;
				foreach($u->roles as $r){
					echo " | ".$r->name;
					if($r->name == "Parent"){
						//$r->delete();
						$u->roles()->detach($r);
					}
				}
			}
			
			
		}
		
		exit;
		
		if($request->has('phone') &&  $request->has('new_phone') && $request->has('action') && $request->action == "remove_from_existing_number" ){
			User::where("phone",$request->phone)->update(['phone'=>$request->new_phone]);
		}
		if($request->has('email') &&  $request->has('new_email') && $request->has('action') && $request->action == "remove_from_existing_email" ){
			User::where("email",$request->email)->update(['email'=>$request->new_email]);
		}
		
		if($request->has('phone') && $request->has('action') && $request->action == "remove_from_verify" ){
			
			$u = User::where("phone",$request->phone)->first();
			
			if($u){
				$u->phone_verified = 0;
				$u->save();
				
				$VerifyUser = VerifyUser::where('user_id',$u->id)->orderby("updated_at","desc")->first();
				
				if($VerifyUser){
		   $serviceAccount = ServiceAccount::fromJsonFile(storage_path('files/mysafetynet-f1902-firebase-adminsdk-j8zry-8e36388209.json'));
			$firebase 		  = (new Factory)
							->withServiceAccount($serviceAccount)
							->withDatabaseUri('https://mysafetynet-f1902.firebaseio.com/')
							->create();
			$database 		= $firebase->getDatabase();
			$newPost 		  = $database
								->getReference('phone-verification/'.$u->id)
								->remove();
			echo"<pre>";
			print_r($newPost->getvalue());
			}
			
			}
		}
		echo phpinfo();
		
		
    }
}
