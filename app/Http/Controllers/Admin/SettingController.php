<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use App\Setting;
use Carbon\Carbon as Carbon;
use App\Responcetext as RT;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        view()->share('route', 'setting');
        view()->share('module', 'Settings');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        $setting = Setting::pluck('value','key')->toArray();
        view()->share('setting',$setting);

        return view('admin.setting.create');
    }
	
	public function testmail(Request $request)
    {
		if($request->has('fire_base_token') && $request->has('device_type') && $request->fire_base_token != ""){
			$msg = "testing abc";
			$msg = $request->message;
			
			
			 $message = array();
			
        

			$message['body'] = $msg;
			$message['message'] = $msg;
			$message['title'] = $msg;
        
			
			echo "---".send_notification($request->fire_base_token,[],$request->device_type,$message);
			
			
		}
		
		$to = \config('admin.mail_to_admin_cc');
		if($request->has('to')){
			$to = $request->to;
		}

		if($request->has('type') && $request->type == "contact-us"){
			$contactUs=\App\ContactUs::whereEmail("jignesh.citrusbug@gmail.com")->first();
			echo view('email.complaint', compact('contactUs'))->render(); 
		
			
			\Mail::send('email.complaint', compact('contactUs'), function ($message) use($request , $to) {
					$message->to($to)->subject("test");
			});
		}
		if($request->has('type') && $request->type == "unsubscription_success"){
			$order=\App\Order::where("trans_status",'active')->first();
			$user = $order->parent;
			$child = $order->child;
			$plan = json_decode($order->plan_ob);
			echo view('email.billing.unsubscribe-success', ['order'=>$order,'plan'=>$plan])->render(); 
			
			\Mail::send('email.billing.unsubscribe-success', ['order'=>$order,'plan'=>$plan], function ($message)  use($request , $to) {
					$message->to($to)->subject("test".$request->type);
			});
		}
		if($request->has('type') && $request->type == "subscription_upgrated"){
			$order=\App\Order::where("trans_status",'active')->first();
			$user = $order->parent;
			$child = $order->child;
			$plan = json_decode($order->plan_ob);
			echo view('email.billing.upgrade-plan-success', ['order'=>$order,'plan'=>$plan])->render(); 
			
			\Mail::send('email.billing.upgrade-plan-success', ['order'=>$order,'plan'=>$plan], function ($message) use($request , $to) {
					$message->to($to)->subject("test".$request->type);
			});
		}
		if($request->has('type') && $request->type == "request_accept_mail_to_parent"){
			$notification = RT::rtext("mail_line1_parent_your_child_connected_with_other");
			$user = \App\User::whereEmail("jignesh.citrusbug@gmail.com")->first();
			
			echo view('email.notification', ['notification'=>$notification,'user'=>$user])->render(); 
			
			\Mail::send('email.notification', ['notification'=>$notification,'user'=>$user], function ($message)  use($request , $to) {
					$message->to($to)->subject("test".$request->type);
			});
		}
		if($request->has('type') && $request->type == "parent_registration"){
			$user = \App\User::whereEmail("jignesh.citrusbug@gmail.com")->first();
			
			echo view('email.parent-registration', ['user'=>$user])->render(); 
			
			\Mail::send('email.parent-registration', ['user'=>$user], function ($message)  use($request , $to) {
					$message->to($to)->subject("test".$request->type);
			});
		}
		if($request->has('type') && $request->type == "child_registration"){
			$user = \App\User::whereEmail("jignesh.citrusbug@gmail.com")->first();
			
			echo view('email.add-child', ['user'=>$user])->render(); 
			
			\Mail::send('email.add-child', ['user'=>$user], function ($message)  use($request , $to) {
					$message->to($to)->subject("test".$request->type);
			});
		}
		
		if($request->has('type') && $request->type == "bill-generate"){
			$bill = \App\Billingcycle::whereNotNull("invoice_id")->first();
			$order=\App\Order::whereId($bill->order_id)->first();
			$plan = json_decode($order->plan_ob);
			$child = $order->child;
			$user = $order->parent;
			
			echo view('email.billing.bill-generate',compact('order','bill','plan'))->render(); 
			
			\Mail::send('email.billing.bill-generate', compact('order','bill','plan'), function ($message)  use($request , $to) {
					$message->to($to)->subject("test".$request->type);
			});
		}
		if($request->has('type') && $request->type == "need-upgrade"){
			$order=\App\Order::where("trans_status",'active')->first();
			$plan = json_decode($order->plan_ob);
			$child = $order->child;
			$user = $order->parent;
			
			echo view('email.billing.upgrade-plan',compact('order','plan'))->render(); 
			
			\Mail::send('email.billing.upgrade-plan', compact('order','plan'), function ($message)  use($request , $to) {
					$message->to($to)->subject("test".$request->type);
			});
		}
		if($request->has('type') && $request->type == "payment-in-feature"){
			$order=\App\Order::where("trans_status",'active')->first();
			$plan = json_decode($order->plan_ob);
			$child = $order->child;
			$user = $order->parent;
			
			echo view('email.billing.bill-in-feature',compact('order','plan'))->render(); 
			
			\Mail::send('email.billing.upgrade-plan', compact('order','plan'), function ($message)  use($request , $to) {
					$message->to($to)->subject("test".$request->type);
			});
		}
		if($request->has('type') && $request->type == "payment-due"){
			$order=\App\Order::where("trans_status",'active')->first();
			$plan = json_decode($order->plan_ob);
			$child = $order->child;
			$user = $order->parent;
			
			echo view('email.billing.bill-due',compact('order','plan'))->render(); 
			
			\Mail::send('email.billing.bill-due', compact('order','plan'), function ($message)  use($request , $to) {
					$message->to($to)->subject("test".$request->type);
			});
		}
		if($request->has('type') && $request->type == "payment-due-inactivated"){
			$order=\App\Order::where("trans_status",'active')->first();
			$plan = json_decode($order->plan_ob);
			$child = $order->child;
			$user = $order->parent;
			
			echo view('email.billing.bill-due-inactivated',compact('order','plan'))->render(); 
			
			\Mail::send('email.billing.upgrade-plan', compact('order','plan'), function ($message)  use($request , $to) {
					$message->to($to)->subject("test".$request->type);
			});
		}
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');

        $rules = [];
        if($input['old_image'] == ''){
            $rules['logo_image'] = ['required','mimes:png'];
            $rules['date_format'] = ['required','min:1'];
            $rules['pagination'] = 'min:1';
        }else{
            $rules['date_format'] = ['required','min:1'];
            $rules['pagination'] = 'min:1';
            $rules['logo_image'] = ['mimes:png'];
        }
		$rules['amount_per_user_approve'] = ['required'];
		
        $this->validate($request, $rules);

        foreach ($input as $key => $value) {
            $data['key'] = $key;
            $data['value'] = $value;

			if($key == 'amount_per_user_approve'){
				$setting = Setting::where('key','amount_per_user_approve')->first();
				if(!$setting){
					$setting = new Setting();
					$setting->key = $key;
				}
				$setting->value = $value;
				$setting->save();
			}
            if($key == 'logo_image'){
                if(isset($input['logo_image'])){
                    $image = $value;
                    $destinationPath = 'uploads/';
                    $data['value'] = 'my_safety_'.uniqid(time()).'-'.$image->getClientOriginalName();
                    $image->move($destinationPath,$data['value']);
                    if(isset($input['old_image']) && file_exists($destinationPath.'/'.$input['old_image'])){
                        unlink($destinationPath.'/'.$input['old_image']); //delete previously uploaded logo
                    }
                }else{
                    $data['logo_image'] = $input['old_image'];
                }
            }
            if($key != "old_image"){
                // Setting::create($data);
                Setting::where('key',$key)->update(['value'=>$data['value']]);
            }
            // echo "<pre>"; print_r($data); exit();
        }
        $setting = Setting::pluck('value','key')->toArray();
        session(['setting' => $setting]);
        return redirect('admin/setting')->with('flash_success', 'Setting updated!');
    }
}
