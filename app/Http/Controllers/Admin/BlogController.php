<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use App\Blog;
use Carbon\Carbon as Carbon;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        view()->share('route', 'blog');
        view()->share('module', 'Blogs or page');
    }

    public function index(Request $request)
    {
        return view('admin.blog.index');
    }

    public function datatable(Request $request)
    {
        $blog = Blog::select([
                    'blog.*',
                ]);
		if($request->has('dtype') && $request->dtype != ""){
			$blog->where("type",$request->dtype);
		}		
               

        return Datatables::of($blog)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('admin.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');
        
        $rules = [
            'title'=>['required','max:191'],
            'sort_desc'=>['required','max:191'],
            'image'=>['required_if:type,blog','mimes:jpg,jpeg,png'],
            'post_date'=>['required_if:type,blog'],
            'detail'=>['required'],
        ];

        $this->validate($request, $rules);

        if($input['type'] == 'blog' && (isset($input['image']) && $input['image'] != '')){
            $imageName = str_replace(' ', '_', $request->title).'_'.uniqid(time()) . '.' . $input['image']->getClientOriginalExtension();

            uploadImage($input['image'],'uploads/blog/thumbnail',$imageName,'150','150');
            uploadImage($input['image'],'uploads/blog',$imageName,'400','400');
            
            $input['image'] = $imageName;
        }

        $input['slug'] = $this->findUniqueSlug($input['title'],$input,'blog');
        

        $blog = Blog::create($input);
        
        return redirect('admin/blog')->with('flash_success', 'Blog added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        return view('admin.employee.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $blog = Blog::where('id',$id)->first();
        if ($blog) {
            return view('admin.blog.edit', compact('blog'));
        } else {
            Session::flash('flash_warning', 'Blog is not exist!');
            return redirect('admin/blog');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $input = $request->except(['_token','_method']);
        
        $rules = [
            'title'=>['required','max:191'],
            'sort_desc'=>['required','max:191'],
            'image'=>['nullable','mimes:jpg,jpeg,png'],
            'post_date'=>['required_if:type,blog'],
            'detail'=>['required'],
        ];

        $this->validate($request, $rules);

        $blog = Blog::where('id',$id)->first();

        if(isset($input['image']) && $input['image'] != ''){
            if($blog->image && file_exists('uploads/blog/thumbnail/'.$blog->image)){
                unlink('uploads/blog/thumbnail/'.$blog->image); //delete previously uploaded Attachment
            }
            if($blog->image && file_exists('uploads/blog/'.$blog->image)){
                unlink('uploads/blog/'.$blog->image); //delete previously uploaded Attachment
            }
            $imageName = str_replace(' ', '_', $blog->title).'_'.uniqid(time()) . '.' . $input['image']->getClientOriginalExtension();

            uploadImage($input['image'],'uploads/blog/thumbnail',$imageName,'150','150');
            uploadImage($input['image'],'uploads/blog',$imageName,'400','400');
            
            $input['image'] = $imageName;
        }else{
            $input['image'] = $input['old_image'];
        }

        $input['status'] = isset($input['status'])?$input['status']:0;

        $blog->update(array_except($input,'old_image'));

        return redirect('admin/blog')->with('flash_success', 'Blog updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        Blog::find($id)->delete();

        return redirect('admin/blog')->with('flash_success', 'Blog deleted!');
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'title'=>['required','max:191'],
            'image'=>['required','mimes:jpg,jpeg,png'],
            'post_date'=>['required'],
            'detail'=>['required'],
        ];

        if($id){
            $rules['image'] = 'mimes:jpg,jpeg,png';
        }

        return $this->validate($request, $rules);
    }

    public function findUniqueSlug($name,$requestData,$table)
    {
        $slug = str_slug($name);
        if($table=="blog"){
            $slugCount = Blog::whereSlug($slug)->count();
            if($slugCount > 1){
                if($count >1){
                    $slug = $slug."-".($slugCount+1);
                }
            }
        }
        return $slug;
    }
}
