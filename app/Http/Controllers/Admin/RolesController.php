<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Role;
use DebugBar\DebugBar;
use Illuminate\Http\Request;
use Session;
use App\Permission;
use Yajra\Datatables\Datatables;

class RolesController extends Controller
{


    public function __construct()
    {
        /* $this->middleware('permission:access.roles');
        $this->middleware('permission:access.role.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.role.create')->only(['create', 'store']);
        $this->middleware('permission:access.role.delete')->only('destroy'); */

        view()->share('module', 'Roles');
        view()->share('route', 'roles');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 15;

        if (!empty($keyword)) {
            $roles = Role::where('name', 'LIKE', "%$keyword%")->orWhere('label', 'LIKE', "%$keyword%")
                ->lower()->paginate($perPage);
        } else {
            $roles = Role::paginate($perPage);
        }

        return view('admin.roles.index', compact('roles'));
    }

    public function datatable(){
        //$roles = Role::all();
        $roles = Role::where('id','>',0);
        return Datatables::of($roles)
            ->make(true);
            exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('admin.roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required|regex:/^[a-z0-9 .\-]+$/i','label'=>'unique:role,label']);

        $role = Role::create($request->all());

        Session::flash('flash_message', __('Role added!'));

        return redirect('admin/roles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        $role = Role::where('id',$id)->first();//findOrFail($id);

        if($role){
            $permissions = Permission::with('child')->parent()->get();
            return view('admin.roles.show', compact('role', 'permissions'));
        }else{
            return redirect('admin/roles');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
        $role = Role::where('id',$id)->first();
        if($role){
            return view('admin.roles.edit', compact('role', 'permissions', 'isChecked'));
        }else{
            return redirect('admin/roles');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required|regex:/^[a-z0-9 .\-]+$/i']);

        $role = Role::findOrFail($id);
        $role->update($request->all());

        Session::flash('flash_success', __('Role updated!'));

        return redirect('admin/roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {
        Role::whereId($id)->lower()->delete();

        Session::flash('flash_message', __('Role deleted!'));

        return redirect('admin/roles');
    }
}
