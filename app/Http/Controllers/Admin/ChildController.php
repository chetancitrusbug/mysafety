<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use App\User;
use App\Role;
use App\Setting;
use App\Badgeapprove;
use Carbon\Carbon as Carbon;

use Session;

class ChildController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    protected $parentId = null;
    function __construct()
    {
        $setting = Setting::pluck('value','key')->toArray();
        session(['setting' => $setting]);
        
        view()->share('route', 'Childs');
        view()->share('module', 'Children');
    }

    public function index(Request $request)
    {
        $parentId = $request->parent;
        return view('admin.users.child_index',compact('parentId'));
    }
	
	public function approvebychild($id=0,Request $request)
    {
		$user = User::where('id',$id)->first();
		
		if(\Auth::user()->hasRole('AU')){
					
		}else if($user && $user->id != \Auth::user()->id){
			$user = \Auth::user();
		}
		
		if(!$user){
			$user = \Auth::user();
		}
		
		if(isset($user) && $user->employee){
			$employee = $user->employee;
			
			return view('admin.users.child-approve-by',compact('user','employee'));
			
		}
        
		\Session::flash('flash_error', 'User Not found!');
        return redirect('admin/childs');
        
    }

	public function datatableapproveBy(Request $request)
    {
        $users = User::select([
                    'users.*','badge_approve.created_at as atime'
                   
                ])
				->join('badge_approve','badge_approve.child_id','=','users.id')
				->with(['roles'])
                ->whereHas('roles', function ($query) {
                    $query->whereIn('name', ['Child']);
                });
				if($request->has('refrence') && $request->get('refrence') != ""){
                    $users->where('regi_refrence',$request->get('refrence'));
                }
              //  $users->where('badge_status','approved');
				   if(\Auth::user()->hasRole('AU')){
					 $users->where('users.badge_approve_by',$request->approve);
				   }else{
					   $users->where('users.badge_approve_by',\Auth::user()->id);
				   }
				 
				 if($request->has('from_date') && $request->has('to_date') && $request->get('from_date') !="" && $request->get('to_date') != "" ){
			
					 $to_date = \Carbon\Carbon::createFromFormat('Y-m-d',$request->get('to_date'))->endOfDay();
					 $from_date = \Carbon\Carbon::createFromFormat('Y-m-d',$request->get('from_date'))->startOfDay();
					 $users->whereBetween('badge_approve.created_at', array($from_date, $to_date));
				}
				
                $users = $users->orderBy('users.id','DESC')
                    ->get();
            

        return Datatables::of($users)
            ->make(true);
    }
    public function datatable(Request $request,$id = 0)
    {
        $users = User::select([
                    'users.*',
                   
                ])
                ->with(['roles'])
                ->whereHas('roles', function ($query) {
                    $query->whereIn('name', ['Child']);
                });
                
                if($id > 0){
                    $users->where('parent_id',$id);
                }
				if($request->has('status') && $request->get('status') != ""){
                    $users->where('status',$request->get('status'));
                }
				if($request->has('refrence') && $request->get('refrence') != ""){
                    $users->where('regi_refrence',$request->get('refrence'));
                }
				if($request->has('approve')){
                   $users->where('badge_status','approved');
				   if(\Auth::user()->hasRole('AU')){
					 $users->where('badge_approve_by',$request->approve);
				   }else{
					   $users->where('badge_approve_by',\Auth::user()->id);
				   }
				   
                }else if(\Auth::user()->hasRole('EU')){
					$users->where('status',2); // only in-progress child
					//$users->where('badge_status','in-progress'); // only in-progress child
				}
                $users = $users->orderBy('id','DESC')
                    ->get();
            

        return Datatables::of($users)
            ->make(true);
    }

    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $rule = [
            'first_name' => 'required|max:191',
            'last_name' => 'required|max:191',
            'email' => 'required|email|unique:users|max:191',
            'password' => 'required|same:confirm_password',
            'phone' => 'required|regex:/^(?=.*[0-9])[ +()0-9]+$/',
        ];

        $this->validate($request, $rule);

        $data = $request->except(['password','insurance_certificates']);

        $data['password'] = bcrypt($request->password);

        $user = User::create($data);

        $role=Role::where('label',"PU")->first();
        $user->roles()->attach($role);

        \Session::flash('flash_success', 'User added!');
        return redirect('admin/users');
    }

    public function show($id)
    {
		$user = User::find($id);

		//dd($user->childorders);
        if(!$user){
            return redirect('admin/childs')->with('flash_error', 'User Not Found!');
        }

        return view('admin.users.child_show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
        $user = User::with('roles')->where('id',$id)->first();        
        if ($user) {
            return view('admin.users.edit', compact('user'));
        } else {
            Session::flash('flash_warning', 'User is not exist!');
            return redirect('admin/users');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
	 
	public function unsubscribe($id, Request $request)
    {
		$SECRET_KEY = \config('admin.stripe.SECRET_KEY');
		
        $order = \App\Order::whereId($id)->first();

        if(!$order){
			\Session::flash('flash_error', 'User detail not found !');
		}else{
                $child = $order->child;
                if($child->stripe_acount_type == "local"){ $SECRET_KEY = \config('admin.stripe_citrus.SECRET_KEY'); }
				
				if($order->subscription_id === 0 || $order->subscription_id == ""){
                    
                }else{
					$stripe=\Stripe::make($SECRET_KEY);
                    $subscription = $stripe->subscriptions()->cancel($child->stripe_id, $order->subscription_id, true);
					$order->unsubscription_ob = json_encode($subscription); 
				}
                
                $order->status=0; 
                $order->trans_status="cancelled"; 
                $order->save();
				
				// Make user inactive if no active order
				$orderc=\App\Order::where('child_id',$child->id)->where("trans_status",'active')->orderby('updated_at','DESC')->first();
				if(!$orderc){
					User::where('id',$child->id)->update(['status'=>0]);
				}
				
			\Session::flash('flash_success', 'Unsubscription success !');
		}
        
        return redirect()->back();
    }

	/*public function unsubscribeCurrentPlanByUserId($user_id,$except_id)
    {
		$SECRET_KEY = \config('stripe.SECRET_KEY');
		
        try{
        
        $orders=Order::where('child_id',$user_id)->where("trans_status",'active')->get();
 
        foreach ($orders as $key => $order) {
            if($order && $order->child && $order->id != $except_id)
            {
                $child = $order->child;

                if($order->subscription_id == 0 || $order->subscription_id == ""){
                    
                }else{
                    $stripe=\Stripe::make($SECRET_KEY);
                    $subscription = $stripe->subscriptions()->cancel($child->stripe_id, $order->subscription_id, true);
                }
                
                $order->status=0; 
                $order->trans_status="cancelled"; 
                $order->save();
            }    
        }

        }catch (\Exception $e) {
            
        }
    }*/
	
    public function viewDocument($id, Request $request){
		$user = User::where('id',$id)->first();
		if($user){
			if($request->has('file') && $request->file == "profilepic" && $user->user_image && $user->user_image !="" ){
				$this->BadgeapproveSave(\Auth::user()->id,$id,'profilepic');
				return \Redirect::to($user->user_image);
			}
			if($request->has('file') && $request->file == "document"){
				if($user->refefile && $user->refefile->count() > 0){
					foreach($user->refefile as $refefile){
						if($refefile->file_url && $refefile->file_url!=""){
							return \Redirect::to($refefile->file_url);
							break;
						}
					}
				}
			}
			if($request->has('file') && $request->file != "document"){
				if($user->refefile && $user->refefile->count() > 0){
					foreach($user->refefile as $refefile){
						if($refefile->file_url && $refefile->file_url!="" && $refefile->refe_file_name == $request->file){
							$this->BadgeapproveSave(\Auth::user()->id,$id,$refefile->id);
							return \Redirect::to($refefile->file_url);
							break;
						}
					}
				}
			}
		}
		\Session::flash('flash_error', 'User Not found!');
        return redirect('admin/childs');
	}
    public function BadgeapproveSave($badge_approve_by,$child_id,$document){
		$ba = Badgeapprove::where("badge_approve_by",$badge_approve_by)->where("child_id",$child_id)->first();
		
		$doArr = [];
		if(!$ba){
			$ba = new Badgeapprove();
			$ba->badge_approve_by = \Auth::user()->id;
			$ba->child_id = $child_id;
			$ba->status = 'pending';
		}else{
			if($ba->checked_docs && $ba->checked_docs!=""){
				$doArr = json_decode($ba->checked_docs,true);
			}
		}
		
		$doArr[$document] = 1;
		$ba->checked_docs = json_encode($doArr);
		
		$ba->save();
		
		return 1;
	}
    public function update($id, Request $request)
    {
        $rule = [
            'first_name' => 'required|max:191',
            'last_name' => 'required|max:191',
            'phone' => 'required|regex:/^(?=.*[0-9])[ +()0-9]+$/',
        ];

        if(isset($request->password) && $request->password != ''){
            $rule['password'] = 'required|same:confirm_password';
        }

        $this->validate($request, $rule);

        $requestData = $request->except(['email','insurance_certificates','confirm_password','_method','_token']);
        if(isset($request->password) && $request->password){
            $requestData['password'] = bcrypt($request->password);
        }else{
            unset($requestData['password']);
        }
        
        $requestData['status'] = isset($requestData['status'])?1:0;

        $user = User::where('id',$id)->update($requestData);

        \Session::flash('flash_success', 'User updated!');
        return redirect('admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
	public function deletefile($id)
    {
        $doc = \App\Refefile::where('id',$id)->first();
        
        if($doc){
            if($doc->refe_file_path && \File::exists(public_path()."/".$doc->refe_file_path)){
				unlink(public_path()."/".$doc->refe_file_path);
			}
			
			$doc->delete();
       
			\Session::flash('flash_success', 'Document deleted!');
		}else{
            \Session::flash('flash_error', 'Document not found!');
            
        }
		
		return redirect()->back();
    }
	
    public function destroy(Request $request,$id)
    {
        User::where('id',$id)->delete();
        $this->unsubscribe($id,$request);
        \DB::table('plan_user')->where('child_id',$id)->update(['status'=>'0']);

        if($request->has('from_index')){
            $message = "User Deleted !!";
            return response()->json(['message' => $message],200);
        }else{
            \Session::flash('flash_success', 'User deleted!');
            return redirect('admin/childs');
        }
    }
	
	public function approveForm(Request $request,$id)
    {
        $user = User::where('id',$id)->first();
        

        if($user && $user->status == 2){
            return view('admin.users.approve', compact('user'));
        }else{
            \Session::flash('flash_success', 'User not found!');
            return redirect('admin/childs');
        }
    }
	public function approveSubmit(Request $request)
    {
		$mesage = "";
		
		$rule = [
            'child_profile' => 'required',
            'parent_id' => 'required',
            'child_id' => 'required',
            'sign_permission_form' => 'required',
            'permission_form_child_parent' => 'required',
            'user_id' => 'required'
        ];
		
		$this->validate($request, $rule);
		
		$rule2 = [
            'doc_profilepic' => 'required'
        ];
		$val_msg = [
			'doc_profilepic.required'=> 'Please once click on Profile picture link and verify it.'
		];
		
		$user = User::where('id',$request->user_id)->first();
		
		if($user && $user->refefile && $user->refefile->count() > 0){
			foreach($user->refefile as $refefile){
				if($refefile->file_url && $refefile->file_url!="" && $refefile->refe_file_name != ""){
					$rule2["doc_".$refefile->id] = "required";
					$val_msg["doc_".$refefile->id.".required"] = "Please once click on document '".$refefile->refe_file_name."' link and verify it.";
				}
			}
		}
		
		
		$ba = Badgeapprove::where("badge_approve_by",\Auth::user()->id)->where("child_id",$request->user_id)->first();
		if($ba){
			
			if($ba->checked_docs && $ba->checked_docs!=""){
				$doArr = json_decode($ba->checked_docs,true);
				
				foreach($doArr as $k=>$v){
					unset($rule2["doc_".$k]);
				}
			}
		}else{
			$ba = new Badgeapprove();
			$ba->badge_approve_by = \Auth::user()->id;
			$ba->child_id = $request->user_id;
			$ba->desc = $request->desc;
		}
		//dd($val_msg);
		
		//$this->validate($request, $rule2,$val_msg);
		
		if($user && $user->status >= 2){
            
			
					$order=\App\Order::where('child_id',$user->id)->where("trans_status",'active')->orderby('updated_at','DESC')->first();
				
				
					$user->badge_status = $request->badge_status;
					
					if($order && $request->badge_status == "disapproved" ){
						
						try{
							//send_sms($user->country_code,$user->phone,$message);
						}catch (\Exception $e) {
				
						}
						$parent = $user->parent;
						
						\Mail::send("email.child-disapproved", ['child'=>$user,'user'=>$parent,'desc'=>$request->desc], function ($message) use ($parent) {
							$message->to($parent->email)->bcc(\config('admin.mail_to_admin_support'))->subject("MySafetyNet Child Profile Not Approve !");
						});
						
						$user->status = 0;
						$mesage = "Profile disapproved! If child subscribed for recurring payment, Please unsubscribe or stop recurring!";
						
					}else if($order && $request->badge_status == "approved" ){
						try{
							//send_sms($user->country_code,$user->phone,$message);
						}catch (\Exception $e) {
				
						}
						$parent = $user->parent;
						
						\Mail::send("email.child-approved", ['child'=>$user,'user'=>$parent], function ($message) use ($parent) {
							$message->to($parent->email)->subject("MySafetyNet Child Profile Activated !");
						});
						
						$mesage = "Badge approve success!";
					
						$user->status = 1;
					}else{
						$user->status = 0;
						$mesage = "Profile inactivated ! ";
					}

				$user->badge_approve_by = \Auth::user()->id;
				$user->save();
				
				$ba->status = $request->badge_status;
			    $ba->save();
					
				
				$employee = \Auth::user()->employee;
				if($employee){
					$employee->approved_count = $employee->approved_count + 1;
					$employee->approve_count_total = $employee->approve_count_total + 1;
					$employee->save();
				}
			
			\Session::flash('flash_success', $mesage);
            return redirect('admin/childs/'.$user->id);
        }else{
            \Session::flash('flash_error', 'User not found!');
            return redirect()->back();
        }
    }
}
