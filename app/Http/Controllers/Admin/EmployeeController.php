<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use App\User;
use App\Role;
use App\Setting;
use Carbon\Carbon as Carbon;
use Session;
use App\PlanUser;
use App\Employee;
use App\Badgeapprove;
use App\Transactionlogs;
use App\Jobs\SendEmailJob;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        $setting = Setting::pluck('value','key')->toArray();
        session(['setting' => $setting]);
        
        view()->share('route', 'employee');
        view()->share('module', 'Employee');
    }

    public function index(Request $request)
    {
		$to_date = null;
		$from_date = null;
		
		$users = User::select([
                    'users.*','employee.try_count','employee.is_setup','employee.approved_count'
                ])
                ->with(['roles'])
				->join('employee','employee.user_id','=','users.id')
                ->whereHas('roles', function ($query) {
                    $query->whereIn('name', ['Employee']);
                })
                ->orderBy('id','DESC')
                ->get();
		$total_approved_child = 0;
		
		foreach($users as $i =>$user){
			$badgeapprove = Badgeapprove::where("badge_approve_by",$user->id);
			if($request->has('from_date') && $request->has('to_date') && $request->get('from_date') !="" && $request->get('to_date') != "" ){
				
				 $to_date = \Carbon\Carbon::createFromFormat('Y-m-d',$request->get('to_date'))->endOfDay();
				 $from_date = \Carbon\Carbon::createFromFormat('Y-m-d',$request->get('from_date'))->startOfDay();
				 $badgeapprove->whereBetween('created_at', array($from_date, $to_date));
			}
			
			$badgeapprove->whereIn("status",["approve","disapproved"]);
			$badgeapprove = $badgeapprove->get();
			
			$total_approved_child = $total_approved_child + $badgeapprove->count();
			$user->badgeapprove = $badgeapprove;
		}	

		$unit_sum = Transactionlogs::sum('unit');
		$for_user_sum = Transactionlogs::sum('for_user');
		 
		if($request->has('from_date') && $request->has('to_date') && $request->get('from_date') !="" && $request->get('to_date') != "" ){
				
				 $to_date = \Carbon\Carbon::createFromFormat('Y-m-d',$request->get('to_date'))->endOfDay();
				 $from_date = \Carbon\Carbon::createFromFormat('Y-m-d',$request->get('from_date'))->startOfDay();
				 
				 $unit_sum = Transactionlogs::whereBetween('created_at', array($from_date, $to_date))->sum('unit');
				 $for_user_sum = Transactionlogs::whereBetween('created_at', array($from_date, $to_date))->sum('for_user');
		}
		 
		 $approve_count = Employee::sum('approved_count');
				

				
		$setting = Setting::where('key','amount_per_user_approve')->first();
		if($setting){
			$peruser = $setting->value;
		}else{
			$peruser = 0;
		}
				
        return view('admin.employee.index',compact('users','from_date','to_date','unit_sum','for_user_sum','total_approved_child','approve_count','peruser'));
		
		
		
		
		
    }
	
	 public function paymentdatatable(Request $request,$uid)
    {
		$user_id = $uid;
		if(\Auth::user()->hasRole('AU')){
			$user_id = $uid;
		}else{
			$user_id = \Auth::user()->id;
		}
		
        $users = Transactionlogs::where("user_id",$user_id)->with('creator')->get();
				
		return Datatables::of($users)
            ->make(true);
    }

    public function datatable(Request $request)
    {
        $users = User::select([
                    'users.*','employee.try_count','employee.is_setup','employee.approved_count'
                ])
                ->with(['roles'])
				->join('employee','employee.user_id','=','users.id')
                ->whereHas('roles', function ($query) {
                    $query->whereIn('name', ['Employee']);
                })
                ->orderBy('id','DESC')
                ->get();
				
				

        return Datatables::of($users)
            ->make(true);
    }

    public function create()
    {
        return view('admin.employee.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $rule = [
            'first_name' => 'required|max:191',
            'last_name' => 'required|max:191',
            'email' => 'required|email|unique:users|max:191'
        ];

        $this->validate($request, $rule);

        $data = $request->except(['password','insurance_certificates']);

        $data['status'] = 1;
        $data['password'] = "";

        $user = User::create($data);
		$token = str_random(40);
		$email = $data['email'];
		$empdata = [
			"user_id"=>$user->id,
			"email"=>$data['email'],
			"token"=>$token,
			"try_count"=>1,
			"approved_count"=>0,
			"is_setup"=>"no",
		];
        $employee = Employee::create($empdata);
		
		$subject = \config('app.name')." Employee Account Activation";
		\Mail::send('email.employee-activation', compact(['user']), function ($message) use ($subject,$email) {
				$message->to($email)->subject($subject);
		});

        $user->assignRole("Employee");

		
        \Session::flash('flash_success', 'Employee Created & activation link has been mailed!');
		
        return redirect('admin/employee');
    }

	 public function showMyPayment($id=0)
    {
		$user_id = \Auth::user()->id;
		
		
        $user = User::with(['roles'])->find($user_id);
		
		if(!$user || !$user->hasRole('EU')){
            return redirect('admin/employee')->with('flash_error', 'User Not Found!');
        }

        return view('admin.employee.my-payments', compact('user'));
    }
    public function show($id,Request $request)
    {
		
        $user = User::with(['roles'])->find($id);
		
		if(!$user || !$user->hasRole('EU')){
            return redirect('admin/employee')->with('flash_error', 'User Not Found!');
        }
		
		$badgeapprove = Badgeapprove::where("badge_approve_by",$id);
		if($request->has('from_date') && $request->has('to_date') && $request->get('from_date') !="" && $request->get('to_date') != "" ){
			
			 $to_date = \Carbon\Carbon::createFromFormat('Y-m-d',$request->get('to_date'))->endOfDay();
             $from_date = \Carbon\Carbon::createFromFormat('Y-m-d',$request->get('from_date'))->startOfDay();
			 $badgeapprove->whereBetween('created_at', array($from_date, $to_date));
		}
		
		$badgeapprove->where("status","approve");
		$badgeapprove = $badgeapprove->get();
		
		

        return view('admin.employee.show', compact('user','badgeapprove'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
        $user = User::with('roles')->where('id',$id)->first();        
        if ($user && $user->hasRole('EU')) {
            return view('admin.employee.edit', compact('user'));
        } else {
            Session::flash('flash_warning', 'Employee not exist!');
            return redirect('admin/employee');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $rule = [
            'first_name' => 'required|max:191',
            'last_name' => 'required|max:191',
        ];

        if(isset($request->password) && $request->password != ''){
            $rule['password'] = 'required|same:confirm_password';
        }

        $this->validate($request, $rule);

        $requestData = $request->except(['email','insurance_certificates','confirm_password','_method','_token']);
        if(isset($request->password) && $request->password){
            $requestData['password'] = bcrypt($request->password);
        }else{
            unset($requestData['password']);
        }
        
        $requestData['status'] = isset($requestData['status'])?1:0;

		 $user = User::where('id',$id)->first();
		
		if(isset($user) && $user->employee){
			$user->update($requestData);
			$employee = $user->employee;
			
			if(isset($request->password) && $request->password){
				$employee->is_setup = "yes";
				$employee->token = str_random(10);
				$employee->save();
			}
			
		}
		
       

        \Session::flash('flash_success', 'User updated!');
        return redirect('admin/employee');
    }

	public function resendmail($id)
    {
    
        $user=User::find($id);
		if($user && $user->hasRole('EU')){
			
			$employee = Employee::where("user_id",$user->id)->first();	
			if(!$employee){
				$employee = new Employee();
				$employee->user_id = $user->id;
				$employee->email = $user->email;
				$employee->try_count = 1;
				$employee->approved_count = 0;
				$employee->is_setup = "no";
			}else{
				$employee->try_count = 1 + $employee->try_count;
			}
			$employee->token = str_random(40);
			$employee->save();
			
			$email = $user->email;
			$subject = \config('app.name')." Employee Account Activation";
			
			\Mail::send('email.employee-activation', compact(['user']), function ($message) use ($subject,$email) {
					$message->to($email)->subject($subject);
			});
			
			\Session::flash('flash_success', 'Activation mail sent');
		}else{
            \Session::flash('flash_error', 'Employee data not found !');
            
        }
		 return redirect('admin/employee');
    }
	
	public function activationForm(Request $request,$email,$token)
    {
		$employee = Employee::where('token', $token)->where('email',$email)->first();
        
		if(isset($employee) && $employee->user){
			$user = $employee->user;
			return view('auth.employee-paassword', compact('user','employee'));
		}else{
			return redirect('admin/employee')->with('flash_error','Invalid token or its expired contact admin for mor detail');
		}
		
	}
	public function activationSubmit(Request $request)
    {
		$rule = [
            'password' => 'required|min:6|max:20|same:confirm_password',
            'email' => 'required|email'
        ];
		$this->validate($request, $rule);
		
		$employee = Employee::where('token', $request->_vtoken)->where('email',$request->email)->first();
        
		if(isset($employee) && $employee->user){
			$employee->is_setup = "yes";
			$employee->token = str_random(10);
			$employee->save();
			
			$user = $employee->user;
			$user->password = bcrypt($request->password);
			$user->save();
			return redirect('login')->with('flash_success','Password set successfully.');
		}else{
			return redirect('/')->with('flash_error','Invalid token or its expired contact admin for mor detail');
		}
		
	}
	
	public function paymentForm(Request $request,$uid)
    {
		$employee = Employee::where('user_id',$uid)->first();
		$setting = Setting::where('key','amount_per_user_approve')->first();
		if($setting){
			$peruser = $setting->value;
		}else{
			$peruser = 0;
		}
        
		if(isset($employee) && $employee->user){
			
			if($employee->approved_count <= 0){
				return redirect('admin/employee/'.$uid)->with('flash_error','Employee has not enough credit  for payment');
			}
			$user = $employee->user;
			return view('admin.employee.payment', compact('user','employee','peruser'));
		}else{
			return redirect('admin/employee')->with('flash_error','Invalid User Id');
		}
		
	}
	public function paymentSubmit(Request $request)
    {
		
		$rule = [
            'total_child' => 'required',
            'amount' => 'required',
            'user_id' => 'required'
        ];

        $this->validate($request, $rule);
		
		$employee = Employee::where('user_id',$request->user_id)->first();
        
		if(isset($employee) && $employee->user){
			$user = $employee->user;
			if($employee->approved_count <= 0 || $employee->approved_count < $request->total_child){
				return redirect('admin/employee/'.$uid)->with('flash_success','Employee has not enough child process count  for payment');
			}
			
			$paydata = [
				"user_id"=>$user->id,
				"unit_type"=>'transfer',
				"unit"=>$request->amount,
				"comment"=>$request->comment,
				"for_user"=>$request->total_child,
				"reference"=>'admin_transfer',
				"created_by"=>\Auth::user()->id,
				"updated_by"=>\Auth::user()->id,
			];
			$employee->approved_count = $employee->approved_count - $request->total_child;
			$employee->save();
			
			$transaction = Transactionlogs::create($paydata);
			
			$mdata = ['action'=>'pay_employee','subject'=>"",'transaction'=>$transaction,'view'=>'email.employee-pay-notification','to'=>\config('admin.mail_to_admin')];
			SendEmailJob::dispatch($mdata)->onConnection('database')->onQueue('emails');
		
			return redirect('admin/employee/'.$user->id)->with('flash_success','Payment Detail saved success!');
		}else{
			return redirect()->back()->with('flash_error','Invalid User Id');
		}
		
	}
	
	public function destroy(Request $request,$id)
    {
    
        $user=User::find($id);
		if($user && $user->employee){
			$user->employee->delete();
		}
		User::where('id',$id)->delete();
        
		if($request->ajax()){
            $message = "User Deleted !!";
            return response()->json(['message' => $message],200);
        }else{
            \Session::flash('flash_success', 'User deleted!');
            return redirect('admin/employee');
        }
    }
}
