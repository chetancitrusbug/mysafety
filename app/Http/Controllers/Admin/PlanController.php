<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use App\Plan;
use App\PlanUser;
use Carbon\Carbon as Carbon;

class PlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        view()->share('route', 'plan');
        view()->share('module', 'Plans');
    }

    public function index(Request $request)
    {
        return view('admin.plan.index');
    }

    public function datatable(Request $request)
    {
        $plan = Plan::select([
                    'plan.*',
                ])
                ->get();

        return Datatables::of($plan)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('admin.plan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');
		
        $rule = [
            'title' => 'required',
            'actual_amount' => 'required',
            'description' => 'required',
            'type' => 'required',
            'status' => 'required',
        ];

        $this->validate($request, $rule);

        $plan = Plan::create($input);
        
        return redirect('admin/plan')->with('flash_success', 'Plan added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $plan = Plan::where('id',$id)->first();
        if ($plan) {
            return view('admin.plan.edit', compact('plan'));
        } else {
            Session::flash('flash_warning', 'Plan is not exist!');
            return redirect('admin/plan');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $input = $request->except(['_token','_method']);
		
        $rule = [
            'title' => 'required',
            'actual_amount' => 'required',
            'description' => 'required',
            'type' => 'required',
            'status' => 'required',
        ];

        $this->validate($request, $rule);

        $input['status'] = isset($input['status'])?$input['status']:0;
        if($input['status'] == 0){
            $user = PlanUser::select([
                        'plan_user.id',
                    ])
                    ->join('users',function($join){
                        $join->on('users.id','=','plan_user.user_id');
                    })
                    ->where('plan_id',$id)
                    ->whereNull('users.deleted_at')
                    ->get();

            if(count($user)){
                return redirect('admin/plan/'.$id.'/edit')->with('flash_error', 'Plan assigned to user');
            }
        }

        $plan = Plan::where('id',$id)->first();

        $plan->update($input);

        return redirect('admin/plan')->with('flash_success', 'Plan updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        $plan=Plan::withTrashed()->find($id);
        if($plan->status == 0){
            $user = PlanUser::select([
                        'plan_user.id',
                    ])
                    ->join('users',function($join){
                        $join->on('users.id','=','plan_user.user_id');
                    })
                    ->where('plan_id',$plan->id)
                    ->whereNull('users.deleted_at')
                    ->get();

            if(count($user)){
                return redirect('admin/plan')->with('flash_error', 'Plan assigned to user');
              
            }
        }
        $plan->delete();


        return redirect('admin/plan')->with('flash_success', 'Plan deleted!');
    }

    public function validateData($request, $id = 0)
    {	$regex = "/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/";
        $rules = [
            'title'=>['required','max:191','regex:/^[a-z0-9 .\-]+$/i'],
            'description'=>['required'],
            'actual_amount'=>['required|numeric','regex:'.$regex,function($attribute, $value, $fail) use($request,$id) {
                            if($value < 0){
                                $fail('The amount must be greater than 0');
                            }
                        }],
            'show_amount'=>['required|numeric','regex:'.$regex,function($attribute, $value, $fail) use($request,$id) {
                            if($value < 0){
                                $fail('The amount must be greater than 0');
                            }
                        }],
            'type'=>['required',function($attribute, $value, $fail) use($request,$id) {
                            $plan = Plan::where($attribute,$value)->where('status','1')->first();
                            if($plan && $plan->id != $id && $request->get('status') != ''){
                                $fail('This type of plan already exist');
                            }
                        }],
        ];

        return $this->validate($request, $rules);
    }

    public function generateStripePlan($id,Request $request)
    {
        $item = Plan::where("id",$id)->first();
        if(!$item){
            \Session::flash('flash_error',"No Plan Found");
            return redirect('admin/plan');
        }
        
        if($item->stripe_product_id && $item->stripe_product_id !=""){
            \Session::flash('flash_error',"Strip Plan Already Exist");
            return redirect('admin/plan');
        }
        
		//echo \config('admin.stripe.SECRET_KEY'); exit;
		
        $stripe = \Stripe::make(\config('admin.stripe.SECRET_KEY'));
        
		$uid = "MY_SAFETY_".$item->id;
        $plan = $stripe->plans()->create([
            'id' => $uid,
            'name' => $item->type,
            'amount' => $item->actual_amount,
            'currency' => 'USD',
            'interval' => config('admin.interval')[$item->type],
            'interval_count'=>  1,
            'statement_descriptor' => $item->type. " USD ".$item->actual_amount,
        ]);

        $item->stripe_product_id = $plan['product'];
		$item->plan_uid = $uid;
        $item->save();
        
        return redirect('admin/plan')->with('flash_success','Strip Plan Created !!');
    }
}
