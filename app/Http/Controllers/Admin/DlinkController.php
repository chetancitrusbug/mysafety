<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use App\ClickFunnel;
use App\User;
use App\Role;
use App\Setting;
use Carbon\Carbon as Carbon;
use Session;
use App\PlanUser;

class DlinkController extends Controller
{

    
    public function store(Request $request)
    {
		$json = '{"purchase":{"id":35401493,"products":[{"id":1887400,"name":"1 Year Free Subscription(Just Pay Processing Fee)","stripe_plan":"","amount":{"fractional":"1200.0","currency":{"id":"usd","alternate_symbols":["US$"],"decimal_mark":".","disambiguate_symbol":"US$","html_entity":"$","iso_code":"USD","iso_numeric":"840","name":"United States Dollar","priority":1,"smallest_denomination":1,"subunit":"Cent","subunit_to_unit":100,"symbol":"$","symbol_first":true,"thousands_separator":","},"bank":{"store":{"index":{"EUR_TO_USD":"1.1244","EUR_TO_JPY":"124.91","EUR_TO_BGN":"1.9558","EUR_TO_CZK":"25.656","EUR_TO_DKK":"7.4608","EUR_TO_GBP":"0.8624","EUR_TO_HUF":"315.84","EUR_TO_PLN":"4.2994","EUR_TO_RON":"4.7491","EUR_TO_SEK":"10.5802","EUR_TO_CHF":"1.1349","EUR_TO_ISK":"136.4","EUR_TO_NOK":"9.77","EUR_TO_HRK":"7.413","EUR_TO_RUB":"74.2572","EUR_TO_TRY":"6.1177","EUR_TO_AUD":"1.5935","EUR_TO_BRL":"4.3275","EUR_TO_CAD":"1.5093","EUR_TO_CNY":"7.5612","EUR_TO_HKD":"8.8264","EUR_TO_IDR":"16067.68","EUR_TO_ILS":"4.0718","EUR_TO_INR":"78.547","EUR_TO_KRW":"1275.28","EUR_TO_MXN":"21.8444","EUR_TO_MYR":"4.5982","EUR_TO_NZD":"1.6492","EUR_TO_PHP":"58.751","EUR_TO_SGD":"1.5283","EUR_TO_THB":"35.655","EUR_TO_ZAR":"16.152","EUR_TO_EUR":1},"options":[],"mutex":[],"in_transaction":false},"rounding_method":null,"currency_string":null,"rates_updated_at":"2019-03-11T00:00:00.000+00:00","last_updated":"2019-03-12T12:18:44.029+00:00"}},"amount_currency":"USD","created_at":"2019-03-07T04:12:31.000Z","updated_at":"2019-03-07T04:12:31.000Z","subject":"Thank you for your purchase!","html_body":"\r\nThank you for your purchase, you are on e step closer to protecting your child. Please use the Following to complete your registration:\r\n\r\nYou may access your Thank You Page here anytime:\r\n\r\n#PRODUCT_THANK_YOU_PAGE#\r\n\r\n","thank_you_page_id":35704450,"stripe_cancel_after_payments":null,"braintree_cancel_after_payments":null,"bump":false,"cart_product_id":null,"billing_integration":"stripe_account-86026","infusionsoft_product_id":null,"braintree_plan":null,"infusionsoft_subscription_id":null,"ontraport_product_id":null,"ontraport_payment_count":null,"ontraport_payment_type":null,"ontraport_unit":null,"ontraport_gateway_id":null,"ontraport_invoice_id":null,"commissionable":true,"statement_descriptor":"MySafetyNet ProcessFee"}],"member_id":null,"contact":{"id":781485451,"page_id":26975758,"first_name":"Karmrajsinh","last_name":"","name":"Karmrajsinh","address":"","city":"","country":"","state":"","zip":"","email":"karmrajsinh@gmail.com","phone":"9898122444","webinar_at":null,"webinar_last_time":null,"webinar_ext":"A6xLJfhL","created_at":"2019-03-12T12:38:31.000Z","updated_at":"2019-03-12T12:38:31.000Z","ip":"182.70.122.97","funnel_id":6866012,"funnel_step_id":35553397,"unsubscribed_at":null,"cf_uvid":"74239af2d84324b6f34f4e84ba14ca5c","cart_affiliate_id":"","shipping_address":"","shipping_city":"","shipping_country":"","shipping_state":"","shipping_zip":"","vat_number":"","affiliate_id":null,"aff_sub":"","aff_sub2":"","cf_affiliate_id":null,"contact_profile":{"id":391072275,"first_name":"Raj111","last_name":"Vaghela","address":"","city":null,"country":null,"state":null,"zip":null,"email":"karmrajsinh@gmail.com","phone":"9898122444","created_at":"2019-03-12T07:08:25.000Z","updated_at":"2019-03-12T12:38:31.000Z","unsubscribed_at":null,"cf_uvid":"74239af2d84324b6f34f4e84ba14ca5c","shipping_address":"","shipping_country":null,"shipping_city":null,"shipping_state":null,"shipping_zip":null,"vat_number":null,"middle_name":null,"websites":null,"location_general":"Ahmedabad Area, India","normalized_location":null,"deduced_location":null,"age":0,"gender":"Male","age_range_lower":26,"age_range_upper":36,"action_score":null,"known_ltv":"0.00","tags":[]},"additional_info":{"cf_affiliate_id":"","time_zone":"Chennai","utm_source":"","utm_medium":"","utm_campaign":"","utm_term":"","utm_content":"","cf_uvid":"74239af2d84324b6f34f4e84ba14ca5c","webinar_delay":"-63719632382427","purchase":{"product_ids":["1887400"],"taxamo_transaction_key":"","payment_method_nonce":"","order_saas_url":"","stripe_customer_token":"tok_1ED9sIEvuSemklM5s9IDT0Od"}},"time_zone":"Chennai"},"funnel_id":6866012,"stripe_customer_token":"tok_1ED9sIEvuSemklM5s9IDT0Od","created_at":"2019-03-12T12:38:32.000Z","updated_at":"2019-03-12T12:38:32.000Z","subscription_id":null,"charge_id":"ch_1ED9sLEvuSemklM5Kc6PKJIf","ctransreceipt":null,"status":"paid","fulfillment_status":null,"fulfillment_id":null,"fulfillments":[],"payments_count":null,"infusionsoft_ccid":null,"oap_customer_id":null,"braintree_customer_id":null,"payment_instrument_type":null,"taxamo_amount":null,"original_amount_cents":1200,"original_amount":{"fractional":"1200.0","currency":{"id":"usd","alternate_symbols":["US$"],"decimal_mark":".","disambiguate_symbol":"US$","html_entity":"$","iso_code":"USD","iso_numeric":"840","name":"United States Dollar","priority":1,"smallest_denomination":1,"subunit":"Cent","subunit_to_unit":100,"symbol":"$","symbol_first":true,"thousands_separator":","},"bank":{"store":{"index":{"EUR_TO_USD":"1.1244","EUR_TO_JPY":"124.91","EUR_TO_BGN":"1.9558","EUR_TO_CZK":"25.656","EUR_TO_DKK":"7.4608","EUR_TO_GBP":"0.8624","EUR_TO_HUF":"315.84","EUR_TO_PLN":"4.2994","EUR_TO_RON":"4.7491","EUR_TO_SEK":"10.5802","EUR_TO_CHF":"1.1349","EUR_TO_ISK":"136.4","EUR_TO_NOK":"9.77","EUR_TO_HRK":"7.413","EUR_TO_RUB":"74.2572","EUR_TO_TRY":"6.1177","EUR_TO_AUD":"1.5935","EUR_TO_BRL":"4.3275","EUR_TO_CAD":"1.5093","EUR_TO_CNY":"7.5612","EUR_TO_HKD":"8.8264","EUR_TO_IDR":"16067.68","EUR_TO_ILS":"4.0718","EUR_TO_INR":"78.547","EUR_TO_KRW":"1275.28","EUR_TO_MXN":"21.8444","EUR_TO_MYR":"4.5982","EUR_TO_NZD":"1.6492","EUR_TO_PHP":"58.751","EUR_TO_SGD":"1.5283","EUR_TO_THB":"35.655","EUR_TO_ZAR":"16.152","EUR_TO_EUR":1},"options":[],"mutex":[],"in_transaction":false},"rounding_method":null,"currency_string":null,"rates_updated_at":"2019-03-11T00:00:00.000+00:00","last_updated":"2019-03-12T12:18:44.029+00:00"}},"original_amount_currency":"USD","taxamo_tax_rate":0,"manual":false,"error_message":null,"nmi_customer_vault_id":null},"event":"created"}';
		

		$ob = json_decode($json, TRUE);
		
		$arr = [];
		$arr['child_id'] = 0;
		$arr['parent_id'] = 0;
		
		if(isset($ob['purchase'])){
			$arr['click_funnel_id'] = $ob['purchase']['id'];
			if(isset($ob['purchase']['products']) && isset($ob['purchase']['products'][0])){
				$arr['product_id'] = $ob['purchase']['products'][0]['id'];
				$arr['product_name'] = $ob['purchase']['products'][0]['name'];
				
				$arr['product_price'] = $ob['purchase']['products'][0]['amount']['fractional'];
				$arr['product_currency'] = $ob['purchase']['products'][0]['amount']['currency']['id'];
				
			}
			
			if(isset($ob['purchase']['contact'])){
				$arr['contact_id'] = $ob['purchase']['contact']['id'];
				$arr['contact_first_name'] = $ob['purchase']['contact']['first_name'];
				$arr['contact_last_name'] = $ob['purchase']['contact']['last_name'];
				$arr['contact_name'] = $ob['purchase']['contact']['name'];
				$arr['contact_email'] = $ob['purchase']['contact']['email'];
				$arr['contact_phone'] = $ob['purchase']['contact']['phone'];
				$arr['contact_funnel_id'] = $ob['purchase']['contact']['funnel_id'];
			}
			
			if(isset($ob['purchase']['charge_id'])){
				$arr['contact_phone'] = $ob['purchase']['stripe_customer_token'];
				$arr['charge_id'] = $ob['purchase']['charge_id'];
				$arr['payment_status'] = $ob['purchase']['status'];
				$arr['subscription_id'] = $ob['purchase']['subscription_id'];
				
				$arr['create_date'] = date('Y-m-d h:i:s', strtotime($ob['purchase']['created_at']));
			}
			
			
			$exist = ClickFunnel::where("click_funnel_id",$ob['purchase']['id'])->first();
			if(!$exist){
				ClickFunnel::create($arr);
			}
			
		}
		
		echo "<pre>"; print_r(($arr)); exit;
    }

    
}
