<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use App\User;
use App\Role;
use App\Setting;
use Carbon\Carbon as Carbon;
use Session;
use App\PlanUser;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        $setting = Setting::pluck('value','key')->toArray();
        session(['setting' => $setting]);
        
        view()->share('route', 'parent');
        view()->share('module', 'Parents');
    }

    public function index(Request $request)
    {
        return view('admin.users.index');
    }

    public function datatable(Request $request)
    {
        $users = User::select([
                    'users.*',
                ])
                ->with(['roles'])
                ->whereHas('roles', function ($query) {
                    $query->whereIn('name', ['Parent','Admin']);
                })
                ->orderBy('id','DESC')
                ->get();

        return Datatables::of($users)
            ->make(true);
    }

    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $rule = [
            'first_name' => 'required|max:191',
            'last_name' => 'required|max:191',
            'email' => 'required|email|unique:users|max:191',
            'password' => 'required|same:confirm_password',
            'phone' => 'required|regex:/^(?=.*[0-9])[ +()0-9]+$/|unique:users,phone|min:10|max:12',
        ];

        $this->validate($request, $rule);

        $data = $request->except(['password','insurance_certificates']);

        $data['password'] = bcrypt($request->password);
        $data['status'] = 1;

        $user = User::create($data);

        $role=Role::where('label',"PU")->first();
        $user->roles()->attach($role);

        \Session::flash('flash_success', 'User added!');
        return redirect('admin/users');
    }

    public function show($id)
    {
        $user = User::with(['roles'])->find($id);
		
		
        
        if(!$user){
            return redirect('admin/users')->with('flash_error', 'User Not Found!');
        }

        return view('admin.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
        $user = User::with('roles')->where('id',$id)->first();        
        if ($user) {
            return view('admin.users.edit', compact('user'));
        } else {
            Session::flash('flash_warning', 'User is not exist!');
            return redirect('admin/users');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $rule = [
            'first_name' => 'required|max:191',
            'last_name' => 'required|max:191',
            'phone' => 'required|regex:/^(?=.*[0-9])[ +()0-9]+$/',
        ];

        if(isset($request->password) && $request->password != ''){
            $rule['password'] = 'required|same:confirm_password';
        }

        $this->validate($request, $rule);

        $requestData = $request->except(['email','insurance_certificates','confirm_password','_method','_token']);
        if(isset($request->password) && $request->password){
            $requestData['password'] = bcrypt($request->password);
        }else{
            unset($requestData['password']);
        }
        
        $requestData['status'] = isset($requestData['status'])?1:0;

        $user = User::where('id',$id)->update($requestData);

        \Session::flash('flash_success', 'User updated!');
        return redirect('admin/users');
    }

	public function updateBadgeStatus($status,$id,Request $request)
    {
        $message = "Welcome to MySafetyNet,
Your parent or legal guardian has registered you to our platform. You have created your online profile,
feel free to download our free phone app for MySafetyNet at the link below and start using your
username and password through our integrated phone app.
Link
Kind Regard
MySafetyNet Team";

        $user =User::find($id);

		
        if($user){
			$order=\App\Order::where('child_id',$user->id)->where("trans_status",'active')->orderby('updated_at','DESC')->first();
			
			if($user->badge_status == "in-progress"){
				$user->badge_status = "approved";
				
				
				if($order){
					try{
						send_sms($user->country_code,$user->phone,$message);
					}catch (\Exception $e) {
            
					}
					$parent = $user->parent;
					
					\Mail::send("email.child-approved", ['child'=>$user,'user'=>$parent], function ($message) use ($parent) {
						$message->to($parent->email)->cc(\config('admin.mail_to_admin_cc'))->subject("MySafetyNet Child Profile Activated !");
					});
				
					$user->status = 1;
				}else{
					$user->status = 0;
				}

			}else if($user->badge_status == "approved"){
				$user->badge_status = "in-progress";
				$user->status = 2;
			}
			$user->save();
		}else{
			
		}
		
		return response()->json(['message' => 'Status Updated'],200);
	}
	
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request,$id)
    {
    
        $user=User::find($id);


        $user_data = PlanUser::select([
            'plan_user.id',
        ])
        ->join('users',function($join){
            $join->on('users.id','=','plan_user.user_id');
        })
        ->where('user_id',$user->id)
        ->whereNull('users.deleted_at')
        ->get();
        if(count($user_data)){
            return redirect('admin/users')->with('flash_error', 'Plan assigned to user');
          
        }

        User::where('id',$id)->delete();
        User::where('parent_id',$id)->delete();
        \DB::table('plan_user')->where('user_id',$id)->update(['status'=>'0']);
       
        if($request->has('from_index')){
            $message = "User Deleted !!";
            return response()->json(['message' => $message],200);
        }else{
            \Session::flash('flash_success', 'User deleted!');
            return redirect('admin/users');
        }
    }
}
