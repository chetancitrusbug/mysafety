<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use App\Order;
use Carbon\Carbon as Carbon;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        view()->share('route', 'orders');
        view()->share('module', 'Orders');
    }

    public function index(Request $request)
    {
        return view('admin.order.index');
    }

    public function datatable(Request $request)
    {
        $order = Order::select([
                    'orders.*',
                ])
                ->get();

        return Datatables::of($order)
            ->make(true);
    }

    public function show($id)
    {
		/*$SECRET_KEY = \config('admin.stripe.SECRET_KEY');
		$stripe=\Stripe::make($SECRET_KEY);
		$order = Order::where('id','=',$id)->first(); 
		$invoices = $stripe->invoices()->all(["subscription"=>$order->subscription_id]);
		echo "<pre>"; print_r($invoices);*/
		
		
        $order = Order::
                leftjoin('order_user',function($join){
                    $join->on('order_user.order_id','=','orders.id');
                })
                ->leftjoin('users',function($join){
                    $join->on('users.id','=','order_user.user_id');
                })
                ->select([
                    'orders.*',
                  'users.first_name',
                ])
                ->where('orders.id',$id)->first();
				
      //  echo "<pre>"; print_r($order ->billing); exit;
                
        if(!$order){
            return redirect('admin/orders')->with('flash_error', 'Order Not Found!');
        }

        return view('admin.order.show', compact('order'));
    }
}
