<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Logs;

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Session;
use Auth;
use Carbon;

class LogsController extends Controller
{
    function __construct()
    {
        view()->share('route', 'logs');
        view()->share('module', 'logs');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
		
        return view('admin.logs.index');
    }
    public function datatable(Request $request) {
		
        $record = Logs::where("id",">",0);
        return Datatables::of($record)->make(true);
    }

	public function show($id,Request $request)
    {
		$item = Logs::where("id",$id)->first();
		if($item){
			return view('admin.logs.show',compact('item'));
		}
	}
	public function destroy($id,Request $request)
    {
        $item = Logs::where("id",$id)->first();

        $result = array();

        if($item){
            $item->delete();
            $result['message'] = "Record Deleted";
            $result['code'] = 200;

        }else{
            $result['message'] = "Some thing went wrong.";
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin');
        }
    }


}
