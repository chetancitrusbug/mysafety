<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use App\Order;
use Carbon\Carbon as Carbon;

use App\Jobs;
use App\Jobs\SendEmailJob;


use App\ContactUs;
use App\User;
use App\Plan;

use App\Responcetext as RT;

class CronController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    

    public function cronrun(Request $request)
    {
		
		$next_send_time = date('H:i:00');
		
		if($next_send_time == "10:10:00"){
			$report = [];
			$report['new_invoice'] = $this->getInvoice();
			$report['feature_invoice_mail'] = $this->sendMailFeaturePayment();
			$report['plan_auto_upgrate_notification'] = $this->autoUpgradeMontlyPlan();
		//	$report['plan_upgrate_notification'] = $this->sendMailToUpgrade(); // we are auto updating from free to monthly pack
			$report['payment_due_notification'] = $this->sendMailDuePayment();
			$report['payment_due_inactive_child'] = $this->DuePaymentInactiveUser();
			
			$subject = \config('admin.MAIL_SUBJECT_PREFIX')."Daily Report - ".\Carbon\Carbon::now()->format("jS M, Y");
			\Mail::send('email.admin.cron-report', with(['report'=>$report]), function ($message) use ($subject) {
				$message->to(\config('admin.mail_to_admin'))->bcc(\config('admin.mail_to_admin_cc'))->subject($subject);
			});
		}
		
		
		$this->sendqueuemail();
		
	// 	$this->pushNotificationTest("");
		//$this->getFirstInvoice(88);
		//$this->mailtest();
		//$this->saveCard();
		
		
	}
	public function pushNotificationTest()
	{
		$user=User::whereEmail("jignesh.citrusbug@gmail.com")->first();
		
		$message = array();
        $title='Get connected';
        $msg=' Connected.';

        $message['body'] = $msg;
        $message['message'] = $msg;
        $message['title'] = $title;
        $message['target_screen'] = 'parent';

        $message['sender']= $user;
        $message['receiver']= $user;
		
        $message['title'] = ' connected.';
            $message['message'] ='Your Request Approved by ';
            $message['target_screen'] = 'child_connect';
            $result=send_notification("db4f4a8366b721a88292571a35d153d6a02a1a93b4515d7ff2bd1ae9f818b010",[],"ios",$message);
       
	}
	public function mailtest()
	{
	//	echo "<br/>1";
		try{
			$contactUs=contactUs::whereEmail("jignesh.citrusbug@gmail.com")->first();
			$subject = \config('admin.MAIL_SUBJECT_PREFIX')."MySafetyNet  Registration";
				
			//echo view('email.complaint', compact('contactUs'))->render();exit;
			
			\Mail::send('email.complaint', with(['contactUs'=>$contactUs]), function ($message) use ($contactUs, $subject) {
				$message->to(\config('admin.mail_to_admin_support'))->bcc(\config('admin.mail_to_admin_cc'))->subject($subject);
				$message->attach(public_path()."/files/parentapermissionform.pdf");
			});
			echo "<br/>2";
		}catch(\Exception $e){
			echo $e->getMessage();
			echo "<br/>3";
		}
		
		echo "<br/>4";
	}
	public function sendqueuemail()
	{
		
		$jobs=Jobs::where("queue","emails")->get();

		
		try{
		
		foreach($jobs as $jo){
			$job=json_decode($jo->payload);
			$cm = unserialize($job->data->command);
			
			$subject = \config('admin.MAIL_SUBJECT_PREFIX').$cm->event['subject'];
			$view = $cm->event['view'];
			$to = $cm->event['to'];
				
			if(isset($cm->event['action']) && $cm->event['action'] == "contact_us_mail"){
				$contactUs = $cm->event['contactUs'];
				
				\Mail::send($view, compact('contactUs'), function ($message) use ($contactUs, $subject,$to) {
					$message->to(\config('admin.mail_to_admin_support'))->bcc(\config('admin.mail_to_admin_cc'))->subject($subject);
				});
			
			}else if(isset($cm->event['action']) && $cm->event['action'] == "ebook_mail"){
				$contactUs = $cm->event['contactUs'];
				
				\Mail::send($view, compact('contactUs'), function ($message) use ($contactUs, $subject,$to) {
					$message->to(\config('admin.mail_to_ebook'))->subject($subject);
				});
				
				$subject = RT::rtext("mail_subject_ebook");
				\Mail::send("email.ebook-responce", compact('contactUs'), function ($message) use ($contactUs, $subject,$to) {
					$message->to($contactUs->email)->subject($subject);
					$message->attach(public_path()."/files/MySafetyNet_eBook.pdf");
				});
				
			}else if(isset($cm->event['action']) && $cm->event['action'] == "referral_mail"){
				$contactUs = $cm->event['contactUs'];
				
				\Mail::send($view, compact('contactUs'), function ($message) use ($contactUs, $subject,$to) {
					$message->to(\config('admin.mail_to_admin'))->bcc(\config('admin.mail_to_admin_cc'))->subject($subject);
				});
				
			}else if(isset($cm->event['action']) && $cm->event['action'] == "unsubscription_success"){
				
				$user = $cm->event['user'];
				$order = $cm->event['order'];
				$plan = $cm->event['plan'];
				
				\Mail::send($view, ['order'=>$order,'plan'=>$plan], function ($message) use ($user, $subject,$to) {
					$message->to($to)->bcc(\config('admin.mail_to_admin_cc'))->subject($subject);
				});
			}else if(isset($cm->event['action']) && $cm->event['action'] == "subscription_upgrated"){
				
				$user = $cm->event['user'];
				$order = $cm->event['order'];
				$plan = $cm->event['plan'];

				\Mail::send($view, ['order'=>$order,'plan'=>$plan], function ($message) use ($user, $subject,$to) {
					$message->to($to)->bcc(\config('admin.mail_to_admin_cc'))->subject($subject);
				});
				
			}else if(isset($cm->event['action']) && $cm->event['action'] == "request_accept_mail_to_parent"){
				
				$user = $cm->event['user'];
				$notification = $cm->event['notification'];
				

				\Mail::send($view, ['notification'=>$notification,'user'=>$user], function ($message) use ($user, $subject,$to) {
					$message->to($to)->bcc(\config('admin.mail_to_admin_cc'))->subject($subject);
				});
				
			}else if(isset($cm->event['action']) && $cm->event['action'] == "parent_registration"){
				$user = $cm->event['user'];
				\Mail::send($view, with(['user'=>$user]), function ($message) use ($user, $subject,$to) {
					$message->to($to)->bcc(\config('admin.mail_to_admin_cc'))->subject($subject);
				});
				
			}else if(isset($cm->event['action']) && $cm->event['action'] == "child_registration"){
				$user = $cm->event['user'];

				\Mail::send($view, with(['user'=>$user]), function ($message) use ($user, $subject,$to) {
					$message->to($to)->bcc(\config('admin.mail_to_admin_cc'))->subject($subject);
					$message->attach(public_path()."/files/parentapermissionform.pdf");
				});
				
			}else if(isset($cm->event['action']) && $cm->event['action'] == "pay_employee"){
				$transaction = $cm->event['transaction'];
				$user = $transaction->user;
				$employee = $user->employee;
				
				
				//to admin	
				$receiver_name = "Admin";
				$to = \config('admin.mail_to_admin_cc');
				\Mail::send($view, compact(['user','transaction','employee','receiver_name']), function ($message) use ($to) {
					$message->to($to)->subject(\config('admin.MAIL_SUBJECT_PREFIX')."Employee Payout Detail");
				});
				
				//to account	
				$receiver_name = "Account Team";
				$to = \config('admin.mail_to_admin');
				\Mail::send($view, compact(['user','transaction','employee','receiver_name']), function ($message) use ($to) {
					$message->to($to)->subject(\config('admin.MAIL_SUBJECT_PREFIX')."Employee Payout Detail");
				});
				
				//to employee	
				$receiver_name = "";
				$to = $user->email;
				\Mail::send($view, compact(['user','transaction','employee','receiver_name']), function ($message) use ($to) {
					$message->to($to)->subject(\config('admin.MAIL_SUBJECT_PREFIX')."Employee Payout Detail");
				});
				
			}else if(isset($cm->event['action']) && $cm->event['action'] == "get_invoice"){
				$order = $cm->event['order'];
				if($order){
						$this->getFirstInvoice($order->id);
				}
				
			}
			
			
			$jo->delete();
		}
		
		}catch(\Exception $e){

			$report = [];
			$report['mail_fail']['error'][] = ["id"=>"N/A","message"=>$e->getMessage()];
			
			$subject = \config('admin.MAIL_SUBJECT_PREFIX')."Daily Mail fail - ".\Carbon\Carbon::now()->format("jS M, Y");
			\Mail::send('email.admin.cron-report', with(['report'=>$report]), function ($message) use ($subject) {
				$message->to(\config('admin.mail_to_admin_cc'))->subject($subject);
			});
		}
	}

    public function getInvoice()
    {
	   $res = [];
		
	   $SECRET_KEY = \config('admin.stripe.SECRET_KEY');
	   $stripe=\Stripe::make($SECRET_KEY);
	   
	   $SECRET_KEY_local = \config('admin.stripe_citrus.SECRET_KEY');
       $stripe_local=\Stripe::make($SECRET_KEY_local);
	   
       $orders = Order::whereNotNull('expiry_date')->where('expiry_date','<',\Carbon\Carbon::now())->where("trans_status","active")->get();
	
	   foreach($orders as $order){
		   
		   
		    $plan = json_decode($order->plan_ob);
			$child = $order->child;
			
			if($child && $order->subscription_id != "" && $order->subscription_id !== 0 && $order->subscription_id != "0"){
				
			$invoices = null;
			

				try{
					
				
					if($child->stripe_acount_type == "live"){ 
						$invoices = $stripe->invoices()->all(["subscription"=>$order->subscription_id]);
					}else{
						$invoices = $stripe_local->invoices()->all(["subscription"=>$order->subscription_id]);
					}
				
				
			
		//	echo "--<pre>"; print_r($invoices); exit;
				if($invoices && isset($invoices['data'])){
				
					foreach($invoices['data'] as $invoice){
						
						$bill= \App\Billingcycle::where("invoice_id",$invoice["id"])->first();
						

						if(!$bill&& $plan){

							$bill = new \App\Billingcycle();
							$bill->invoice_id = $invoice["id"];
							$bill->plan_id = $order->plan_id ;
							$bill->order_id= $order->id;
							$bill->amount_paid= $invoice["amount_paid"]/100;
							$bill->charge_id= $invoice["charge"];
							$bill->price_currency	= "USD";
							$bill->discount	= 0;
							$bill->billing_type	= "stripe";
							$bill->billing_ob	= json_encode($invoice);
							
							
							$start_date = $order->expiry_date;
							if($plan->type == "monthly"){
								$end_date = \Carbon\Carbon::createFromFormat('Y-m-d',$order->expiry_date)->addMonth();
							}
							if($plan->type == "yearly"){
								$end_date = \Carbon\Carbon::createFromFormat('Y-m-d',$order->expiry_date)->addYear();
							}
							if($plan->type == "weekly"){
								$end_date = \Carbon\Carbon::createFromFormat('Y-m-d',$order->expiry_date)->addWeek();
							}
							
							if(isset($invoice['lines']) && isset($invoice['lines']['data']) && isset($invoice['lines']['data'][0])){
								$start_date = \Carbon\Carbon::createFromTimestamp($invoice['lines']['data'][0]['period']['start'])->format("Y-m-d"); 
								$end_date = \Carbon\Carbon::createFromTimestamp($invoice['lines']['data'][0]['period']['end'])->format("Y-m-d"); 
							}
							
							$bill->start_date	= $start_date;
							$bill->end_date	= $end_date;
							$bill->save();
							
							if($order->expiry_date < $end_date){
								$order->expiry_date = $end_date;
								$order->save();
							}
							
							
							
							$subject = \config('admin.MAIL_SUBJECT_PREFIX')."Mysafetynet Receive Payment";
							$user = $order->parent;
							if($user){
								\Mail::send('email.billing.bill-generate', compact('order','bill','plan'), function ($message) use ($user, $subject) {
									$message->to($user->email)->bcc(\config('admin.mail_to_admin_cc'))->subject($subject);
								});
							}
						}
					}
					
					$res['success'][] = ["id"=>$order->id,"message"=>$user->email];
				}
				
				}catch(\Exception $e){
					
					$res['error'][] = ["id"=>$order->id,"message"=>$e->getMessage()];
				}
			}else if(!$child){
				$res['warning'][] = ["id"=>$order->id,"message"=>"Child deleted"];
			}
	   }
	   
	   return $res;
				 
	}
	public function autoUpgradeMontlyPlan()
    {
		$res = [];
		
		$today = \Carbon\Carbon::now()->format("Y-m-d");
		$where_filter = "((subscription_id = '')  OR (subscription_id = '0') OR (subscription_id IS NULL))";
        $orders = Order::whereNotNull('expiry_date')->where('expiry_date','<',$today)->where("trans_status","active")->whereRaw($where_filter)->get();
		
		foreach($orders as $order){
			
			try{
			   $plan = json_decode($order->plan_ob);
			   $subject = \config('admin.MAIL_SUBJECT_PREFIX')."Mysafetynet Subscription Upgrade Notification";
			   $user = $order->parent;
			   $child = $order->child;
			   
			   if(!$child || $child->stripe_id || $child->stripe_id == ""){
				   $res['warning'][] = ["id"=>$order->id,"message"=>"No child or stripe account found"];
				   continue;
			   }
			   $mplan = Plan::where("type","monthly")->where("status","1")->first();
			   
			   $SECRET_KEY = \config('admin.stripe.SECRET_KEY');
			   if($child->stripe_acount_type == "local"){ 
					$SECRET_KEY = \config('admin.stripe_citrus.SECRET_KEY');
					$mplan = Plan::where("plan_uid","MY_SAFETY_19")->first();
			   }
			   $stripe=\Stripe::make($SECRET_KEY);
			   
						
						if($user && $mplan && $child){
							$planid = $mplan->plan_uid;
							if($planid == "" || !$planid){
								$planid = $mplan->id;
							}
							$subscription = $stripe->subscriptions()->create($child->stripe_id, ['plan' => $planid]);
							
							if($subscription && isset($subscription['id'])){
               
								if($child->status == 0) $child->status = 1;
								$child->save();
								
								$date = Carbon::now()->addMonths(1);
								$order_new = [];

								$order_new['parent_id'] = $user->id;
								$order_new['child_id'] = $child->id;
								$order_new['plan_id'] = $mplan->id;

								$order_new['start_date'] = Carbon::now()->format('Y-m-d');
								$order_new['expiry_date'] = $date->subDays(1)->format('Y-m-d');
								$order_new['next_pay'] = $date->addDays(1)->format('Y-m-d');
								$order_new['auth_response'] = json_encode($subscription);
								$order_new['plan_ob'] = json_encode($mplan);
								$order_new['order_no'] = Order::getNextOrderNumber();
								$order_new['amount'] = $mplan->actual_amount;
								$order_new['subscription_id'] = $subscription['id'];
								$order_new['trans_status'] = $subscription['status'];
								$order_new['token'] = "";
								$order_new['status'] = 1; 
								
								$result = Order::create($order_new);

								// echo "<pre>"; print_r($order); exit;
							
								$subject = \config('admin.MAIL_SUBJECT_PREFIX').RT::rtext("mail_subject_upgrate_plan_success");
										
								$mdata = ['action'=>'subscription_upgrated','user'=>$user,'subject'=>$subject,'order'=>$result,'plan'=>$mplan,'view'=>'email.billing.upgrade-plan-success','to'=>$user->email];
								SendEmailJob::dispatch($mdata)->onConnection('database')->onQueue('emails');
											
								$mdata = ['action'=>'get_invoice','order'=>$result,'view'=>'','to'=>"","subject"=>""];
								SendEmailJob::dispatch($mdata)->onConnection('database')->onQueue('emails');
										
								$order->status=0; 		
								$order->trans_status = "cancelled";
								$order->save();		
							}
						}
						
				$res['success'][] = ["id"=>$order->id,"message"=>$user->email];		
						
			}catch(\Exception $e){
					
					$res['error'][] = ["id"=>$order->id,"message"=>$e->getMessage()];
			}			
	    }
		
		return $res;
    }
	public function sendMailToUpgrade()
    {
		$res = [];
		
		$todaysub3 = \Carbon\Carbon::now()->addDays(3)->startOfDay()->format("Y-m-d");
		$where_filter = "((subscription_id = '')  OR (subscription_id = '0') OR (subscription_id IS NULL))";
        $orders = Order::whereNotNull('expiry_date')->where('expiry_date',$todaysub3)->where("trans_status","active")->whereRaw($where_filter)->get();
		
		foreach($orders as $order){
			
			try{
			   $plan = json_decode($order->plan_ob);
			   $subject = \config('admin.MAIL_SUBJECT_PREFIX')."Mysafetynet Subscription Upgrade Notification";
						$user = $order->parent;
						if($user && $plan){
							\Mail::send('email.billing.upgrade-plan', compact('order','plan'), function ($message) use ($user, $subject) {
								$message->to($user->email)->cc(\config('admin.mail_to_admin_cc'))->subject($subject);
							});
						}
						
				$res['success'][] = ["id"=>$order->id,"message"=>$user->email];		
						
			}catch(\Exception $e){
					
					$res['error'][] = ["id"=>$order->id,"message"=>$e->getMessage()];
			}			
	    }
		
		return $res;
    }
	public function sendMailFeaturePayment()
    {
	   $res = [];
		
	   $todaysub3 = \Carbon\Carbon::now()->addDays(3)->startOfDay();
       $orders = Order::whereNotNull('expiry_date')->where('expiry_date',$todaysub3)->where("trans_status","active")->where("subscription_id","!=",0)->where("subscription_id","!=","")->whereNotNull("subscription_id")->get();
	   foreach($orders as $order){
		    try{
			   $plan = json_decode($order->plan_ob);
			   $subject = \config('admin.MAIL_SUBJECT_PREFIX')."Mysafetynet Billing Notification";
						$user = $order->parent;
						if($user && $plan){
							\Mail::send('email.billing.bill-in-feature', compact('order','plan'), function ($message) use ($user, $subject) {
								$message->to($user->email)->cc(\config('admin.mail_to_admin_cc'))->subject($subject);
							});
						}
				$res['success'][] = ["id"=>$order->id,"message"=>$user->email];
				
			}catch(\Exception $e){
					$res['error'][] = ["id"=>$order->id,"message"=>$e->getMessage()];
			}			
	   }
	   
	   return $res;
    }
	
	public function sendMailDuePayment()
    {
		$res = [];
		
	   $todaysub1 = \Carbon\Carbon::now()->subDays(1)->startOfDay();
	   $todaysub3 = \Carbon\Carbon::now()->subDays(3)->startOfDay();
	   $todaysub6 = \Carbon\Carbon::now()->subDays(6)->startOfDay();
	   $todaysub10 = \Carbon\Carbon::now()->subDays(10)->startOfDay();
	   
       $orders = Order::whereNotNull('expiry_date')->whereIn('expiry_date',[$todaysub1,$todaysub3,$todaysub6,$todaysub10])->where("trans_status","active")->where("subscription_id","!=","")->whereNotNull("subscription_id")->get();
	   
	   foreach($orders as $order){
		   
		    try{
			   $plan = json_decode($order->plan_ob);
			   $subject = \config('admin.MAIL_SUBJECT_PREFIX')."Mysafetynet Payment due";
						$user = $order->parent;
						if($user && $plan){
							\Mail::send('email.billing.bill-due', compact('order','plan'), function ($message) use ($user, $subject) {
								$message->to($user->email)->cc(\config('admin.mail_to_admin_cc'))->subject($subject);
							});
						}
						
				$res['success'][] = ["id"=>$order->id,"message"=>$user->email];		
			
			}catch(\Exception $e){
					
					$res['error'][] = ["id"=>$order->id,"message"=>$e->getMessage()];
			}
	   }
	   
	   
       return $res;
    }
	public function DuePaymentInactiveUser()
    {
		$res = [];
		
		$todaysub2 = \Carbon\Carbon::now()->subDays(11)->startOfDay();
		$orders = Order::whereNotNull('expiry_date')->where('expiry_date',"<",$todaysub2)->where("trans_status","active")->get();
		foreach($orders as $order){
			if($order->child){
				
				try{
					$child = $order->child;
					$child->status = 0;
					$child->save();
				
					if($order->subscription_id === 0 || $order->subscription_id == "" || $order->subscription_id == "0" || !$order->subscription_id){
                    
					}else{
                    
						$SECRET_KEY = \config('admin.stripe.SECRET_KEY');
						if($child->stripe_acount_type == "local"){ $SECRET_KEY = \config('admin.stripe_citrus.SECRET_KEY'); }
						$stripe=\Stripe::make($SECRET_KEY);

						$subscription = $stripe->subscriptions()->cancel($child->stripe_id, $order->subscription_id, true);
						$order->unsubscription_ob = json_encode($subscription); 
					}
				
				
					$order->trans_status = "suspended";
					$order->save();
					
					$plan = json_decode($order->plan_ob);
					$subject = \config('admin.MAIL_SUBJECT_PREFIX')."Mysafetynet child profile Inactivated";
							$user = $order->parent;
							if($user && $plan){
								\Mail::send('email.billing.bill-due-inactivated', compact('order','plan'), function ($message) use ($user, $subject) {
									$message->to($user->email)->cc(\config('admin.mail_to_admin_cc'))->subject($subject);
								});
							}
					$res['success'][] = ["id"=>$order->id,"message"=>$user->email];		
						
				}catch(\Exception $e){
					$res['error'][] = ["id"=>$order->id,"message"=>$e->getMessage()];
				}		
			}else{
				$order->trans_status = "cancelled";
				$order->save();
				
				$res['warning'][] = ["id"=>$order->id,"message"=>"Child deleted so order inactivated"];
			}			
						
						
		}
       
	     return $res;
    }

    public function savePlanOb()
    {
        $orders = Order::where('plan_ob','=',"")->orWhereNull('plan_ob')->get();
		
		foreach($orders as $order){
			$plan = $order->plan;
			if($plan){
				$order->plan_ob = json_encode($plan);
				$order->save();
			}
		}
    }
	public function saveCard()
    {
		$SECRET_KEY = \config('admin.stripe.SECRET_KEY');
		$stripe=\Stripe::make($SECRET_KEY);
	   
		$SECRET_KEY_local = \config('admin.stripe_citrus.SECRET_KEY');
		$stripe_local=\Stripe::make($SECRET_KEY_local);
	   
        $users = User::where("stripe_id","!=","")->whereNotNull("stripe_id")->limit(25)->orderby("id","desc")->get();
		
		foreach($users as $user){
			if($user->cards->count() <= 0){
				if($user->stripe_acount_type == "live"){ 
					$mystripe = $stripe;
				}else{
					$mystripe = $stripe_local;
				}

				try{
					$mycards = $mystripe->cards()->all($user->stripe_id,[]);	
					if($mycards && isset($mycards['data'])){
						foreach($mycards['data'] as $cardRes){
							\App\Cards::saveCard($cardRes,$user->stripe_id);
							
							echo "<br/>--".$user->stripe_id."(".$user->full_name.") : ".$cardRes['id'];
						}
					}
					
				
				}catch(\Exception $e){
					
					echo "<br/>--".$e->getMessage();
				}
				
			} 
		}
    }
	public function getFirstInvoice($id)
    {
	   $res = [];
		
	   $SECRET_KEY = \config('admin.stripe.SECRET_KEY');
	   $stripe=\Stripe::make($SECRET_KEY);
	   
	   $SECRET_KEY_local = \config('admin.stripe_citrus.SECRET_KEY');
       $stripe_local=\Stripe::make($SECRET_KEY_local);
	   
	   if($id && $id !=""){
		   $orders = Order::whereId($id)->get();
	   }else{
		   $orders = Order::where("subscription_id","!=","")->whereNotNull("subscription_id")->get();
	   }
       
	
	   foreach($orders as $order){
		   
		   
		    $plan = json_decode($order->plan_ob);
			$child = $order->child;
			
			
			if($order->billing->count() == 0 && $child && $order->subscription_id != "" && $order->subscription_id !== 0 && $order->subscription_id != "0"){
			//	echo "dd"; exit;
				$invoices = null;
			

				try{
					
				
					if($child->stripe_acount_type == "live"){ 
						$invoices = $stripe->invoices()->all(["subscription"=>$order->subscription_id]);
					}else{
						$invoices = $stripe_local->invoices()->all(["subscription"=>$order->subscription_id]);
					}
				
				
			
		//	echo "--<pre>"; print_r($invoices); exit;
				if($invoices && isset($invoices['data'])){
				
					foreach($invoices['data'] as $invoice){
						
						$bill= \App\Billingcycle::where("invoice_id",$invoice["id"])->first();
						

						if(!$bill&& $plan){

							$bill = new \App\Billingcycle();
							$bill->invoice_id = $invoice["id"];
							$bill->plan_id = $order->plan_id ;
							$bill->order_id= $order->id;
							$bill->amount_paid= $invoice["amount_paid"]/100;
							$bill->charge_id= $invoice["charge"];
							$bill->price_currency	= "USD";
							$bill->discount	= 0;
							$bill->billing_type	= "stripe";
							$bill->billing_ob	= json_encode($invoice);
							
							
							$start_date = $order->expiry_date;
							if($plan->type == "monthly"){
								$end_date = \Carbon\Carbon::createFromFormat('Y-m-d',$order->expiry_date)->addMonth();
							}
							if($plan->type == "yearly"){
								$end_date = \Carbon\Carbon::createFromFormat('Y-m-d',$order->expiry_date)->addYear();
							}
							if($plan->type == "weekly"){
								$end_date = \Carbon\Carbon::createFromFormat('Y-m-d',$order->expiry_date)->addWeek();
							}
							
							if(isset($invoice['lines']) && isset($invoice['lines']['data']) && isset($invoice['lines']['data'][0])){
								$start_date = \Carbon\Carbon::createFromTimestamp($invoice['lines']['data'][0]['period']['start'])->format("Y-m-d"); 
								$end_date = \Carbon\Carbon::createFromTimestamp($invoice['lines']['data'][0]['period']['end'])->format("Y-m-d"); 
							}
							
							$bill->start_date	= $start_date;
							$bill->end_date	= $end_date;
							$bill->save();
							
							if($order->expiry_date < $end_date || $id !=""){
								$order->expiry_date = $end_date;
								$order->save();
							}
							
							
							
							$subject = \config('admin.MAIL_SUBJECT_PREFIX')."Mysafetynet Receive Payment";
							$user = $order->parent;
							if($user){
								\Mail::send('email.billing.bill-generate', compact('order','bill','plan'), function ($message) use ($user, $subject) {
									$message->to($user->email)->cc(\config('admin.mail_to_admin_cc'))->subject($subject);
								});
							}
						}
					}
					
					$res['success'][] = ["id"=>$order->id,"message"=>"New invoice sent to ".$user->email];
				}
				
				}catch(\Exception $e){
					
					$res['error'][] = ["id"=>$order->id,"message"=>$e->getMessage()];
				}
			}else{
				$res['warning'][] = ["id"=>$order->id,"message"=>"Child deleted"];
			}
	   }
	   
	  
				 
	}
	
	
	public function inactiveAll()
    {
		$res = [];
		
		$todaysub2 = \Carbon\Carbon::now()->subDays(11)->startOfDay();
		$orders = Order::get();
		foreach($orders as $order){
			if($order->child){
				
				try{
					$child = $order->child;
					$child->status = 0;
					$child->save();
				
					if($order->subscription_id === 0 || $order->subscription_id == "" || $order->subscription_id == "0" || !$order->subscription_id){
                    
					}else{
                    
						$SECRET_KEY = \config('admin.stripe.SECRET_KEY');
						if($child->stripe_acount_type == "local"){ $SECRET_KEY = \config('admin.stripe_citrus.SECRET_KEY'); }
						$stripe=\Stripe::make($SECRET_KEY);

						$subscription = $stripe->subscriptions()->cancel($child->stripe_id, $order->subscription_id, true);
						$order->unsubscription_ob = json_encode($subscription); 
					}
				
				
					$order->trans_status = "suspended";
					$order->save();
					
					$plan = json_decode($order->plan_ob);
					$subject = \config('admin.MAIL_SUBJECT_PREFIX')."Mysafetynet child profile Inactivated";
							$user = $order->parent;
							if($user && $plan){
								/*\Mail::send('email.billing.bill-due-inactivated', compact('order','plan'), function ($message) use ($user, $subject) {
									$message->to($user->email)->subject($subject);
								});*/
							}
					$res['success'][] = ["id"=>$order->id,"message"=>$user->email];		
						
				}catch(\Exception $e){
					$res['error'][] = ["id"=>$order->id,"message"=>$e->getMessage()];
				}		
			}else{
				$order->trans_status = "cancelled";
				$order->save();
				
				$res['warning'][] = ["id"=>$order->id,"message"=>"Child deleted so order inactivated"];
			}			
						
						
		}
       
	   \Mail::raw(json_encode($res), function ($message) {
  $message->to("jignesh.citrusbug@gmail.com")
    ->subject("Deactive all user");
});


    }
}
