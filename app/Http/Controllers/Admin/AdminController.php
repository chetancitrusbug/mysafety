<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\Setting;
use Auth;
use App\User;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    function __construct()
    {
        view()->share('route', 'blog');
        view()->share('module', 'Edit Profile');
    }
    public function index()
    {
    	$setting = Setting::pluck('value','key')->toArray();
		session(['setting' => $setting]);
    	
        if(Session::has('flash_success')){
            Session::flash('flash_success',Session::get('flash_success'));
        }
        return redirect('/admin/users');
    }
    public function edit_profile()
    {
        $user=Auth::user();
        return view('admin.editprofile',compact('user'));
    }
    public function update_profile(Request $request)
    {
        $rule = [
            'first_name' => 'required|max:191',
            'last_name' => 'required|max:191',
            'phone' => 'required|regex:/^(?=.*[0-9])[ +()0-9]+$/',
        ];

        if(isset($request->password) && $request->password != ''){
            $rule['password'] = 'required|same:confirm_password';
        }

        $this->validate($request, $rule);

        $requestData = $request->all();

        $user = User::findOrFail(Auth::user()->id);
        $user->first_name=$request->first_name;
        $user->last_name=$request->last_name;
        $user->phone=$request->phone;
        $user->save();


        return redirect('admin/users')->with('flash_success', 'Profile Updated!');
    }
    public function change_password(Request $request)
    {
        $user= User::find(Auth::user()->id);
        if($user == null)
        {
            return redirect()->back()->with('flash_warning', 'User not Found!');
        }

         return view('admin.change_password', compact('user'));  
    }

    public function post_password(Request $request)
    {
        $rule = [
           
            'password' => 'required|string|min:6|same:confirm_password',
        ];
        $this->validate($request, $rule);

        $user= User::find(Auth::user()->id);
        $user->password=Hash::make($request->password);
        $user->save();

        $email=$user->email;

        return redirect('admin/users')->with('flash_success', 'Password Successfully Changed!');
        
    }
}
