<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Responcetext;
use App\Seocontent;

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Session;
use Auth;
use Carbon;

class SeoController extends Controller
{
    function __construct()
    {
        view()->share('route', 'seo-data');
        view()->share('module', 'Seo Data');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('admin.seo-data.index');
    }
	public function datatable(Request $request) {
        $record = Seocontent::where("id",">",0);
        return Datatables::of($record)->make(true);
    }
    
	public function show($id,Request $request)
    {
		$item = Seocontent::where("page_name",$id)->first();
		if(!$item){
			$item = Seocontent::where("page_name","default")->first();
		}
		
		$page_id = $id;
		return view('admin.seo-data.show',compact('item','page_id'));
		
	}
	
    public function edit($id)
    {
        $result = array();
        $item = Seocontent::where("page_name",$id)->first();
		if(!$item){
			$item = Seocontent::where("page_name","default")->first();
		}
		//dd($item);
		$page_id = $id;
        return view('admin.seo-data.edit', compact('item','page_id'));
		
    }

	
	
    public function update($id, Request $request)
    {
        $result = array();

        $this->validate($request, [
            'page_name' => 'required',
            'title' => 'required',
            'keywords' => 'required',
            'description' => 'required'
        ]);

        
        $requestData = $request->all();
		$item = Seocontent::updateOrCreate(["page_name"=>$request->page_name],$requestData);
        
        if($item){
            $result['message'] = "Record updated success";
            $result['code'] = 200;

        }else{
            $result['message'] = "Something went wrong!";
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/seo-data');
        }
    }
	public function store(Request $request)
    {
        $result = array();

        $this->validate($request, [
            'page_name' => 'required',
            'title' => 'required',
            'keywords' => 'required',
            'description' => 'required'
        ]);

        
        $requestData = $request->all();
		$item = Seocontent::updateOrCreate(["page_name"=>$request->page_name],$requestData);
        
        if($item){
            $result['message'] = "Record updated success";
            $result['code'] = 200;

        }else{
            $result['message'] = "Something went wrong!";
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/seo-data');
        }
        
    }
    

    


}
