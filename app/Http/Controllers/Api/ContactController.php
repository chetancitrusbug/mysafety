<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blog;
use App\ContactUs;
use App\Setting;

use App\Jobs\SendEmailJob;
use App\Responcetext as RT;

class ContactController extends Controller
{
   
    public function getEbook(Request $request)
    {
        $rules = array(
            'email' => 'required|email',
            'first_name' => 'required',
            'last_name' => 'required'
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json([
                'message' =>$messages,
                'success' => false,
                'status' => $code],RESPONCE_ERROR_CODE);
        }

		$subject = $request->subject;
		$subject = RT::rtext("mail_subject_contact_for_ebook");
		

        $contactUs=new ContactUs();
        $contactUs->name=$request->first_name." ".$request->last_name;
        $contactUs->email=$request->email;
        $contactUs->subject=$request->subject;
        $contactUs->message="";
        $contactUs->save();

        $mdata = ['action'=>'ebook_mail','subject'=>$subject,'contactUs'=>$contactUs,'view'=>'email.ebook','to'=>\config('admin.mail_to_admin')];
        SendEmailJob::dispatch($mdata)->onConnection('database')->onQueue('emails');

        $result=make_null($contactUs);

        return response()->json([
            'result' => $result,
            'message' => RT::rtext("success_ebook_request"),
            'success' => true,
            'status' => 200,
        ],200);

    }
	public function contact_us(Request $request)
    {
        $rules = array(
            'email' => 'required|email',
            'subject' => 'required',
            'name' => 'required',
            'message' => 'required',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json([
                'message' =>$messages,
                'success' => false,
                'status' => $code],RESPONCE_ERROR_CODE);
        }

		$subject = $request->subject;
		if($request->has('enquiry_type') && $request->enquiry_type == "media-schools"){
			$subject = RT::rtext("mail_subject_contact_for_media_or_school");;
		}

        $contactUs=new ContactUs();
        $contactUs->name=$request->name;
        $contactUs->email=$request->email;
        $contactUs->subject=$request->subject;
        $contactUs->message=$request->message;
        $contactUs->save();

        $mdata = ['action'=>'contact_us_mail','subject'=>$subject,'contactUs'=>$contactUs,'view'=>'email.complaint','to'=>\config('admin.mail_to_admin')];
        SendEmailJob::dispatch($mdata)->onConnection('database')->onQueue('emails');

        
		
        $result=make_null($contactUs);

        return response()->json([
            'result' => $result,
            'message' => RT::rtext("success_contact_request"),
            'success' => true,
            'status' => 200,
        ],200);

    }
	public function complain(Request $request)
    {
        $rules = array(
            'email' => 'required|email',
            'name' => 'required',
            'message' => 'required',
        );

		if($request->get('user_type') == "CU"){
			$rules = array(
				'phone' => 'required',
				'name' => 'required',
				'message' => 'required',
			);
		}
		
		$val_msg = [
			'phone.required'=>'Required phone number'
		];
        $validator = \Validator::make($request->all(), $rules, $val_msg);
        

        if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json([
                'message' =>$messages,
                'success' => false,
                'status' => $code],RESPONCE_ERROR_CODE);
        }


        $contactUs=new ContactUs();
        $contactUs->name=$request->name;
        $contactUs->message=$request->message;
		
		if($request->has('email')){
			$contactUs->email=$request->email;
		}
		if($request->has('subject')){
			$contactUs->subject=$request->subject;
		}
		if($request->has('phone')){
			$contactUs->phone=$request->phone;
		}
		if($request->has('user_id')){
			$contactUs->user_id=$request->user_id;
		}
		
        $contactUs->save();

		$subject = "Mysafetynet complaint request from ".$request->name;
        $mdata = ['action'=>'contact_us_mail','subject'=>$subject,'contactUs'=>$contactUs,'view'=>'email.complaint','to'=>\config('admin.mail_to_admin')];
        SendEmailJob::dispatch($mdata)->onConnection('database')->onQueue('emails');
		
		$result=make_null($contactUs);

        return response()->json([
            'result' => $result,
            'message' => RT::rtext("success_complaint_request"),
            'success' => true,
            'status' => 200,
        ],200);

    }
	
	public function referral(Request $request)
    {
        $rules = array(
            'email' => 'required|email',
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'method_of_contact' => 'required',
            'message' => 'required',
        );

		$validator = \Validator::make($request->all(), $rules,[]);
        

        if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json([
                'message' =>$messages,
                'success' => false,
                'status' => $code],RESPONCE_ERROR_CODE);
        }


        $contactUs=new ContactUs();
        $contactUs->name=$request->first_name." ".$request->last_name;
        $contactUs->message=$request->message;
		
		if($request->has('email')){
			$contactUs->email=$request->email;
		}
		if($request->has('method_of_contact')){
			$contactUs->subject=$request->method_of_contact;
		}
		if($request->has('phone')){
			$contactUs->phone=$request->phone;
		}
		if($request->has('user_id')){
			$contactUs->user_id=$request->user_id;
		}
		
        $contactUs->save();

		$subject = "Mysafetynet referral program request from ".$request->first_name." ".$request->last_name;
        $mdata = ['action'=>'referral_mail','subject'=>$subject,'contactUs'=>$contactUs,'view'=>'email.referral','to'=>\config('admin.mail_to_admin')];
        SendEmailJob::dispatch($mdata)->onConnection('database')->onQueue('emails');
		
		$result=make_null($contactUs);

        return response()->json([
            'result' => $result,
            'message' => RT::rtext("success_complaint_request"),
            'success' => true,
            'status' => 200,
        ],200);

    }
}
