<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Faqs;

use App\Setting;
use App\Responcetext as RT;

class FaqsController extends Controller
{
    //
    public function allFaqs()
    {
        $setting = Setting::pluck('value','key')->toArray();
        $page=Faqs::where('status',"active")->orderby("display_order","ASC")->paginate($setting['pagination']);

        $result=$page->toArray();

        if($page->count() > 0)
        {
            return response()->json([
                'total' => get_api_data(isset($result['total']) ? $result['total'] : 0),
                'current_page' => get_api_data(isset($result['current_page']) ? $result['current_page'] : 0),
                'prev_page_url' => get_api_data(isset($result['prev_page_url']) ? $result['prev_page_url'] : ''),
                'next_page_url' => get_api_data(isset($result['next_page_url']) ? $result['next_page_url'] : ''),
                'result' =>$result['data'],
                'message' => 'Page list.',
                'success' => true,
                'status' => 200,
            ]);

        }
        return response()->json([
            'total' => get_api_data(isset($result['total']) ? $result['total'] : 0),
            'current_page' => get_api_data(isset($result['current_page']) ? $result['current_page'] : 0),
            'prev_page_url' => get_api_data(isset($result['prev_page_url']) ? $result['prev_page_url'] : ''),
            'next_page_url' => get_api_data(isset($result['next_page_url']) ? $result['next_page_url'] : ''),
            'message' => RT::rtext("warning_no_data_found"),
            'status'  => 400
        ], RESPONCE_ERROR_CODE);
    }

    
}
