<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blog;
use App\ContactUs;
use App\Setting;
use App\Responcetext as RT;

class BlogController extends Controller
{
    //
    public function allPages()
    {
        $setting = Setting::pluck('value','key')->toArray();
        $page=Blog::select([
            'blog.*',
            \DB::raw('(CASE WHEN image != "" THEN CONCAT( "'.url('').'",CONCAT("/uploads/blog/"),image) ELSE "" END) AS image'),
            \DB::raw('(CASE WHEN image != "" THEN CONCAT( "'.url('').'",CONCAT("/uploads/blog/thumbnail/"),image) ELSE "" END) AS thumb_image'),
        ])->where('type','page')->where('status',1)->paginate($setting['pagination']);

        $result=$page->toArray();

        if($page->count() > 0)
        {
            return response()->json([
                'total' => get_api_data(isset($result['total']) ? $result['total'] : 0),
                'current_page' => get_api_data(isset($result['current_page']) ? $result['current_page'] : 0),
                'prev_page_url' => get_api_data(isset($result['prev_page_url']) ? $result['prev_page_url'] : ''),
                'next_page_url' => get_api_data(isset($result['next_page_url']) ? $result['next_page_url'] : ''),
                'result' =>$result['data'],
                'message' => 'Page list.',
                'success' => true,
                'status' => 200,
            ]);

        }
        return response()->json([
            'total' => get_api_data(isset($result['total']) ? $result['total'] : 0),
            'current_page' => get_api_data(isset($result['current_page']) ? $result['current_page'] : 0),
            'prev_page_url' => get_api_data(isset($result['prev_page_url']) ? $result['prev_page_url'] : ''),
            'next_page_url' => get_api_data(isset($result['next_page_url']) ? $result['next_page_url'] : ''),
            'message' => RT::rtext("warning_no_data_found"),
            'status'  => 400
        ], RESPONCE_ERROR_CODE);
    }

    public function page(Request $request)
    {
        $page=Blog::where('slug',$request->slug)->first();

        if($page)
        {
            $result=make_null($page);
            
            return response()->json([
                'result' => $result,
                'message' => 'Page.',
                'success' => true,
                'status' => 200,
            ],200);
        }
        return response()->json([
            'success' => false,
            'message' => RT::rtext("warning_no_data_found"),
            'status'  => 400
        ], RESPONCE_ERROR_CODE);
    }

    public function allBlogs(Request $request)
    {
        $setting = Setting::pluck('value','key')->toArray();
        $page=Blog::select([
                'blog.*',
                \DB::raw('(CASE WHEN image != "" THEN CONCAT( "'.url('').'",CONCAT("/uploads/blog/"),image) ELSE "" END) AS image'),
                \DB::raw('(CASE WHEN image != "" THEN CONCAT( "'.url('').'",CONCAT("/uploads/blog/thumbnail/"),image) ELSE "" END) AS thumb_image'),
            ])->where('type','blog')->where('status',1)->orderBy('id',$request->get('order_by','DESC'))->paginate(4);
        
        $result=$page->toArray();
    
        if($page->count() > 0)
        {
  
            return response()->json([
                'total' => get_api_data(isset($result['total']) ? $result['total'] : 0),
                'current_page' => get_api_data(isset($result['current_page']) ? $result['current_page'] : 0),
                'prev_page_url' => get_api_data(isset($result['prev_page_url']) ? $result['prev_page_url'] : ''),
                'next_page_url' => get_api_data(isset($result['next_page_url']) ? $result['next_page_url'] : ''),
                'result' =>$result['data'],
                'message' => 'Blog list.',
                'success' => true,
                'status' => 200,
            ]);

        }
        else
        {
            return response()->json([
                'total' => get_api_data(isset($result['total']) ? $result['total'] : 0),
                'current_page' => get_api_data(isset($result['current_page']) ? $result['current_page'] : 0),
                'prev_page_url' => get_api_data(isset($result['prev_page_url']) ? $result['prev_page_url'] : ''),
                'next_page_url' => get_api_data(isset($result['next_page_url']) ? $result['next_page_url'] : ''),
                'message' => RT::rtext("warning_no_data_found"),
                'status'  => 400
            ], RESPONCE_ERROR_CODE);
        }
        
    }

    public function blog(Request $request)
    {
        $blog=Blog::select([
                        'blog.*',
                        \DB::raw('(CASE WHEN image != "" THEN CONCAT( "'.url('').'",CONCAT("/uploads/blog/"),image) ELSE "" END) AS image'),
                        \DB::raw('(CASE WHEN image != "" THEN CONCAT( "'.url('').'",CONCAT("/uploads/blog/thumbnail/"),image) ELSE "" END) AS thumb_image'),
                    ])
                    ->where('slug',$request->slug)
                    ->where('type','blog')
                    ->first();
               
                    

        if($blog)
        {
            $result=$blog->toArray();
            
            return response()->json([
               
                'result' => $result,
                'message' => 'Blog.',
                'success' => true,
                'status' => 200,
            ],200);
        }
        return response()->json([
            'success' => false,
            'message' => RT::rtext("warning_no_data_found"),
            'status'  => 400
        ], RESPONCE_ERROR_CODE);
    }

    
}
