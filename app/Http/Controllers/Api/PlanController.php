<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Plan;
use App\Responcetext as RT;

class PlanController extends Controller
{
    //
    public function index()
    {
        $plan=Plan::where('status',1)->get();

        if($plan->count() > 0)
        {
            $result=make_null($plan);
            
            return response()->json([
                'result' => $result,
                'message' => 'Plan list.',
                'success' => true,
                'status' => 200,
            ],200);

        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => RT::rtext("warning_no_data_found"),
                'status'  => 400
            ], RESPONCE_ERROR_CODE);
        }
    }
}
