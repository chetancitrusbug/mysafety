<?php

namespace App\Http\Controllers\Api\Parent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use App\OrderUser;
use App\Order;
use App\PlanUser;
use App\Payment;
use App\PlanOrder;
use App\Setting;
use App\Plan;
use App\Cards;
use App\Subscription;
use App\Billingcycle;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use Carbon\Carbon as Carbon;
use DB;
use App\Notification;

use App\Jobs\SendEmailJob;
use App\Responcetext as RT;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
		
        $rules = array(
            'email' => 'required|email|unique:users',
            'password'=>'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'parent_mob_no' => 'required|regex:/^(?=.*[0-9])[ +()0-9]+$/|unique:users,phone',
            'user_type' => 'required'
        );
		$val_msg = [
			'parent_mob_no.unique'=> RT::rtext("warning_require_unique_phone_number")
		];
        $validator = \Validator::make($request->all(), $rules, $val_msg);

        if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json([
                'message' =>$messages,
                'success' => false,
                'status' => $code],RESPONCE_ERROR_CODE);
        }


        $user=new User();
        $user->first_name=$request->first_name;
        $user->last_name=$request->last_name;
        $user->email=$request->email;
        $user->password=Hash::make($request->password);
        $user->phone=$request->parent_mob_no;
		$user->regi_refrence="special_link";
		
        $user->status=1;
		if($request->has('country_code') && $request->get('country_code') != "" ){ 
			$user->country_code=$request->country_code;
		}
        $user->save();

        $role=Role::where('label',$request->user_type)->first();
        $user->roles()->attach($role);

        $result=make_null($user);
		
                $subject = RT::rtext("mail_subject_parent_register_welcome");
                
                $mdata = ['action'=>'parent_registration','subject'=>$subject,'user'=>$user,'view'=>'email.parent-registration','to'=>$user->email];
                SendEmailJob::dispatch($mdata)->onConnection('database')->onQueue('emails');
			
				

        return response()->json([
            'result' => $result,
            'message' => RT::rtext("success_parent_register"),
            'success' => true,
            'status' => 200,
        ],200);

    }

    public function update(Request $request)
    {
		$user= JWTAuth::touser($request->header('authorization'));
        $rules = array(
            'phone'=>'regex:/^(?=.*[0-9])[ +()0-9]+$/|unique:users,phone,'.$user->id,			
        );
		$val_msg = [
			'phone.unique'=>RT::rtext("warning_require_unique_phone_number")
		];
        $validator = \Validator::make($request->all(), $rules,$val_msg);

        if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json([
                'message' =>$messages,
                'success' => false,
                'status' => $code],RESPONCE_ERROR_CODE);
        }
        
        
        $input = $request->all();
        $input['first_name']=($request->first_name)?$request->first_name:$user->first_name;
        $input['last_name']=($request->last_name)?$request->last_name:$user->last_name;
        $input['phone']=($request->phone)?$request->phone:$user->phone;
        
        /*if(isset($input['image']) && $input['image'] != ''){
            if(file_exists('uploads/user/thumbnail/'.$user->image)){
                unlink('uploads/user/thumbnail/'.$user->image); //delete previously uploaded Attachment
            }
            if(file_exists('uploads/user/'.$user->image)){
                unlink('uploads/user/'.$user->image); //delete previously uploaded Attachment
            }
            $imageName = str_replace(' ', '_', $input['name']).'_'.uniqid(time()) . '.' . $input['image']->getClientOriginalExtension();

            uploadImage($input['image'],'uploads/user/thumbnail',$imageName,'150','150');
            uploadImage($input['image'],'uploads/user',$imageName,'400','400');
            
            $input['image'] = $imageName;
        }*/
		$change_phone = 0;
		if($user->phone != $request->phone){ $change_phone = 1;	}
		if($request->has('country_code') && $request->get('country_code') != "" && $user->country_code != $request->country_code ){ 
			$change_phone = 1;	
			$input['country_code'] = $request->country_code;
		}
		
		if($change_phone == 1){
			$input['phone_verified'] = 0;
		}


        $user->update($input);

        $result=(Object)make_null($user);

        return response()->json([
            'result' => $result,
            'message' => RT::rtext("success_parent_profile_updated"),
            'success' => true,
            'status' => 200,        ],200);
    }

    public function viewChild(Request $request)
    {
        $setting = Setting::pluck('value','key')->toArray();
        
        $user= JWTAuth::touser($request->header('authorization'));
        $child = User::select([
                'users.*',
            ])->where('parent_id',$user->id)->orderby('created_at','DESC')->paginate($setting['pagination']);

        foreach ($child as $key => $value) {
            $order=Order::where('child_id',$value->id)->where("trans_status",'active')->orderby('updated_at','DESC')->first();

            $value['current_subscription']="";
            if($order){
                $value['subscription_id']=$order->subscription_id;
                $value['start_date']=$order->start_date;
                $value['expiry_date']=$order->expiry_no_date;
				$value['start_date_formated']=$order->start_date_formated;
                $value['expiry_date_formated']=$order->expiry_date_formated;
                $value['next_pay']=$order->next_pay;
                $value['order_status']=$order->status;

                if($order->plan){
                    $value['current_subscription'] = $order->plan;
                }
            }else{
                $value['subscription_id']=0;
                $value['order_status']=0;
                $value['start_date']="";
                $value['expiry_date']="";
                $value['next_pay']="";
            }
        }
        
        $result=(Object)make_null($child);

        return response()->json([
            'total' => get_api_data(isset($result->total) ? $result->total : 0),
            'current_page' => get_api_data(isset($result->current_page) ? $result->current_page : 0),
            'prev_page_url' => get_api_data(isset($result->prev_page_url) ? $result->prev_page_url : ''),
            'next_page_url' => get_api_data(isset($result->next_page_url) ? $result->next_page_url : ''),
            'result' => $result,
            'message' => 'Child data.',
            'success' => true,
            'status' => 200,
        ]);
    }

    public function parentDetail(Request $request)
    {
        $user= JWTAuth::touser($request->header('authorization'));
        
        $result=(Object)make_null($user);

        return response()->json([
            'result' => $result,
            'message' => 'Parent Detail.',
            'success' => true,
            'status' => 200,        ],200);
    }

    public function childDetail(Request $request)
    {
        $user= JWTAuth::touser($request->header('authorization'));
        
        $child= User::select([
                'users.*',
                
            ])
            ->where('id',$request->child_id)
            ->where('parent_id',$user->id)
            ->first();

        if(!$child){
            return response()->json([
                'result' => (Object)[],
                'message' => RT::rtext("warning_user_data_not_found"),
                'success' => false,
                'status' => 400,        ],RESPONCE_ERROR_CODE);            
        }

        $result=(Object)make_null($child);
        $order=Order::where('child_id',$child->id)->where("trans_status",'active')->orderby('updated_at','DESC')->first();
        
        $result->current_subscription = "";
        if($order && $order->plan){
            $result->current_subscription = $order->plan;
            $result->start_date = $order->start_date;
            $result->expiry_date = $order->expiry_no_date;
            $result->next_pay = $order->next_pay;
        }
        
        return response()->json([
            'result' => $result,
            'message' => 'Child Detail.',
            'success' => true,
            'status' => 200,],200);
    }

    public function orderDetail(Request $request)
    {
        $setting = Setting::pluck('value','key')->toArray();

        $user= JWTAuth::touser($request->header('authorization'));
        $order = Order::select([
                            'orders.id as order_id',
                            'users.id as user_id',
                            'orders.created_at',
                            'orders.updated_at',
                            'users.username','users.first_name','users.last_name','users.stripe_id','users.dob','users.gender','users.batch_id','users.status as user_status',
                            'orders.*',
                            DB::raw('(CASE WHEN users.image IS NULL THEN 
                           ""
                            ELSE (CONCAT("'.url('/').'/uploads/user/",users.image )) END) AS user_image'),
                            \DB::raw('(CASE WHEN image != "" THEN CONCAT( "'.url('').'",CONCAT("/uploads/user/thumbnail/"),image) ELSE "" END) AS thumb_image'),
                            DB::raw('(CASE WHEN dob != "" THEN TIMESTAMPDIFF(YEAR, dob, CURDATE())ELSE "" END) AS child_age')
                            
                        ])->join('users',function($join){
                            $join->on('orders.child_id','=','users.id');
                        })
                        ->where('orders.parent_id',$user->id)
                        //->where('orders.status',1)
                        ->orderby('orders.created_at','DESC')
                        ->paginate($setting['pagination']);
    
                    

        if(!$order){
            return response()->json([
                'result' => (Object)[],
                'message' => RT::rtext("warning_order_detail_not_found"),
                'success' => false,
                'status' => 400,],RESPONCE_ERROR_CODE);            
        }
        $data = [];
        $i=0;

        $order_data=$order->groupBy(function($d) {
            return Carbon::parse($d->created_at)->format('m');
         });
        foreach ($order_data as $key => $value) {

            $data[$i]['month'] = date("F", mktime(0, 0, 0, $key, 10));
            $data[$i]['data']= $value->toArray();
            $i++;
        }
    
        $result=$order->toArray();
   
        return response()->json([
            'total' => get_api_data(isset($result['total']) ? $result['total'] : 0),
            'current_page' => get_api_data(isset($result['current_page']) ? $result['current_page'] : 0),
            'prev_page_url' => get_api_data(isset($result['prev_page_url']) ? $result['prev_page_url'] : ''),
            'next_page_url' => get_api_data(isset($result['next_page_url']) ? $result['next_page_url'] : ''),
            'result' => $data,
            'message' => 'Order Data.',
            'success' => true,
            'status' => 200,
        ]);
    }

    function getNextOrderNumber()
    {
        // Get the last created order
        $lastOrder =Order::orderBy('created_at', 'desc')->first();

        if ($lastOrder != null)
            $number = $lastOrder->id;
        else 
            $number =0;

        return 'PO' . sprintf('%05d', intval($number) + 1);
    }

    public function childRegister(Request $request)
    {
        $onetime_charge = \config('admin.one_time_fee');
        $stripe_acount_type = "live";
        $country_code = "+1";
		
        $rules = array(
            'first_name'=>'required',
            'last_name'=>'required',
            'gender'=>'required|in:male,female',
            'dob'=>'required|date_format:Y-m-d',
            'username' => 'required|unique:users',
            'password' => 'required|same:confirm_password',
            'phone' => 'required|regex:/^(?=.*[0-9])[ +()0-9]+$/',
            'school_name' => 'required',
            'school_district_no' => 'required',
            'state' => 'required',
            'plan_id' => 'required',
            'image' => 'nullable|mimes:jpg,jpeg,png',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json([
                'message' =>$messages,
                'success' => false,
                'status' => $code],RESPONCE_ERROR_CODE);
        }
        
        $user= JWTAuth::touser($request->header('authorization'));
        
       // \DB::beginTransaction();

        $plan = Plan::find($request->plan_id);
        if(!$plan){
            return response()->json([
                'result' => (Object)[],
                'message' => 'Plan not exist.',
                'success' => false,
                'status' => 400,
            ],RESPONCE_ERROR_CODE);
        }
        
        $result = (Object)[];
        $message = '';
        $code = 400;
        $status = "false";
        $token = "";
        
        
			$SECRET_KEY = \config('admin.stripe.SECRET_KEY');
			if($request->school_district_no == "CITRUS07"){ 
                $SECRET_KEY = \config('admin.stripe_citrus.SECRET_KEY'); 
                $stripe_acount_type = "local";
                $country_code = "+91";
            }
			
			
            if($request->school_district_no == "CITRUS007"){
				$charge = ["id"=>""];
                $customer = ["id"=>""];
                $country_code = "+91";
				$stripe_acount_type = "local";
				
			}else{
				$stripe = \Stripe::make($SECRET_KEY);
				
				
				/*if($user->stripe_id && $user->stripe_id != ""){
					 $customer = ["id"=>$user->stripe_id];
				}else{
					$customer = $stripe->customers()->create([
						'email' => $user->username,
					]);
					
					$user->stripe_id = $customer['id'];
					$user->save();
				} */
				
				$customer = $stripe->customers()->create([
					'email' => $request->username,
				]);
				
				if($request->has('token_id')){
					$token = $request->token_id;
					$cardRes = $stripe->cards()->create($customer['id'], $token);
					
					if(isset($cardRes['id'])){
						
						$customerupdate = $stripe->customers()->update($customer['id'], [
							'default_source' => $cardRes['id']
						]);
						$cardRes['default'] = 1;
						\App\Cards::saveCard($cardRes,$customer['id']);
					
					}
					
					//echo "-<pre>"; print_r($card); exit;
				}
				
				
				$charge = $stripe->charges()->create([
					'customer' => $customer['id'],
					'currency' => 'USD',
					'amount'   => $onetime_charge,
				]);
			}
			
		//	echo "-<pre>"; print_r($charge); exit;
            //make payment
            if($plan->type != "free")
            {	
				$planid = $plan->plan_uid;
				if($planid == "" || !$planid){
					$planid = $plan->id;
				}
				
				if($request->school_district_no == "CITRUS07"){ $planid="MY_SAFETY_19"; }
				
                $subscription = $stripe->subscriptions()->create($customer['id'], [
                    'plan' => $planid,
                ]);
            }
           else
           {
                $subscription['id']=0;
                $subscription['status']="active";
              //  $subscription['trans_status']="active";
           }
            
            if($subscription && isset($subscription['id']) && $charge && isset($charge['id'])){
                $input = $request->all();
                $input['parent_id'] = $user->id;
                $input['password'] = Hash::make($input['password']);
                $input['batch_id']=uniqid();

                if(isset($input['image']) && $input['image'] != ''){
                    $imageName = str_replace(' ', '_', $input['username']).'_'.uniqid(time()) . '.' . $input['image']->getClientOriginalExtension();

                    uploadImage($input['image'],'uploads/user/thumbnail',$imageName,'150','150');
                    uploadImage($input['image'],'uploads/user',$imageName,'400','400');
                    
                    $input['image'] = $imageName;
                }
				
				if($request->has('country_code') && $request->get('country_code') != "" ){ 
					$country_code = $request->country_code;
				}
		
                $input['stripe_id'] = $customer['id'];
                $input['amount'] = $plan->actual_amount;
                $input['status'] = 2;
                $input['regi_refrence'] = "site";
                $input['stripe_acount_type'] = $stripe_acount_type;
                $input['country_code'] = $country_code;
				
				

                $child = User::create(array_except($input,['user_type','plan_id','payment_method','transaction_id']));
                
                //image with url
                if($child->image){
                    $path = url("/")."/"."uploads/user/";
                    $child->thumb_image = $path."thumbnail/".$child->image;
                    $child->image = $path.$child->image;
                }else{
                    $child->thumb_image = null;
                    $child->image = null;
                }
                $child->amount = $plan->actual_amount;

                $role=Role::where('label','CU')->first();
                $child->roles()->attach($role);

                // order start
                $order['parent_id'] = $user->id;
                $order['child_id'] = $child->id;
                $order['plan_id'] = $plan->id;
                $order['start_date'] = Carbon::now()->format('Y-m-d');
                $date = null;
                if($plan->type == "monthly"){
                    $date = Carbon::now()->addMonths(1);
                }elseif($plan->type == "yearly"){
                    $date = Carbon::now()->addYears(1);
                }elseif($plan->type == "daily"){
                    $date = Carbon::now()->addDays(1);
                }elseif($plan->type == "weekly"){
                    $date = Carbon::now()->addWeeks(1);
                }
                elseif($plan->type == "free"){
                    $date = Carbon::now()->addDays(\config('admin.free_trial_duration'));
                }
                $order['expiry_date'] = $date->subDays(1)->format('Y-m-d');
                $order['next_pay'] = $date->addDays(1)->format('Y-m-d');
                $order['auth_response'] = json_encode($subscription);
                $order['plan_ob'] = json_encode($plan);
                $order['order_no'] = $this->getNextOrderNumber();
                $order['amount'] = $plan->actual_amount;
                $order['subscription_id'] = $subscription['id'];
                $order['trans_status'] = $subscription['status'];
                $order['token'] = $token;
                $order['status'] = 1; 
				$order['one_time_fee'] = $onetime_charge;
				$order['one_time_charge_id'] = $charge['id'];
				$order['one_time_charge_ob'] = json_encode($charge);
				
                $result = Order::create($order);
				$orderob = $result;
				$result->child_name = $request->first_name." ".$request->last_name;
				$result->plann_actual_amount = $plan->actual_amount;
				$result->plann_name = $plan->title;
				$result->plann_type = $plan->type;


                //plan user insert
                $childInput['plan_id'] = $plan->id;
                $childInput['user_id'] = $user->id;
                $childInput['child_id'] = $child->id;
                $childInput['start_date'] = Carbon::now()->format('Y-m-d');
                $childInput['end_date'] = $date->subDays(1)->format('Y-m-d');
                $childInput['subscription_id'] = $subscription['id'];
                $childInput['status'] = 2; // 2 mean in progress
                
                $child->plan = PlanUser::create($childInput);
                //plan user insert end

                //payment start
                /*$transaction['method'] = 'stripe';
                $transaction['amount'] = $request->amount;
                $transaction['transaction_id'] = $request->transaction_id;
                $transaction['status'] = $request->status;

                $child->payment = Payment::create($transaction);*/
                //payment end

                //plan order start
                /*$planOrder['plan_id'] = $request->plan_id;
                $planOrder['order_id'] = $child->order->id;

                $child->planOrder = PlanOrder::create($planOrder);       */ 
                //plan order over

				
                $result=(Object)make_null($result);
                \DB::commit();

				
				$subject = RT::rtext("mail_subject_child_register_success"); 
			
				$mdata = ['action'=>'child_registration','user'=>$user,'subject'=>$subject,'view'=>'email.add-child','to'=>$user->email];
                SendEmailJob::dispatch($mdata)->onConnection('database')->onQueue('emails');
				
				$mdata = ['action'=>'get_invoice','order'=>$orderob,'view'=>'','to'=>"","subject"=>""];
                SendEmailJob::dispatch($mdata)->onConnection('database')->onQueue('emails');
			
                $message = RT::rtext("success_child_register_done");
				// send_sms($child->phone,$message);
                $code = 200;
                $status = 'true';
            }else{
                $message = 'Credential is invalid';
                $code = 400;
                $status = 'false';
            }
        
      
		$rescode = 200;
		if($code != 200){
			$rescode =  RESPONCE_ERROR_CODE;
		}
		
        return response()->json([
            'result' => $result,
            'message' => $message,
            'success' => $status,
            'status' => $code,
        ],$rescode);
    }

    public function updateChild(Request $request)
    {
		
        $user= JWTAuth::touser($request->header('authorization'));
        $rules = array(
            'gender' => 'nullable|in:male,female',
            'image' => 'nullable|mimes:jpg,jpeg,png',
            'child_id' => 'required',
            'phone'=>'regex:/^(?=.*[0-9])[ +()0-9]+$/|unique:users,phone,'.$request->child_id,
        );

        $val_msg = [
			'phone.unique'=>RT::rtext("warning_require_unique_phone_number")
        ];
        
        $validator = \Validator::make($request->all(), $rules, $val_msg);

        if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json([
                'message' =>$messages,
                'success' => false,
                'status' => $code],RESPONCE_ERROR_CODE);
        }
        
        
        $child = User::where('id',$request->child_id)->where('parent_id',$user->id)->first();
        
        if(!$child){
            return response()->json([
                'message' => RT::rtext("warning_user_data_not_found"),
                'success' => false,
                'status' => 400],RESPONCE_ERROR_CODE);
        }
        $input = $request->all();
        // $input['username']=($request->username)?$request->username:$user->username;
        $input['first_name']=($request->first_name)?$request->first_name:$child->first_name;
        $input['last_name']=($request->last_name)?$request->last_name:$child->last_name;
        $input['phone']=($request->phone)?$request->phone:$child->phone;
        $input['school_name']=($request->school_name)?$request->school_name:$child->school_name;
        $input['school_district_no']=($request->school_district_no)?$request->school_district_no:$child->school_district_no;
        $input['state']=($request->state)?$request->state:$child->state;
        $input['dob']=($request->dob)?$request->dob:$child->dob;
        $input['gender']=($request->gender)?$request->gender:$child->gender;
        
        if(isset($input['image']) && $input['image'] != ''){
            if($child->image && file_exists('uploads/user/thumbnail/'.$child->image)){
                unlink('uploads/user/thumbnail/'.$child->image); //delete previously uploaded Attachment
            }
            if($child->image && file_exists('uploads/user/'.$child->image)){
                unlink('uploads/user/'.$child->image); //delete previously uploaded Attachment
            }
            $imageName = str_replace(' ', '_', $child->username).'_'.uniqid(time()) . '.' . $input['image']->getClientOriginalExtension();

            uploadImage($input['image'],'uploads/user/thumbnail',$imageName,'150','150');
            uploadImage($input['image'],'uploads/user',$imageName,'400','400');
            
            $input['image'] = $imageName;
        }

        $path = url("/")."/"."uploads/user/";
        $image = (isset($input['image'])?$input['image']:$child->image);
        if($image){
            $thumbImage = $path."thumbnail/".$image;
            $image = $path.$image;
        }else{
            $thumbImage = null;
            $image = null;
        }

        $child = $child->update(array_except($input,['child_id']));

       
        
        $result=(Object)$input;

        return response()->json([
            'result' => $result,
            'message' => RT::rtext("success_child_profile_update"),
            'success' => true,
            'status' => 200,
        ],200);
    }

    public function subscriptionList(Request $request)
    {
        $plan = Plan::where("status","1")->get();

        if(!count($plan)){
            return response()->json([
                'result' => (Object)[],
                'message' => 'Plan not found.',
                'success' => false,
                'status' => 400,        ],RESPONCE_ERROR_CODE);            
        }

        $result=make_null($plan);

        return response()->json([
            'result' => ['data'=>$result],
            'message' => 'Plan List.',
            'success' => true,
            'status' => 200,        ],200);
    }

    public function unique_child(Request $request)
    {
        $rules = array(
           'user_name'=>'required'
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json([
                'message' =>$messages,
                'success' => false,
                'status' => $code],RESPONCE_ERROR_CODE);
        }
        
        $user = User::where('username',$request->user_name)->first();
        if($user)
        {
            return response()->json([
             
                'message' => RT::rtext("warning_require_unique_user_name"),
                'success' => true,
                'status' => 400,
            ],RESPONCE_ERROR_CODE);
        }
        else
        {
            return response()->json([
               
                'message' => 'Suceess',
                'success' => true,
                'status' => 200,
            ],200);
        }
    }

    public function unique_child_number(Request $request)
    {
        $rules = array(
           'phone'=>'required'
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json([
                'message' =>$messages,
                'success' => false,
                'status' => $code],RESPONCE_ERROR_CODE);
        }
        
        $user = User::where('phone',$request->phone)->first();
        if($user)
        {
            return response()->json([
             
                'message' => RT::rtext("warning_require_unique_phone_number"),
                'success' => true,
                'status' => 400,
            ],RESPONCE_ERROR_CODE);
        }
        else
        {
            return response()->json([
                'message' => 'Suceess',
                'success' => true,
                'status' => 200,
            ],200);
        }
    }

    public function unsubscribe(Request $request)
    {
        $rules = array(
            'stripe_id'=>'required',
            'subscription_id' =>'required'
         );
 
         $validator = \Validator::make($request->all(), $rules, []);
 
         if ($validator->fails())
         {
             $validation = $validator;
             $status = false;
             $code = 400;
             $msgArr = $validator->messages()->toArray();
             $messages = reset($msgArr)[0];
 
             return response()->json(['message' =>$messages,'success' => false,'status' => $code],RESPONCE_ERROR_CODE);
         }

		 
        $order_details=Order::where('orders.subscription_id',$request->subscription_id)->first();
		
        if($order_details && $order_details->child)
        {
			
                $child = $order_details->child;
                $SECRET_KEY = \config('admin.stripe.SECRET_KEY');
                if($child->stripe_acount_type == "local"){ $SECRET_KEY = \config('admin.stripe_citrus.SECRET_KEY'); }

				$stripe=\Stripe::make($SECRET_KEY);
				$subscription = $stripe->subscriptions()->cancel($request->stripe_id, $request->subscription_id, false);
				
				$order_details->unsubscription_ob = json_encode($subscription); 
				
				
				$order_details->status=0; 
				$order_details->trans_status="cancelled"; 
				$order_details->save();
				
				$child->status = 0; $child->save();
				
				
				$plan = json_decode($order_details->plan_ob);
				$subject = RT::rtext("mail_subject_stop_subscription_success");
						$user = $order_details->parent;
						if($user && $plan){
							$mdata = ['action'=>'unsubscription_success','user'=>$user,'subject'=>$subject,'order'=>$order_details,'plan'=>$plan,'view'=>'email.billing.unsubscribe-success','to'=>$user->email];
                            SendEmailJob::dispatch($mdata)->onConnection('database')->onQueue('emails');
						}
			  
			
          
         
            return response()->json([
                'message' => "Success! You have unsubscribed package for child ".$child->first_name." ".$child->last_name,
				'data'=>$child,
                'success' => true,
                'status' => 200,
            ],200);

        }
        else{
            return response()->json(['message' =>'Subscription not found !','success' => false,'status' =>400],RESPONCE_ERROR_CODE);
        }
        

    }

    public function get_notification(Request $request)
    {
        $user= JWTAuth::touser($request->header('authorization'));
		$result = [];
        $notification=Notification::where('receiver_id',$user->id)->where('read_flag',0)->get();
		if($notification){
			$result=make_null($notification);
		}
        
        return response()->json([
            'result' => $result,
            'message' => 'notification data',
            'success' => true,
            'status' => 200,],200);
    }

    public function update_notification(Request $request)
    {
        if($request->has('notification_id') && $request->notification_id == "all"){
            $notification=Notification::find($request->notification_id);
        }else{
            $notification=Notification::find($request->notification_id);
        }
        
        if($notification)
        {
            $notification->read_flag=1;
            $notification->save();
            return response()->json([
                'message' => 'Success',
                'success' => true,
                'status' => 200,],200);
        }
        else{
            return response()->json([
                'result' => (Object)[],
                'message' => 'Notification does not found',
                'success' => false,
                'status' => 400,
            ],RESPONCE_ERROR_CODE); 
        }
     
       
    }
	
}
