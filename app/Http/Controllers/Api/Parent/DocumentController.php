<?php

namespace App\Http\Controllers\Api\Parent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;

use App\Setting;

use JWTAuth;
use Carbon\Carbon as Carbon;
use DB;
use App\Responcetext as RT;


class DocumentController extends Controller
{
    public function addDocument(Request $request)
    {
		$result = [];
        $rules = array(
            'child_id'=>'required',
            'document' => 'nullable|mimes:jpg,jpeg,png,zip,pdf',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json([
                'message' =>$messages,
                'success' => false,
                'status' => $code],RESPONCE_ERROR_CODE);
        }
		
		$user= JWTAuth::touser($request->header('authorization'));
		$child = null;
		if($user){
			$child = User::where('id',$request->child_id)->where('parent_id',$user->id)->first();
		}
		
		if(!$child){
			return response()->json([
                'message' =>RT::rtext("warning_user_data_not_found"),
                'success' => false,
                'status' => $code],RESPONCE_ERROR_CODE);
		}
		
		$input = $request->all();
		// print_r($input);exit;
		

		if ($request->hasFile('file')) {
			$file = $request->file('file');
            $timestamp = uniqid();
            $real_name = $file->getClientOriginalName();
            $name = $timestamp."_".$real_name;
            
           
			uploadModalReferenceFile($file,$real_name,'uploads/document/child/'.$request->child_id,$name,'user_id',$request->child_id,'document');

        }
		
		
		
        return response()->json([
            'result' => $result,
            'message' => RT::rtext("success_document_upload"),
            'success' => true,
            'status' => 200,
        ],200);

    }


   
}
