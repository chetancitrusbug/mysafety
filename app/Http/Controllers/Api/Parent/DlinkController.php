<?php

namespace App\Http\Controllers\Api\Parent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use App\OrderUser;
use App\Order;
use App\PlanUser;
use App\Payment;
use App\PlanOrder;
use App\Setting;
use App\Plan;
use App\Cards;
use App\Subscription;
use App\Billingcycle;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use Carbon\Carbon as Carbon;
use DB;
use App\Notification;

use App\Jobs\SendEmailJob;
use App\Responcetext as RT;

class DlinkController extends Controller
{
    public function register(Request $request)
    {
		
		$result = null;
		
		$user = null;
		if($request->has('email') && $request->get('email') != "" ){ 
			$user = User::where('email',$request->email)->first();		
		}
		
		
        $rules = array(
            'email' => 'required|email|unique:users',
            'password'=>'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'parent_mob_no' => 'required|regex:/^(?=.*[0-9])[ +()0-9]+$/|unique:users,phone',
			'child_first_name'=>'required',
            'child_last_name'=>'required',
            'gender'=>'required|in:male,female',
            'dob'=>'required|date_format:Y-m-d',
            'username' => 'required|unique:users',
            'child_password' => 'required|same:child_confirm_password',
            'phone' => 'required|different:parent_mob_no|regex:/^(?=.*[0-9])[ +()0-9]+$/|unique:users,phone',
            'school_name' => 'required',
            'school_district_no' => 'required',
            'state' => 'required',
            'plan_id' => 'required',
            'image' => 'nullable|mimes:jpg,jpeg,png',
        );
		
		if($user){
            unset($rules['password'],$rules['first_name'],$rules['last_name'],$rules['parent_mob_no'],$rules['email']);
        }
		
		$val_msg = [
			'parent_mob_no.unique'=> RT::rtext("warning_require_unique_phone_number"),
			'phone.unique'=> "Child ".RT::rtext("warning_require_unique_phone_number"),
			'phone.different'=> RT::rtext("warning_require_different_phone_number"),
		];
        $validator = \Validator::make($request->all(), $rules, $val_msg);

        if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json([
                'message' =>$messages,
                'success' => false,
                'status' => $code],RESPONCE_ERROR_CODE);
        }
		$plan = null;
		if($request->plan_id == "yearly"){
			$plan = Plan::where("type",'special_yearly')->first();
		}
		if($request->plan_id == "lifetime"){
			$plan = Plan::where("type",'special_lifetime')->first();
		}
		
        if(!$plan){
            return response()->json([
                'result' => (Object)[],
                'message' => 'Plan not exist.',
                'success' => false,
                'status' => 400,
            ],RESPONCE_ERROR_CODE);
        }

		if(!$user){
			$user=new User();
			$user->first_name=$request->first_name;
			$user->last_name=$request->last_name;
			$user->email=$request->email;
			$user->regi_refrence="special_link";
			$user->password=Hash::make($request->password);
			$user->phone=$request->parent_mob_no;
			$user->status=1;
			if($request->has('country_code') && $request->get('country_code') != "" ){ 
				$user->country_code=$request->country_code;
			}
			$user->save();

			$role=Role::where('label',"PU")->first();
			$user->roles()->attach($role);
			
			$subject = RT::rtext("mail_subject_parent_register_welcome");
			$mdata = ['action'=>'parent_registration','subject'=>$subject,'user'=>$user,'view'=>'email.parent-registration','to'=>$user->email];
			SendEmailJob::dispatch($mdata)->onConnection('database')->onQueue('emails');

		}
        
		
		
		
		//Child data
		$onetime_charge = 0;
        $stripe_acount_type = "live";
        $charge = ["id"=>""];
        $customer = ["id"=>""];
        $country_code = "+91";
		$subscription['id']=0;
        $subscription['status']="active";

		
		if(1){
				$input = $request->all();
                $_input = [];
                $_input['first_name'] = $request->child_first_name;
                $_input['last_name'] = $request->child_last_name;
                $_input['gender'] = $request->gender;
                $_input['dob'] = $request->dob;
                $_input['username'] = $request->username;
                $_input['phone'] = $request->phone;
                $_input['school_name'] = $request->school_name;
                $_input['school_district_no'] = $request->school_district_no;
                $_input['state'] = $request->state;
                $_input['regi_refrence'] = "special_link";
				$_input['parent_id'] = $user->id;
                $_input['password'] = Hash::make($request->child_password);
                $_input['batch_id']=uniqid();

                if(isset($input['image']) && $input['image'] != ''){
                    $imageName = str_replace(' ', '_', $input['username']).'_'.uniqid(time()) . '.' . $input['image']->getClientOriginalExtension();

                    uploadImage($input['image'],'uploads/user/thumbnail',$imageName,'150','150');
                    uploadImage($input['image'],'uploads/user',$imageName,'400','400');
                    
                    $_input['image'] = $imageName;
                }
				
				if($request->has('child_country_code') && $request->get('child_country_code') != "" ){ 
					$country_code = $request->child_country_code;
				}
		
                $_input['stripe_id'] = $customer['id'];
                $_input['amount'] = $plan->actual_amount;
                $_input['status'] = 2;
                $_input['stripe_acount_type'] = $stripe_acount_type;
                $_input['country_code'] = $country_code;
				
				

                $child = User::create($_input);
				
				if ($request->hasFile('file')) {
					$file = $request->file('file');
					$timestamp = uniqid();
					$real_name = $file->getClientOriginalName();
					$name = $timestamp."_".$real_name;
					
					uploadModalReferenceFile($file,$real_name,'uploads/document/child/'.$child->id,$name,'user_id',$child->id,'document');
				}
                
                $role=Role::where('label','CU')->first();
                $child->roles()->attach($role);

                // order start
                $order['parent_id'] = $user->id;
                $order['child_id'] = $child->id;
                $order['plan_id'] = $plan->id;
                $order['start_date'] = Carbon::now()->format('Y-m-d');
                $date = null;
                if($plan->type == "monthly"){
                    $date = Carbon::now()->addMonths(1);
                }elseif($plan->type == "special_lifetime"){
					$date = null;
                }elseif($plan->type == "special_yearly"){
					$date = Carbon::now()->addYears(1);
				}elseif($plan->type == "yearly"){
                    $date = Carbon::now()->addYears(1);
                }elseif($plan->type == "daily"){
                    $date = Carbon::now()->addDays(1);
                }elseif($plan->type == "weekly"){
                    $date = Carbon::now()->addWeeks(1);
                }
                else{
                    $date = Carbon::now()->addDays(\config('admin.free_trial_duration'));
                }
				if($plan->type == "special_lifetime"){
					
					$bod_18 = Carbon::createFromFormat('Y-m-d',$request->dob)->addYears(18);
					$order['expiry_date'] = $bod_18;
					$order['next_pay'] = $bod_18;
				}else{
					$order['expiry_date'] = $date->subDays(1)->format('Y-m-d');
					$order['next_pay'] = $date->addDays(1)->format('Y-m-d');
				}
                
                $order['auth_response'] = json_encode($subscription);
                $order['plan_ob'] = json_encode($plan);
                $order['order_no'] = $this->getNextOrderNumber();
                $order['amount'] = $plan->actual_amount;
                $order['subscription_id'] = $subscription['id'];
                $order['trans_status'] = $subscription['status'];
                $order['token'] = "";
                $order['status'] = 1; 
				$order['one_time_fee'] = $onetime_charge;
				$order['one_time_charge_id'] = $charge['id'];
				$order['one_time_charge_ob'] = json_encode($charge);
				
                $result = Order::create($order);
				
                
                

				
				$subject = RT::rtext("mail_subject_child_register_success"); 
			
				$mdata = ['action'=>'child_registration','user'=>$user,'subject'=>$subject,'view'=>'email.add-child','to'=>$user->email];
                SendEmailJob::dispatch($mdata)->onConnection('database')->onQueue('emails');
				
				
			
                $message = RT::rtext("success_child_register_done");
				// send_sms($child->phone,$message);
                $code = 200;
                $status = 'true';
            }else{
                $message = 'Credential is invalid';
                $code = 400;
                $status = 'false';
            }
			
			
        $rescode = 200;
		if($code != 200){
			$rescode =  RESPONCE_ERROR_CODE;
			
		}
		
		
        return response()->json([
            'result' => $result,
            'message' => $message,
            'success' => $status,
            'status' => $rescode,
        ],$rescode);

    }
	
	
	public function ParentChildRegister(Request $request)
    {
		
		$result = null;
		
		$user = null;
		if($request->has('email') && $request->get('email') != "" ){ 
			$user = User::where('email',$request->email)->first();		
		}
		
		
        $rules = array(
            'email' => 'required|email|unique:users',
            'password'=>'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'parent_mob_no' => 'required|regex:/^(?=.*[0-9])[ +()0-9]+$/|unique:users,phone',
			'child_first_name'=>'required',
            'child_last_name'=>'required',
            'gender'=>'required|in:male,female',
            'dob'=>'required|date_format:Y-m-d',
            'username' => 'required|unique:users',
            'child_password' => 'required|same:child_confirm_password',
            'phone' => 'required|different:parent_mob_no|regex:/^(?=.*[0-9])[ +()0-9]+$/|unique:users,phone',
            'school_name' => 'required',
            'school_district_no' => 'required',
            'state' => 'required',
            'plan_id' => 'required',
            'image' => 'nullable|mimes:jpg,jpeg,png',
        );
		
		if($user){
            unset($rules['password'],$rules['first_name'],$rules['last_name'],$rules['parent_mob_no'],$rules['email']);
        }
		
		$val_msg = [
			'parent_mob_no.unique'=> RT::rtext("warning_require_unique_phone_number"),
			'phone.unique'=> "Child ".RT::rtext("warning_require_unique_phone_number"),
			'phone.different'=> RT::rtext("warning_require_different_phone_number"),
		];
        $validator = \Validator::make($request->all(), $rules, $val_msg);

        if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json([
                'message' =>$messages,
                'success' => false,
                'status' => $code],RESPONCE_ERROR_CODE);
        }
		
		if(!$user){
			$user=new User();
			$user->first_name=$request->first_name;
			$user->last_name=$request->last_name;
			$user->email=$request->email;
			$user->regi_refrence="site";
			$user->password=Hash::make($request->password);
			$user->phone=$request->parent_mob_no;
			$user->status=1;
			if($request->has('country_code') && $request->get('country_code') != "" ){ 
				$user->country_code=$request->country_code;
			}
			$user->save();

			$role=Role::where('label',"PU")->first();
			$user->roles()->attach($role);
			
			$subject = RT::rtext("mail_subject_parent_register_welcome");
			$mdata = ['action'=>'parent_registration','subject'=>$subject,'user'=>$user,'view'=>'email.parent-registration','to'=>$user->email];
			SendEmailJob::dispatch($mdata)->onConnection('database')->onQueue('emails');

		}
        
		
		$plan = null;
		if($request->plan_id != ""){
			$plan = Plan::find($request->plan_id);
		}
		
		$onetime_charge = \config('admin.one_time_fee');
        $stripe_acount_type = "live";
        $country_code = "+1";
		
		$charge = ["id"=>""];
        $customer = ["id"=>""];
        $country_code = "+91";
		$subscription['id']=0;
        $subscription['status']="active";

		    
        
		$SECRET_KEY = \config('admin.stripe.SECRET_KEY');
		if($request->school_district_no == "CITRUS07"){ 
            $SECRET_KEY = \config('admin.stripe_citrus.SECRET_KEY'); $stripe_acount_type = "local";$country_code = "+91";
        }
		if($request->school_district_no == "CITRUS007"){
			$charge = ["id"=>""];$customer = ["id"=>""];$country_code = "+91";$stripe_acount_type = "local";
		}else{
			$stripe = \Stripe::make($SECRET_KEY);
			
			$customer = $stripe->customers()->create([
				'email' => $request->username,
			]);
			
			if($request->has('token_id')){
				$token = $request->token_id;
				$cardRes = $stripe->cards()->create($customer['id'], $token);
				
				if(isset($cardRes['id'])){
					
					$customerupdate = $stripe->customers()->update($customer['id'], [
						'default_source' => $cardRes['id']
					]);
					$cardRes['default'] = 1;
					\App\Cards::saveCard($cardRes,$customer['id']);
				
				}
			}
			$charge = $stripe->charges()->create([
				'customer' => $customer['id'],
				'currency' => 'USD',
				'amount'   => $onetime_charge,
			]);
		}
		
		if($plan->type != "free")
        {	
			$planid = $plan->plan_uid;
			if($planid == "" || !$planid){
				$planid = $plan->id;
			}
			
			if($request->school_district_no == "CITRUS07"){ $planid="MY_SAFETY_19"; }
			
            $subscription = $stripe->subscriptions()->create($customer['id'], [
                'plan' => $planid,
            ]);
        }else{
             $subscription['id']=0;
             $subscription['status']="active";
        }
		
		if($subscription && isset($subscription['id']) && $charge && isset($charge['id'])){
			
				$input = $request->all();
                $_input = [];
                $_input['first_name'] = $request->child_first_name;
                $_input['last_name'] = $request->child_last_name;
                $_input['gender'] = $request->gender;
                $_input['dob'] = $request->dob;
                $_input['username'] = $request->username;
                $_input['phone'] = $request->phone;
                $_input['school_name'] = $request->school_name;
                $_input['school_district_no'] = $request->school_district_no;
                $_input['state'] = $request->state;
                $_input['regi_refrence'] = "site";
				$_input['parent_id'] = $user->id;
                $_input['password'] = Hash::make($request->child_password);
                $_input['batch_id']=uniqid();

                if(isset($input['image']) && $input['image'] != ''){
                    $imageName = str_replace(' ', '_', $input['username']).'_'.uniqid(time()) . '.' . $input['image']->getClientOriginalExtension();

                    uploadImage($input['image'],'uploads/user/thumbnail',$imageName,'150','150');
                    uploadImage($input['image'],'uploads/user',$imageName,'400','400');
                    
                    $_input['image'] = $imageName;
                }
				
				if($request->has('child_country_code') && $request->get('child_country_code') != "" ){ 
					$country_code = $request->child_country_code;
				}
		
                $_input['stripe_id'] = $customer['id'];
                $_input['amount'] = $plan->actual_amount;
                $_input['status'] = 2;
                $_input['stripe_acount_type'] = $stripe_acount_type;
                $_input['country_code'] = $country_code;
				
				if(isset($input['image']) && $input['image'] != ''){
					$imageName = str_replace(' ', '_',$_input['username']).'_'.uniqid(time()) . '.' . $input['image']->getClientOriginalExtension();

					uploadImage($input['image'],'uploads/user/thumbnail',$imageName,'150','150');
					uploadImage($input['image'],'uploads/user',$imageName,'400','400');
					
					$_input['image'] = $imageName;
				}
				

                $child = User::create($_input);
				
				if ($request->hasFile('file')) {
					$file = $request->file('file');
					$timestamp = uniqid();
					$real_name = $file->getClientOriginalName();
					$name = $timestamp."_".$real_name;
					
					uploadModalReferenceFile($file,$real_name,'uploads/document/child/'.$child->id,$name,'user_id',$child->id,'document');
				}
                
                $role=Role::where('label','CU')->first();
                $child->roles()->attach($role);

                // order start
                $order['parent_id'] = $user->id;
                $order['child_id'] = $child->id;
                $order['plan_id'] = $plan->id;
                $order['start_date'] = Carbon::now()->format('Y-m-d');
                $date = null;
                if($plan->type == "monthly"){
                    $date = Carbon::now()->addMonths(1);
                }elseif($plan->type == "special_lifetime"){
					$date = null;
                }elseif($plan->type == "special_yearly"){
					$date = Carbon::now()->addYears(1);
				}elseif($plan->type == "yearly"){
                    $date = Carbon::now()->addYears(1);
                }elseif($plan->type == "daily"){
                    $date = Carbon::now()->addDays(1);
                }elseif($plan->type == "weekly"){
                    $date = Carbon::now()->addWeeks(1);
                }
                else{
                    $date = Carbon::now()->addDays(\config('admin.free_trial_duration'));
                }
				if($plan->type == "special_lifetime"){
					
					$bod_18 = Carbon::createFromFormat('Y-m-d',$request->dob)->addYears(18);
					$order['expiry_date'] = $bod_18;
					$order['next_pay'] = $bod_18;
				}else{
					$order['expiry_date'] = $date->subDays(1)->format('Y-m-d');
					$order['next_pay'] = $date->addDays(1)->format('Y-m-d');
				}
                
                $order['auth_response'] = json_encode($subscription);
                $order['plan_ob'] = json_encode($plan);
                $order['order_no'] = $this->getNextOrderNumber();
                $order['amount'] = $plan->actual_amount;
                $order['subscription_id'] = $subscription['id'];
                $order['trans_status'] = $subscription['status'];
                $order['token'] = "";
                $order['status'] = 1; 
				$order['one_time_fee'] = $onetime_charge;
				$order['one_time_charge_id'] = $charge['id'];
				$order['one_time_charge_ob'] = json_encode($charge);
				
                $result = Order::create($order);
				
                
                

				
				$subject = RT::rtext("mail_subject_child_register_success"); 
			
				$mdata = ['action'=>'child_registration','user'=>$user,'subject'=>$subject,'view'=>'email.add-child','to'=>$user->email];
                SendEmailJob::dispatch($mdata)->onConnection('database')->onQueue('emails');
				
				$mdata = ['action'=>'get_invoice','order'=>$result,'view'=>'','to'=>"","subject"=>""];
                SendEmailJob::dispatch($mdata)->onConnection('database')->onQueue('emails');
			
                $message = RT::rtext("success_child_register_done");
				// send_sms($child->phone,$message);
                $code = 200;
                $status = 'true';
            }else{
                $message = 'Credential is invalid';
                $code = 400;
                $status = 'false';
            }
			
			
        $rescode = 200;
		if($code != 200){
			$rescode =  RESPONCE_ERROR_CODE;
			
		}
		
		
        return response()->json([
            'result' => $result,
            'message' => $message,
            'success' => $status,
            'status' => $rescode,
        ],$rescode);

    }
	
	function getNextOrderNumber()
    {
        // Get the last created order
        $lastOrder =Order::orderBy('created_at', 'desc')->first();

        if ($lastOrder != null)
            $number = $lastOrder->id;
        else 
            $number =0;

        return 'PO' . sprintf('%05d', intval($number) + 1);
    }
	
	public function uniqueField($field , Request $request)
    {
		if( $field == "parent_mob_no"){
			$field = "phone";
		}
        $user = User::where($field,$request->field_value)->first();
		
		$on_exist = false;
		
		if($request->has('on_exist') && $request->get('on_exist') != "" && $request->get('on_exist') != "false" ){ 
					$on_exist = true;
		}
		if($user)
        {
			if($on_exist){
				return response()->json([
					'message' => 'Suceess',
					'data' => $user,
					'success' => true,
					'status' => 200,
				],200);
			}else{
				return response()->json([
             
					'message' => RT::rtext("warning_require_unique_field",['field'=>$field]),
					'success' => true,
					'status' => 400,
				],RESPONCE_ERROR_CODE);
			}
            
        }
        else
        {
           if(!$on_exist){
				return response()->json([
					'message' => 'Suceess',
					'success' => true,
					'status' => 200,
				],200);
			}else{
				return response()->json([
             
					'message' => "No data found",
					'success' => true,
					'status' => 400,
				],RESPONCE_ERROR_CODE);
			}
        }
    }

    

    
    

    
}
