<?php

namespace App\Http\Controllers\Api\Parent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Twilio\Twiml;
use Twilio\TwiML\VoiceResponse;

use JWTAuth;
use App\User;
use App\VerifyUser;
use App\Responcetext as RT;


require storage_path('package/vendor/autoload.php');

use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;

class TwillioController extends Controller
{
	
	public function getPhoneVerificationOtp(Request $request){
		$result = [];
		$code = 200;  $message = ""; 
		
		$user= JWTAuth::touser($request->header('authorization'));
		if(!$user){
			$message = RT::rtext("warning_user_data_not_found");;
            $code = 400;
		}else{
			
			$phone = $user->country_code.$user->phone;
			$this->deleteFb($user->id);
			$VerifyUser = VerifyUser::where('user_id',$user->id)->where('phone_number',$phone)->first();
			if(!$VerifyUser){
				$VerifyUser = new VerifyUser();
			}
			$token = substr(number_format(time() * rand(),0,'',''),0,4);
			$VerifyUser->try_count = 0;
			$VerifyUser->user_id = $user->id;
			$VerifyUser->phone_number = $phone;
			$VerifyUser->token = $token;
			$VerifyUser->save();
			
			$result['issue_to_connect_call'] = RT::rtext("issue_to_connect_call");
			$result['enter_digit'] = RT::rtext("enter_digit");
			$result['we_have_initiated_verification_call'] = RT::rtext("we_have_initiated_verification_call");
			
			$result['phone_number'] = $phone;
			$result['otp'] = $token;
			$result['user_id'] = $user->id;
			$result['phone'] = $user->phone;
			$result['country_code'] = $user->country_code;
		}
		
		$res_code = 200;
        $status = true;
        if($code != 200){
            $res_code = RESPONCE_ERROR_CODE;
            $status = false;
        }

        return response()->json([
            'result' => $result,
            'message' => $message,
            'success' => $status,
            'status' => $code,
        ],$res_code);
	}
	
    public function phoneVerificationCall(Request $request){
		
		$result = [];
		$code = 200;  $message = ""; 
		
		$user= JWTAuth::touser($request->header('authorization'));
		
		$rules = array(
            'country_code'=>'required',
            'phone'=>'required|regex:/^(?=.*[0-9])[ +()0-9]+$/|unique:users,phone,'.$user->id,
            'otp'=>'required',
        );
		
		
        
		$val_msg = [
			'phone.unique'=>RT::rtext("warning_require_unique_phone_number")
		];

        $validator = \Validator::make($request->all(), $rules,$val_msg);
		
		if ($validator->fails())
        {
            $validation = $validator;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
			
		}else{
			
			if(!$user){
				$message = RT::rtext("warning_user_data_not_found");;
				$code = 400;
			}else{
				$user->country_code = $request->country_code;
				$user->phone = $request->phone;
				$user->save();
				
				$phone = $request->country_code.$request->phone;
				$VerifyUser = VerifyUser::where('user_id',$user->id)->where('token',$request->otp)->first();
				if(!$VerifyUser){
					
					$message = RT::rtext("warning_no_data_found");;
					$code = 400;
					
				}else{
					VerifyUser::where('user_id',"!=",$user->id)->where('phone_number',$phone)->delete();
					
					$VerifyUser->phone_number = $phone;
					$VerifyUser->try_count = 1;
					$VerifyUser->save();
					
					$this->initVerificationCall($phone);
					
					$message = RT::rtext("verification_call_initiated");
					$code = 200;
				}
				
			}
		}
		
		$res_code = 200;
        $status = true;
        if($code != 200){
            $res_code = RESPONCE_ERROR_CODE;
            $status = false;
        }

        return response()->json([
            'result' => $result,
            'message' => $message,
            'success' => $status,
            'status' => $code,
        ],$res_code);
	}
    
	
    public function initVerificationCall($phoneno)
    {

		$sid = \config('admin.twilio.SID'); // Your Account SID from www.twilio.com/console
        $token = \config('admin.twilio.TOKEN'); // Your Auth Token from www.twilio.com/console

        $client = new \Twilio\Rest\Client($sid, $token);
		
		$call = $client->calls->create(
			$phoneno, 
			\config('admin.twilio.FROM'),
			array(
				'url' => url("verification/voice.xml"),
				"statusCallbackMethod" => "POST",
				"statusCallback" => url('api/call-end'),
				"statusCallbackEvent" => array("completed")
			)
        );
	}
	
	public function callresponce(Request $request)
    {
		
		$fail = 0;
		$response = new Twiml();
        
		if($request->has('Digits') && $request->has('Called')){
			$token = $request->input('Digits');
			$phone_number = $request->input('Called');
			
			$VerifyUser = VerifyUser::where('token',$token)->where('phone_number',$phone_number)->first();
			
			if(!$VerifyUser){
				$VerifyUser = VerifyUser::where('phone_number',$phone_number)->orderby("updated_at","desc")->first();
				
				if(!$VerifyUser || $VerifyUser->try_count >= 2){
					$VerifyUser->try_count = 0;
					$VerifyUser->save();
				
					$response->say("Verification code incorrect, please try again.");
					$fail = 1;
				}else{
					$VerifyUser->try_count = $VerifyUser->try_count +1;
					$VerifyUser->save();
					
					$gather = $response->gather(
						[
							'numDigits' => 4,
							'action' => "call-response",
						]
					);
					$gather->say("Verification code incorrect, please try again.");
				}
				
		
			}else{
				
				$VerifyUser->try_count = 0;
				$VerifyUser->save();
				
				$response->say("Thank you! Your phone number has been verified.");
				$user = User::where('id',$VerifyUser->user_id)->first();
				
				
				
				if($user){
					$user->phone_verified = 1;
					$user->save();
					
					$fail = 0;
					
					$serviceAccount = ServiceAccount::fromJsonFile(storage_path('files/mysafetynet-f1902-firebase-adminsdk-j8zry-8e36388209.json'));
					$firebase 		  = (new Factory)
                        ->withServiceAccount($serviceAccount)
                        ->withDatabaseUri('https://mysafetynet-f1902.firebaseio.com/')
                        ->create();
						$database = $firebase->getDatabase();
						$newPost  = $database->getReference('phone-verification/'.$VerifyUser->user_id)
								  ->set(["user_id"=>$VerifyUser->user_id,"verified"=>1,'phone_number'=>$phone_number,'updated_at'=>date('H:i:s')]);
				}
			}
		}else{
			//$fail = 1;
		}
		
		if($fail == 1){
			
			if($request->has('Called')){
				$phone_number = $request->input('Called');
				$VerifyUser = VerifyUser::where('phone_number',$phone_number)->orderby("updated_at","desc")->first();
				
				if($VerifyUser){
					
					$VerifyUser->try_count = 0;
					$VerifyUser->save();
					
					$serviceAccount = ServiceAccount::fromJsonFile(storage_path('files/mysafetynet-f1902-firebase-adminsdk-j8zry-8e36388209.json'));
					$firebase 		  = (new Factory)
                        ->withServiceAccount($serviceAccount)
                        ->withDatabaseUri('https://mysafetynet-f1902.firebaseio.com/')
                        ->create();
						$database = $firebase->getDatabase();
						$newPost  = $database->getReference('phone-verification/'.$VerifyUser->user_id)
								  ->set(["user_id"=>$VerifyUser->user_id,"verified"=>0,'phone_number'=>$phone_number,'updated_at'=>date('H:i:s')]);
				}
			}
			
			
		
			
		}
		return $response;
        
	}
	public function callStatus(Request $request)
    {
			/*		\Mail::raw(json_encode($request->all()), function ($message) {
  $message->to("jignesh.citrusbug@gmail.com")
    ->subject("Call test1");
});*/

		if($request->has('Called') && $request->has('CallStatus') && ($request->CallStatus == "no-answer" || $request->CallStatus == "failed")){
				$phone_number = $request->input('Called');
				$VerifyUser = VerifyUser::where('phone_number',$phone_number)->orderby("updated_at","desc")->first();
				
				if($VerifyUser && $VerifyUser->try_count > 0){
					
					$VerifyUser->try_count = 0;
					$VerifyUser->save();
					
					$serviceAccount = ServiceAccount::fromJsonFile(storage_path('files/mysafetynet-f1902-firebase-adminsdk-j8zry-8e36388209.json'));
					$firebase 		  = (new Factory)
                        ->withServiceAccount($serviceAccount)
                        ->withDatabaseUri('https://mysafetynet-f1902.firebaseio.com/')
                        ->create();
						$database = $firebase->getDatabase();
						$newPost  = $database->getReference('phone-verification/'.$VerifyUser->user_id)
								  ->set(["user_id"=>$VerifyUser->user_id,"verified"=>0,'phone_number'=>$phone_number,'updated_at'=>date('H:i:s')]);
				}
			}
	}
	public function deleteFb($userid){
		   $serviceAccount = ServiceAccount::fromJsonFile(storage_path('files/mysafetynet-f1902-firebase-adminsdk-j8zry-8e36388209.json'));
			$firebase 		  = (new Factory)
							->withServiceAccount($serviceAccount)
							->withDatabaseUri('https://mysafetynet-f1902.firebaseio.com/')
							->create();
			$database 		= $firebase->getDatabase();
			$newPost 		  = $database
								->getReference('phone-verification/'.$userid)
								->remove();
	}
	
	/*public function askforotp(Request $request)
    {
		 $response = new Twiml();
        $gather = $response->gather(
            [
                'numDigits' => 1,
                'action' => "call-response",
            ]
        );
        $gather->say('Please press 1 for directions. Press 2 for a ');
		//return $response;
		header('Content-Type: text/xml');
        return response($response, 200)->header('Content-Type', 'application/xml');
	}*/
	

   
}
