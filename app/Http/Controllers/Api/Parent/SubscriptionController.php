<?php

namespace App\Http\Controllers\Api\Parent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use App\OrderUser;
use App\Order;
use App\PlanUser;
use App\Payment;
use App\PlanOrder;
use App\Setting;
use App\Plan;
use App\Subscription;
use App\Billingcycle;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use Carbon\Carbon as Carbon;
use DB;
use App\Notification;
use App\Responcetext as RT;



use App\Jobs\SendEmailJob;

class SubscriptionController extends Controller
{
    
    

    

    public function upgradeSubscription(Request $request)
    {
		
		try{
			
        $code = 200;  $message = ""; $result =[];

        $rules = array(
            'child_id'=>'required',
            'plan_id' => 'required'
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails())
        {
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
            $code = 400;
        }else{
            $user= JWTAuth::touser($request->header('authorization'));
            $child = User::where('id',$request->child_id)->where('parent_id',$user->id)->first();
            $plan = Plan::find($request->plan_id);
        
            if(!$user || !$child || !$plan) {
                $code = 400; $message = RT::rtext("warning_user_data_not_found");
				
            }
        }
        
        if($code != 400){
        
        
            $SECRET_KEY = \config('admin.stripe.SECRET_KEY');
            if($child->stripe_acount_type == "local"){ 
				$SECRET_KEY = \config('admin.stripe_citrus.SECRET_KEY');
				$plan = Plan::where("plan_uid","MY_SAFETY_19")->first();
			}
			
            $stripe=\Stripe::make($SECRET_KEY);

           
			
			$planid = $plan->plan_uid;
			if($planid == "" || !$planid){
				$planid = $plan->id;
			}
            $subscription = $stripe->subscriptions()->create($child->stripe_id, ['plan' => $planid]);

			
			//echo "<pre>"; print_r($subscription);
            if($subscription && isset($subscription['id'])){
               
                if($child->status == 0) $child->status = 1;
				
				$child->save();
                
                if($plan->type == "monthly"){
                    $date = Carbon::now()->addMonths(1);
                }elseif($plan->type == "yearly"){
                    $date = Carbon::now()->addYears(1);
                }elseif($plan->type == "daily"){
                    $date = Carbon::now()->addDays(1);
                }
				elseif($plan->type == "weekly"){
                    $date = Carbon::now()->addWeek(1);
                }
                elseif($plan->type == "free"){
                    $date = Carbon::now()->addDays(\config('admin.free_trial_duration'));
                }

                $order['parent_id'] = $user->id;
                $order['child_id'] = $child->id;
                $order['plan_id'] = $plan->id;

                $order['start_date'] = Carbon::now()->format('Y-m-d');
                $order['expiry_date'] = $date->subDays(1)->format('Y-m-d');
                $order['next_pay'] = $date->addDays(1)->format('Y-m-d');
                $order['auth_response'] = json_encode($subscription);
				$order['plan_ob'] = json_encode($plan);
                $order['order_no'] = Order::getNextOrderNumber();
                $order['amount'] = $plan->actual_amount;
                $order['subscription_id'] = $subscription['id'];
                $order['trans_status'] = $subscription['status'];
                $order['token'] = "";
                $order['status'] = 1; 
                $result = Order::create($order);

				//echo "<pre>"; print_r($order);
                $this->unsubscribeCurrentPlanByUserId($child->id,$result->id);
                
                \DB::commit();
				
                $message = RT::rtext("success_plan_upgration");
                $code = 200;
				
						$plan = json_decode($result->plan_ob);
						$subject = RT::rtext("mail_subject_upgrate_plan_success");
						$user = $result->parent;
						if($user && $plan){

                            $mdata = ['action'=>'subscription_upgrated','user'=>$user,'subject'=>$subject,'order'=>$result,'plan'=>$plan,'view'=>'email.billing.upgrade-plan-success','to'=>$user->email];
                            SendEmailJob::dispatch($mdata)->onConnection('database')->onQueue('emails');
							
							$mdata = ['action'=>'get_invoice','order'=>$result,'view'=>'','to'=>"","subject"=>""];
                            SendEmailJob::dispatch($mdata)->onConnection('database')->onQueue('emails');
                        }
            }else{
                $message = 'Credential is invalid';
                $code = 400;
            }
        

    }
	
	}catch(\Exception $e){
			$message = $e->getMessage();
			$code = 400;
	}
      
        $res_code = 200;
        $status = true;
        if($code != 200){
            $res_code = RESPONCE_ERROR_CODE;
            $status = false;
        }
         
        return response()->json([
            'result' => $result,
            'message' => $message,
            'success' => $status,
            'status' => $code,
        ],$res_code);
    }
	
	public function setDefaultCard(Request $request)
    {
		$rules = array(
            'card_id'=>'required'
        );
		$result = null;
        $validator = \Validator::make($request->all(), $rules, []);
		
		if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
		}else{
			
			$user= JWTAuth::touser($request->header('authorization'));
			if($request->has('child_id')){
				$child = User::where('id',$request->child_id)->where('parent_id',$user->id)->first();
			}else{
				$child = User::where('stripe_id',$request->stripe_id)->where('parent_id',$user->id)->first();
			}
            
			
			$SECRET_KEY = \config('admin.stripe.SECRET_KEY');
            if($child->stripe_acount_type == "local"){ $SECRET_KEY = \config('admin.stripe_citrus.SECRET_KEY'); }
            $stripe=\Stripe::make($SECRET_KEY);
			
					$savecard = \App\Cards::where("stripe_id",$child->stripe_id)->where("id",$request->card_id)->first();
					
					if($savecard){
						$cardRes = $stripe->customers()->update($child->stripe_id, [
							'default_source' => $savecard->card_id
						]);
						
						if($cardRes){
							\App\Cards::where("stripe_id",$child->stripe_id)->update(['default'=>0]);
							$savecard->update(['default'=>1]);
							
							$result = User::select(['id','username','first_name','last_name','stripe_id','stripe_acount_type'])
						->with('cards')
						->where('id',$child->id)->orderby('created_at','DESC')->first();
						
							$messages = RT::rtext("success_parent_card_updated_for_child");
							$status = true;
							$code = 200;
						}else{
							$messages = RT::rtext("error_default");
							$status = true;
							$code = 400;
						}
					}else{
						$messages = RT::rtext("error_default");
							$status = true;
							$code = 400;
					}
		}
		
		
		$rescode = 200;
		if($code != 200){
			$rescode =  RESPONCE_ERROR_CODE;
		}
		
		return response()->json([
                'message' =>$messages,
                'success' => $status,
                'result' => $result,
                'status' => $code],$rescode);
       
        
    }
	
	public function removeCard(Request $request)
    {
		$result = null;
		$rules = array(
            'card_id'=>'required'
        );

        $validator = \Validator::make($request->all(), $rules, []);
		
		if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
		}else{
			
			$user= JWTAuth::touser($request->header('authorization'));
            if($request->has('child_id')){
				$child = User::where('id',$request->child_id)->where('parent_id',$user->id)->first();
			}else{
				$child = User::where('stripe_id',$request->stripe_id)->where('parent_id',$user->id)->first();
			}
			
			$SECRET_KEY = \config('admin.stripe.SECRET_KEY');
            if($child->stripe_acount_type == "local"){ $SECRET_KEY = \config('admin.stripe_citrus.SECRET_KEY'); }
            $stripe=\Stripe::make($SECRET_KEY);
			
					$savecard = \App\Cards::where("stripe_id",$child->stripe_id)->where("id",$request->card_id)->first();
					
					if($savecard){
						$cardRes = $stripe->cards()->delete($child->stripe_id,$savecard->card_id);
						
						if($cardRes){
							
							$savecard->delete();
							
							$result = User::select(['id','username','first_name','last_name','stripe_id','stripe_acount_type'])
							->with('cards')
							->where('id',$child->id)->orderby('created_at','DESC')->first();
						
							
							$messages = RT::rtext("success_parent_card_removed_for_child");
							$status = true;
							$code = 200;
						}else{
							$messages = RT::rtext("error_default");
							$status = true;
							$code = 400;
						}
					}else{
						$messages = RT::rtext("error_default");
							$status = true;
							$code = 400;
					}
		}
		
		
		$rescode = 200;
		if($code != 200){
			$rescode =  RESPONCE_ERROR_CODE;
		}
		
		return response()->json([
                'message' =>$messages,
                'success' => $status,
                'result' => $result,
                'status' => $code],$rescode);
       
        
    }
	public function getMyCards(Request $request)
    {
		$user= JWTAuth::touser($request->header('authorization'));
		
		$result = User::select(['id','username','first_name','last_name','stripe_id','stripe_acount_type'])
				->with('cards')
				->where('parent_id',$user->id)->orderby('created_at','DESC')->get();

        return response()->json([
                'message' =>"Card List",
                'success' => true,
                'result' => $result,
                'status' => 200],200);
        
	}
    public function addCard(Request $request)
    {
		$rules = array(
            'child_id'=>'required',
            'token_id'=>'required'
        );
		$result = null;

        $validator = \Validator::make($request->all(), $rules, []);
		
		if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
		}else{
			
			$user= JWTAuth::touser($request->header('authorization'));
            $child = User::where('id',$request->child_id)->where('parent_id',$user->id)->first();
			
			$SECRET_KEY = \config('admin.stripe.SECRET_KEY');
            if($child->stripe_acount_type == "local"){ $SECRET_KEY = \config('admin.stripe_citrus.SECRET_KEY'); }
            $stripe=\Stripe::make($SECRET_KEY);
			
			if(!$child->stripe_id || $child->stripe_id == ""){
				$customer = $stripe->customers()->create([
					'email' => $child->username,
				]);
				$child->stripe_id = $customer['id'];
				$child->save();
			}
			
					$cardRes = $stripe->cards()->create($child->stripe_id,$request->token_id);
					
					if(isset($cardRes['id'])){
						
						$customerupdate = $stripe->customers()->update($child->stripe_id, [
							'default_source' => $cardRes['id']
						]);
						
						$cardRes['default'] = 1;
						/*if($child->cards()->count() <= 0){
							$cardRes['default'] = 1;
						}*/
						\App\Cards::where("stripe_id",$child->stripe_id)->update(['default'=>0]);
						
						\App\Cards::saveCard($cardRes,$child->stripe_id);
						
						$result = User::select(['id','username','first_name','last_name','stripe_id','stripe_acount_type'])
						->with('cards')
						->where('id',$child->id)->orderby('created_at','DESC')->first();
					
						$messages = RT::rtext("success_parent_card_added_for_child");
						$status = true;
						$code = 200;
					}else{
						$messages = RT::rtext("error_default");
						$status = true;
						$code = 400;
					}
		}
		
		
		$rescode = 200;
		if($code != 200){
			$rescode =  RESPONCE_ERROR_CODE;
		}
		
		return response()->json([
                'message' =>$messages,
                'success' => $status,
                'result' => $result,
                'status' => $code],$rescode);
       
        
    }
	
    public function unsubscribeCurrentPlanByUserId($user_id,$except_id)
    {
       
        
        $orders=Order::where('child_id',$user_id)->where("trans_status",'active')->get();
 
        foreach ($orders as $key => $order) {
            if($order && $order->child && $order->id != $except_id)
            {
                $child = $order->child;

                if($order->subscription_id === 0 || $order->subscription_id == "" || $order->subscription_id == "0" || !$order->subscription_id){
                    
                }else{
                    
                    $SECRET_KEY = \config('admin.stripe.SECRET_KEY');
                    if($child->stripe_acount_type == "local"){ $SECRET_KEY = \config('admin.stripe_citrus.SECRET_KEY'); }
                    $stripe=\Stripe::make($SECRET_KEY);

                    $subscription = $stripe->subscriptions()->cancel($child->stripe_id, $order->subscription_id, true);
					$order->unsubscription_ob = json_encode($subscription); 
                }
				
				$order->status=0; 
                $order->trans_status="cancelled"; 
                $order->save();
				
						$plan = json_decode($order->plan_ob);
						$subject = RT::rtext("mail_subject_stop_subscription_success");
						$user = $order->parent;
						if($user && $plan){

                            $mdata = ['action'=>'unsubscription_success','user'=>$user,'subject'=>$subject,'order'=>$order,'plan'=>$plan,'view'=>'email.billing.unsubscribe-success','to'=>$user->email];
                            SendEmailJob::dispatch($mdata)->onConnection('database')->onQueue('emails');
                        }
                
               
            }    
        }

        
    }

    public function getPlanlistForUser(Request $request){

        $code = 200;  $message = ""; $result =['data'=>[],'current_plan'=>'','child'=>""];

        $rules = array(
            'child_id'=>'required'
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails())
        {
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
            $code = 400;
        }else{
            $user= JWTAuth::touser($request->header('authorization'));
            $child = User::where('id',$request->child_id)->where('parent_id',$user->id)->first();
            
            if(!$user || !$child ) {
                $code = 400; $message = RT::rtext("warning_user_data_not_found");
            }else{
                $order=Order::where('child_id',$request->child_id)->where("trans_status",'active')->orderby('updated_at','DESC')->first();
                $current_plan = 0 ;
				$current_plan_id = 0;
                if($order){
                    $current_plan_id = $order->plan_id ;
					$current_plan = Plan::where("id",$current_plan_id)->first();
					if($current_plan){
						$child->subscription_id = $order->subscription_id; 
					}
                }
                $result['data'] = Plan::where("status","1")->where("id","!=",$current_plan_id)->where("type","!=",'free')->get();
                $result['current_plan'] =$current_plan; 
                $result['child'] = $child;
            }
        }

        $res_code = 200;
        $status = true;
        if($code != 200){
            $res_code = RESPONCE_ERROR_CODE;
            $status = false;
        }

        return response()->json([
            'result' => $result,
            'message' => $message,
            'success' => $status,
            'status' => $code,
        ],$res_code);
    }

    
}
