<?php

namespace App\Http\Controllers\Api\Child;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use JWTAuth;
use Carbon\Carbon as Carbon;
use App\Notification;
use Twilio\Rest\Client;


use App\Jobs\SendEmailJob;

use App\Responcetext as RT;

class RegisterController extends Controller
{
	public function update(Request $request)
    {
		$user= JWTAuth::touser($request->header('authorization'));
		
        $rules = array(
            'gender' => 'nullable|in:male,female',
            'image' => 'nullable|mimes:jpg,jpeg,png',
            'phone'=>'regex:/^(?=.*[0-9])[ +()0-9]+$/|unique:users,phone,'.$user->id,
        );
		
		$val_msg = [
			'phone.unique'=>RT::rtext("warning_require_unique_phone_number")
		];

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json([
                'message' =>$messages,
                'success' => false,
                'status' => $code],RESPONCE_ERROR_CODE);
        }
        
        
        $child= User::where('id',$request->child_id)->first();

        if(!$child){
            return response()->json([
                'result' => (Object)[],
                'message' => RT::rtext("warning_user_data_not_found"),
                'success' => false,
                'status' => 400,],RESPONCE_ERROR_CODE);            
        }

        $input = $request->all();
        // $input['username']=($request->username)?$request->username:$child->username;
        $input['first_name']=($request->first_name)?$request->first_name:$child->first_name;
        $input['last_name']=($request->last_name)?$request->last_name:$child->last_name;
        $input['phone']=($request->phone)?$request->phone:$child->phone;
        $input['age']=($request->age)?$request->age:$child->age;
        $input['dob']=($request->dob)?$request->dob:$child->dob;
        $input['gender']=($request->gender)?$request->gender:$child->gender;
        
        if(isset($input['image']) && $input['image'] != ''){
            if($child->image && file_exists('uploads/user/thumbnail/'.$child->image)){
                unlink('uploads/user/thumbnail/'.$child->image); //delete previously uploaded Attachment
            }
            if($child->image && file_exists('uploads/user/'.$child->image)){
                unlink('uploads/user/'.$child->image); //delete previously uploaded Attachment
            }
            $imageName = str_replace(' ', '_', $child->username).'_'.uniqid(time()) . '.' . $input['image']->getClientOriginalExtension();

            uploadImage($input['image'],'uploads/user/thumbnail',$imageName,'150','150');
            uploadImage($input['image'],'uploads/user',$imageName,'400','400');
            
            $input['image'] = $imageName;
        }

        $path = url("/")."/"."uploads/user/";
        $image = (isset($input['image'])?$input['image']:$child->image);
        if($image){
            $thumbImage = $path."thumbnail/".$image;
            $image = $path.$image;
        }else{
            $thumbImage = null;
            $image = null;
        }
        
        /*$path = url("/")."/"."uploads/user/";
        $thumbImage = $path."thumbnail/".(isset($input['image'])?$input['image']:$child->image);
        $image = $path.(isset($input['image'])?$input['image']:$child->image);*/

        $child->update($input);

        
        $child->image = $image;
        
        $result=(Object)make_null($child);

        return response()->json([
            'result' => $result,
            'message' => 'Success! Profile has been updated!',
            'success' => true,
            'status' => 200,
        ],200);
    }

    public function viewChildFromBatch(Request $request){
        
    	$rules = array(
            'batch_id' => 'required',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json([
                'message' =>$messages,
                'success' => false,
                'status' => $code],RESPONCE_ERROR_CODE);
        }
        $child= JWTAuth::touser($request->header('authorization'));

        if($child->batch_id == $request->batch_id){
            return response()->json([
                'result' => (Object)[],
                'message' => RT::rtext("warning_user_not_found_for_given_badge"),
                'success' => false,
                'status' => 400,
            ],RESPONCE_ERROR_CODE);
        }

        $user = User::where('batch_id',$request->batch_id)->first();

        if(!$user){
            return response()->json([
                'result' => (Object)[],
                'message' => RT::rtext("warning_user_not_found_for_given_badge"),
                'success' => false,
                'status' => 400,
            ],RESPONCE_ERROR_CODE);   
        }
        $message = array();
        $title=$child->first_name;
        $msg="Badge Request from ".$child->first_name." ".$child->last_name;

        $message['body'] = $msg;
        $message['message'] = $msg;
        $message['title'] = $title;
        $message['target_screen'] = 'child';

        $message['sender']= make_null($child);
        $message['receiver']= make_null($user);

       /* $notification=$message['notification'] = array(
            "body" => $msg,
            "title" => $title
         
        );*/
        $result="";
        if($user->device_type != "web" && $user->fire_base_token != "" && $user->fire_base_token)
        {
            try{
                $result=send_notification($user->fire_base_token,[],$user->device_type,$message);
            }catch(\Exception $exception){
                $message = $exception->getMessage()." -------- AT LINE : ".$exception->getLine()."--------- IN FILE : ". $exception->getFile()."--------Request Url : ".\Request::capture()->getUri();
			    add_logs("error","exception_".$request->path(),$message,$exception->getCode(),$exception->getLine(),$exception->getFile());
            }
            
        }
		
        if(true)
        {
            $exist_count = Notification::where('sender_id',$child->id)->where('receiver_id',$user->id)->where('type','Request')->where('read_flag',0)->count();
			
			if($exist_count <=0){
				$notification_data=new Notification();
				$notification_data->sender_id=$child->id;
				$notification_data->receiver_id=$user->id;
				$notification_data->message=$msg;
				$notification_data->child_a_id=$child->id;
				$notification_data->child_b_id=$user->id;
				$notification_data->type='Request';
				$notification_data->read_flag=0;
				$notification_data->save();
			}else{
				//$exist_ = Notification::where('sender_id',$child->id)->where('receiver_id',$user->id)->where('type','Request')->where('read_flag',0)->get();
				//dd($exist_);
			}

        }
		
		try{
			
			
			send_sms($user->country_code,$user->phone,RT::rtext("sms_child_you_receive_badge_request"));
			send_sms($child->country_code,$child->phone,RT::rtext("sms_child_you_just_request_for_badge"));
		}catch(\Exception $exception){
			$message = $exception->getMessage()." -------- AT LINE : ".$exception->getLine()."--------- IN FILE : ". $exception->getFile()."--------Request Url : ".\Request::capture()->getUri();
			add_logs("error","exception_".$request->path(),$message,$exception->getCode(),$exception->getLine(),$exception->getFile());
		}
       
		
		
        $data['title'] = 'Request send from '.$child->username. ' to '.$user->username;
        $data['message'] = 'Request send to '.$user->username;
        $data['sender'] = make_null($child);
        $data['receiver'] = make_null($user);
        $data['target_screen'] = 'child';
        $data['response']=$result;
      
            
        $result=(Object)$data;

        return response()->json([
            'result' => $result,
            'message' => RT::rtext("success_badge_request_sent"),
            'success' => true,
            'status' => 200,
        ],200);
  
    }

    public function sendNotification(Request $request){
    	$rules = array(
            'batch_id' => 'required',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json([
                'message' =>$messages,
                'success' => false,
                'status' => $code],RESPONCE_ERROR_CODE);
        }

        $child= JWTAuth::touser($request->header('authorization'));

        if($child->batch_id == $request->batch_id){
            return response()->json([
                'result' => (Object)[],
                'message' => RT::rtext("warning_user_not_found_for_given_badge"),
                'success' => false,
                'status' => 400,
            ],RESPONCE_ERROR_CODE);
        }

        $user = User::where('batch_id',$request->batch_id)->first();
        
        if(!$user){
            return response()->json([
                'result' => (Object)[],
                'message' => RT::rtext("warning_user_not_found_for_given_badge"),
                'success' => false,
                'status' => 400,
            ],RESPONCE_ERROR_CODE);   
        }
        $parent_1=User::find($child->parent_id);
        $parent_2=User::find($user->parent_id);
		
		
		Notification::where('sender_id',$user->id)->where('receiver_id',$child->id)->where('type','Request')->where('read_flag',0)->update(['read_flag'=>1]);
        
		$message = array();
        $title='Get connected';
        $msg=$child->first_name." ".$child->last_name.' and '.$user->first_name." ".$user->last_name.' Connected.';

        $message['body'] = $msg;
        $message['message'] = $msg;
        $message['title'] = $title;
        $message['target_screen'] = 'parent';

        $message['sender']= make_null($child);
        $message['receiver']= make_null($user);
		
        if($user->device_type != 'web')
        {  
            $message['title'] = $child->first_name." ".$child->last_name.' and '.$user->first_name." ".$user->last_name.' connected.';
            $message['message'] ='Your Request Approved by '.$child->first_name." ".$child->last_name;
            $message['target_screen'] = 'child_connect';
            $result=send_notification($user->fire_base_token,[],$user->device_type,$message);
        }
        if(true)
        {
			
            $notification_data=new Notification();
            $notification_data->sender_id=$child->id;
            $notification_data->receiver_id=$user->id;
            $notification_data->message='Your Request Approved by '.$child->first_name." ".$child->last_name;;
            $notification_data->child_a_id=$user->id;
            $notification_data->child_b_id=$child->id;
            $notification_data->type='Approve';
            $notification_data->save();
		}
        
       
        if($parent_1->device_type != 'web')
        {  
         
            $message['target_screen'] = 'parent';
            $result_1=send_notification($parent_1->fire_base_token,[],$parent_1->device_type,$message);
        }
        if(true)
        {
            $notification_data=new Notification();
            $notification_data->sender_id=$child->id;
            $notification_data->receiver_id=$parent_1->id;
            $notification_data->message=$msg;
            $notification_data->child_a_id=$child->id;
            $notification_data->child_b_id=$user->id;
            $notification_data->type='Approve';
            $notification_data->save();
		}
		
		if($parent_2->device_type != 'web')
        {  
          
            $message['target_screen'] = 'parent';
            $result_2=send_notification($parent_2->fire_base_token,[],$parent_2->device_type,$message);
        }
		
        if(true)
        {
            $notification_data=new Notification();
            $notification_data->sender_id=$child->id;
            $notification_data->receiver_id=$parent_2->id;
            $notification_data->message=$msg;
            $notification_data->child_a_id=$user->id;
            $notification_data->child_b_id=$child->id;
            $notification_data->type='Approve';
            $notification_data->save();
        }
      
		try{
			$notification = RT::rtext("sms_parent_your_child_viewed_a_badge");
			send_sms($parent_1->country_code,$parent_1->phone,$notification);
			send_sms($parent_2->country_code,$parent_2->phone,$notification);
			
			

		}catch(\Exception $e){
			
		}

			$notification = RT::rtext("mail_line1_parent_your_child_connected_with_other");
			$subject = RT::rtext("mail_subject_parent_your_child_connected");
            
            $mdata = ['action'=>'request_accept_mail_to_parent','notification'=>$notification,'subject'=>$subject,'user'=>$parent_1,'view'=>'email.notification','to'=>$parent_1->email];
            SendEmailJob::dispatch($mdata)->onConnection('database')->onQueue('emails');

            $mdata['to'] =$parent_2->email;
            $mdata['user'] =$parent_2;

            SendEmailJob::dispatch($mdata)->onConnection('database')->onQueue('emails');
      
        $data['title'] = 'Get connected '.$child->username.' and '.$user->username;
        $data['message'] = $child->username.' is approved '.$user->username.'�s request';
        $data['sender'] = make_null($user);
        $data['receiver'] = make_null($child);
        $data['target_screen'] = 'parent';
        
        $result=(Object)$data;

        return response()->json([
            'result' => $result,
            'message' => RT::rtext("success_child_you_approve_badge_request"),
            'success' => true,
            'status' => 200,
        ],200);

    }

    public function childDetail(Request $request)
    {
        $user= JWTAuth::touser($request->header('authorization'));

        $child= User::where('id',$request->child_id)->first();
        /*($path = url("/")."/"."uploads/user/";
        $child->thumb_image = ($child->image)?$path."thumbnail/".$child->image:'';
        $child->image = ($child->image)?$path.$child->image:'';*/
        
        if(!$child){
            return response()->json([
                'result' => (Object)[],
                'message' => 'User not found.',
                'success' => false,
                'status' => 400,],RESPONCE_ERROR_CODE);            
        }
        //$order_data=Order::join('')
        $result=(Object)make_null($child);

        return response()->json([
            'result' => $result,
            'message' => 'Child Detail.',
            'success' => true,
            'status' => 200,],200);
    }

    public function get_notification(Request $request)
    {
        $user= JWTAuth::touser($request->header('authorization'));

		$result = [];
        $notification=Notification::with('sender','receiver')->where('receiver_id',$user->id)->where('read_flag',0);
		
		if($request->has('type')){
			$notification->where("type",$request->get('type'));
		}
		$notification = $notification->get();
		if($notification){
			$result=make_null($notification);
		}
		return response()->json([
            'result' => $result,
            'message' => 'notification data',
            'success' => true,
            'status' => 200,],200);
    }
    public function update_notification(Request $request)
    {
        $user= JWTAuth::touser($request->header('authorization'));
        $ids = explode(",",$request->notification_id);

        if($request->has('notification_id') && $request->notification_id == "all"){
           // $notification=Notification::where('receiver_id',$user->id)->where('type','!=','Request')->get();
            //echo "<pre>"; print_r($notification);
			if($request->has('type')){
				Notification::where('receiver_id',$user->id)->where('type','=',$request->type)->update(['read_flag'=>1]);
			}else{
				Notification::where('receiver_id',$user->id)->update(['read_flag'=>1]);
			}
				
        }else{
          //  $notification=Notification::whereIn("id",$ids)->get();
            Notification::whereIn("id",$ids)->update(['read_flag'=>1]);
        }
		
        return response()->json([
            'message' => 'Success',
            'success' => true,
            'status' => 200,],200);
     
       
    }

   
}
