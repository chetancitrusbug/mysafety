<?php

namespace App\Http\Controllers\Api\Child;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\Password;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Session;
use App\Setting;
use App\Responcetext as RT;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $rules = array(
            'username' => 'required',
            'password'=>'required',
            'user_type' => 'required'
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json([
                'message' =>$messages,
                'success' => false,
                'status' => $code],RESPONCE_ERROR_CODE);
        }

        if(isset($request->username) && isset($request->password) && isset($request->user_type))
       	{
           $input = $request->only('username','password');
           $jwt_token = null;
           $user = User::where("username", $request->username)->withTrashed()->with('roles')->first();
          
           if ($user)
           {
			   if($request->has('fire_base_token')){
					$user->fire_base_token = $request->fire_base_token;
			   }
			   if($request->has('device_type')){
					$user->device_type = $request->device_type;
			   }
               $user->save();

               if($user->hasRole($request->user_type))
               {    
                   if (Hash::check($request->password, $user->password))
                   {
                       if ($user->status == 1 && $jwt_token = JWTAuth::attempt($input,['exp' =>Carbon::now()->addDays(7)->timestamp]))
                       {
                           if($request->user_type == 'CU')
                           {
                                $user = JWTAuth::user();
                                $path = url("/")."/"."uploads/user/";
                                $user->thumb_image = ($user->image)?$path."thumbnail/".$user->image:'';
                                $user->image = ($user->image)?$path.$user->image:'';

                           		$result=make_null($user);
                           		$result['token']=$jwt_token;

                               return response()->json([
                                   'result' => $result,
                                   'message' => RT::rtext("success_login"),
                                   'success' => true,
                                   'status' => 200,
                               ]);
                           }
                           else
                           {
                               return response()->json([
                                   'success' => false,
                                   'message' => 'Please Enter Correct User Type.',
                                   'status'  => 401
                               ], RESPONCE_ERROR_CODE);
                           }
                       }
                       else
                       {
                           return response()->json([
                               'success' => false,
                               'message' => RT::rtext("warning_your_account_not_activated_yet"),
                               'status'  => 401
                           ], RESPONCE_ERROR_CODE);
                       }
                   }
                   else
                   {
                       return response()->json([
                           'success' => false,
                           'message' => RT::rtext('warning_incorrect_email_or_password'),
                           'status'  => 401
                       ], RESPONCE_ERROR_CODE);
                   }
               }
               else
               {
                   return response()->json([
                       'success' => false,
                       'message' => 'Please Select Correct User type.',
                       'status'  => 401
                   ], RESPONCE_ERROR_CODE);
               }
           }
           else
           {
               return response()->json([
                   'success' => false,
                   'message' => RT::rtext('warning_incorrect_email_or_password'),
                   'status'  => 401
               ], RESPONCE_ERROR_CODE);
           }
       	}
       	else
      	{
           return response()->json([
               'success' => false,
               'message' => 'Invalid Parameter.',
               'status'  => 400
           ], RESPONCE_ERROR_CODE);
        }
    }

    public function logout(Request $request)
    {
        $rules = array(
            'user_type' => 'required|in:CU',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json([
                'message' =>$messages,
                'success' => false,
                'status' => $code],RESPONCE_ERROR_CODE);
        }

        if($request->header('authorization') != null)
        {
            try {
                JWTAuth::invalidate($request->header('authorization'));

                return response()->json([
                    'success' => true,
                    'message' => RT::rtext('success_logout'),
                    'status'  => 200
                ], 200);

            } catch (JWTException $exception) {

                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, the user cannot be logged out.',
                    'status'  => 500
                ], RESPONCE_ERROR_CODE);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => 'Invalid Parameter.',
                'status'  => 400
            ], RESPONCE_ERROR_CODE);
        }
        
    }

    public function change_password(Request $request)
    {
        $rules = array(
            'username' => 'required',
            'password'=>'required|same:confirm_password',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json([
                'message' =>$messages,
                'success' => false,
                'status' => $code],RESPONCE_ERROR_CODE);
        }

        $user=JWTAuth::touser($request->header('authorization'));
        
        if($user != null && $user->username == $request->username)
        {
        	// $user->username=$request->name;
        	$user->password=Hash::make($request->password);
        	$user->save();

        	$result=make_null($user);
            $result['token']=$request->header('authorization');

            return response()->json([
               'result' => $result,
               'message' => RT::rtext('success_password_changed'),
               'success' => true,
               'status' => 200,
            ]);

        }
        else
        {
        	return response()->json([
               'success' => false,
               'message' => RT::rtext('warning_incorrect_email_or_password'),
               'status'  => 400
           ], RESPONCE_ERROR_CODE);
        }

    }

    public function forgotPassword(Request $request)
    {
        if(isset($request->phone))
        {
            $user=User::where('phone',$request->phone)->first();

            if($user)
            {
                $otp = substr(number_format(time() * rand(),0,'',''),0,6);
                $user->otp = $otp;
                $user->save();
				$msg = $otp." is one time password(OTP) to reset your mysafetynet account password";
                send_sms($user->country_code,$request->phone,$msg);
                
				if(RESPONCE_ERROR_CODE == 400) { $otp="test"; } // Disable in responce for web only
				
                return response()->json([
                    'data' => ['otp'=>$otp],
                    'success' => true,
                    'message' => RT::rtext('success_forgot_password_send_to_phone_no'),
                    'status'  => 200
                ], 200);
            }
            else
            {
                return response()->json([
                    'success' => false,
                    'message' => RT::rtext("warning_user_data_not_found"),
                    'status'  => 400
                ], RESPONCE_ERROR_CODE);
            }
        }
        else{
            return response()->json([
                'success' => false,
                'message' => RT::rtext("warning_user_data_not_found"),
                'status'  => 400
            ], RESPONCE_ERROR_CODE);
        }
    }

    public function updateForgotPassword(Request $request)
    {
        $rules = array(
            'username' => 'required',
            'otp'=>'required',
            'password'=>'required|same:confirm_password', 
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json([
                'message' =>$messages,
                'success' => false,
                'status' => $code],RESPONCE_ERROR_CODE);
        }

        if(isset($request->username))
        {
            $user=User::where('username',$request->username)->where('otp',$request->otp)->first();
            
            if($user)
            {
                $otp = substr(number_format(time() * rand(),0,'',''),0,6);
                $user->otp = null;
                $user->password = bcrypt($request->password);
                $user->save();

                $result=make_null($user);
                
                return response()->json([
                    'data' => $result,
                    'success' => true,
                    'message' => RT::rtext("success_password_changed"),
                    'status'  => 200
                ], 200);
            }
            else
            {
                return response()->json([
                    'success' => false,
                    'message' => RT::rtext("warning_user_data_not_found"),
                    'status'  => 400
                ], RESPONCE_ERROR_CODE);
            }
        }
        else{
            return response()->json([
                'success' => false,
                'message' => RT::rtext("warning_user_data_not_found"),
                'status'  => 400
            ], RESPONCE_ERROR_CODE);
        }
    }
    
}
