<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Seocontent;

class SeoController extends Controller
{

    public function getSeopage(Request $request)
    {
		$default = Seocontent::where("page_name","default")->first();
		$result = [];
		$i = 0;
		foreach(\config('constant.meta_key_page') as $k => $val){
			$item = Seocontent::where("page_name",$k)->first();
			if(!$item){
				$item = $default;
			}
			
			if($item){
				$res = $item->toArray();
				$res['page_name'] = $k;
				$result[] = $res;
			}
		}
		
		$items = Seocontent::where("id",">",0)->get();
		
		return response()->json([
            'success' => true,
            'result' => $result,
            'message' => "",
            'status'  => 200
        ], 200);
    }

}
