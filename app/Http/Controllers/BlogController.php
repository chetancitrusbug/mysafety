<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blog;
use App\ContactUs;
use App\Setting;

class BlogController extends Controller
{
	/*
    //
    public function allPages()
    {
        $setting = Setting::pluck('value','key')->toArray();
        $page=Blog::select([
            'blog.*',
            \DB::raw('(CASE WHEN image != "" THEN CONCAT( "'.url('').'",CONCAT("/uploads/blog/"),image) ELSE "" END) AS image'),
            \DB::raw('(CASE WHEN image != "" THEN CONCAT( "'.url('').'",CONCAT("/uploads/blog/thumbnail/"),image) ELSE "" END) AS thumb_image'),
        ])->where('type','page')->where('status',1)->paginate($setting['pagination']);

        $result=$page->toArray();

        if($page->count() > 0)
        {
            return response()->json([
                'total' => get_api_data(isset($result['total']) ? $result['total'] : 0),
                'current_page' => get_api_data(isset($result['current_page']) ? $result['current_page'] : 0),
                'prev_page_url' => get_api_data(isset($result['prev_page_url']) ? $result['prev_page_url'] : ''),
                'next_page_url' => get_api_data(isset($result['next_page_url']) ? $result['next_page_url'] : ''),
                'result' =>$result['data'],
                'message' => 'Page list.',
                'success' => true,
                'status' => 200,
            ]);

        }
        return response()->json([
            'total' => get_api_data(isset($result['total']) ? $result['total'] : 0),
            'current_page' => get_api_data(isset($result['current_page']) ? $result['current_page'] : 0),
            'prev_page_url' => get_api_data(isset($result['prev_page_url']) ? $result['prev_page_url'] : ''),
            'next_page_url' => get_api_data(isset($result['next_page_url']) ? $result['next_page_url'] : ''),
            'message' => 'No Record Found.',
            'status'  => 400
        ], RESPONCE_ERROR_CODE);
    }
*/
    public function page(Request $request)
    {
        $page=Blog::where('slug',$request->slug)->first();

        return view('cms.app-cms-page', compact('page'));
    }

   /* public function allBlogs(Request $request)
    {
        $setting = Setting::pluck('value','key')->toArray();
        $page=Blog::select([
                'blog.*',
                \DB::raw('(CASE WHEN image != "" THEN CONCAT( "'.url('').'",CONCAT("/uploads/blog/"),image) ELSE "" END) AS image'),
                \DB::raw('(CASE WHEN image != "" THEN CONCAT( "'.url('').'",CONCAT("/uploads/blog/thumbnail/"),image) ELSE "" END) AS thumb_image'),
            ])->where('type','blog')->where('status',1)->orderBy('id',$request->get('order_by','DESC'))->paginate($setting['pagination']);
        
        $result=$page->toArray();
    
        if($page->count() > 0)
        {
  
            return response()->json([
                'total' => get_api_data(isset($result['total']) ? $result['total'] : 0),
                'current_page' => get_api_data(isset($result['current_page']) ? $result['current_page'] : 0),
                'prev_page_url' => get_api_data(isset($result['prev_page_url']) ? $result['prev_page_url'] : ''),
                'next_page_url' => get_api_data(isset($result['next_page_url']) ? $result['next_page_url'] : ''),
                'result' =>$result['data'],
                'message' => 'Blog list.',
                'success' => true,
                'status' => 200,
            ]);

        }
        else
        {
            return response()->json([
                'total' => get_api_data(isset($result['total']) ? $result['total'] : 0),
                'current_page' => get_api_data(isset($result['current_page']) ? $result['current_page'] : 0),
                'prev_page_url' => get_api_data(isset($result['prev_page_url']) ? $result['prev_page_url'] : ''),
                'next_page_url' => get_api_data(isset($result['next_page_url']) ? $result['next_page_url'] : ''),
                'message' => 'No Record Found.',
                'status'  => 400
            ], RESPONCE_ERROR_CODE);
        }
        
    }

    public function blog(Request $request)
    {
        $blog=Blog::select([
                        'blog.*',
                        \DB::raw('(CASE WHEN image != "" THEN CONCAT( "'.url('').'",CONCAT("/uploads/blog/"),image) ELSE "" END) AS image'),
                        \DB::raw('(CASE WHEN image != "" THEN CONCAT( "'.url('').'",CONCAT("/uploads/blog/thumbnail/"),image) ELSE "" END) AS thumb_image'),
                    ])
                    ->where('slug',$request->slug)
                    ->where('type','blog')
                    ->first();
               
                    

        if($blog)
        {
            $result=$blog->toArray();
            
            return response()->json([
               
                'result' => $result,
                'message' => 'Blog.',
                'success' => true,
                'status' => 200,
            ],200);
        }
        return response()->json([
            'success' => false,
            'message' => 'No Record Found.',
            'status'  => 400
        ], RESPONCE_ERROR_CODE);
    }

    public function contact_us(Request $request)
    {
        $rules = array(
            'email' => 'required|email',
            'subject' => 'required',
            'name' => 'required',
            'message' => 'required',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json([
                'message' =>$messages,
                'success' => false,
                'status' => $code],RESPONCE_ERROR_CODE);
        }


        $contactUs=new ContactUs();
        $contactUs->name=$request->name;
        $contactUs->email=$request->email;
        $contactUs->subject=$request->subject;
        $contactUs->message=$request->message;
        $contactUs->save();

        $result=make_null($contactUs);

        return response()->json([
            'result' => $result,
            'message' => 'Message sent successfully.',
            'success' => true,
            'status' => 200,
        ],200);

    }*/
}
