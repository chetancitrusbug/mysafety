<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blog;
use App\ContactUs;
use App\Setting;
use Twilio\Twiml;
use Twilio\TwiML\VoiceResponse;

class TwillioController extends Controller
{
	
	
    public function initVerificationCall(Request $request)
    {

	


        $sid = \config('admin.twilio.SID'); // Your Account SID from www.twilio.com/console
        $token = \config('admin.twilio.TOKEN'); // Your Auth Token from www.twilio.com/console

        $client = new \Twilio\Rest\Client($sid, $token);
		
		$call = $client->calls->create(
			"+919998543618", 
			\config('admin.twilio.FROM'),
			array(
				'url' => "https://secure.mysafetynet.info/verification/voice.xml"
			)
        );
		
	
    }
	public function askforotp(Request $request)
    {
		 $response = new Twiml();
        $gather = $response->gather(
            [
                'numDigits' => 1,
                'action' => "call-response",
            ]
        );
        $gather->say('Please press 1 for directions. Press 2 for a ');
		//return $response;
		header('Content-Type: text/xml');
        return response($response, 200)->header('Content-Type', 'application/xml');
	}
	public function callresponce(Request $request)
    {
	//	dd($request->all());
		
				\Mail::raw(json_encode($request->all()), function ($message) {
  $message->to("jignesh.citrusbug@gmail.com")
    ->subject("Call test");
});

$response = new Twiml();

 $response->say('Thank you!');
 
return $response;
exit;



        $selectedOption = $request->input('Digits');
		$selectedOption = $request->input('Called');
		
        switch ($selectedOption) {
            case 1:
                return $this->_getReturnInstructions();
            case 2:
                return $this->_getPlanetsMenu();
        }
        $response = new Twiml();
        $response->say(
            'Returning to the main menu',
            ['voice' => 'Alice', 'language' => 'en-GB']
        );
        $response->redirect(route('welcome', [], false));
        return $response;
	}

   
}
