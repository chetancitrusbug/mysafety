<?php

namespace App\Http\Middleware;

use Closure;

class defingGloble
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$responce_code = 200;
		if($request->header('devicetype') && $request->header('devicetype') == "web" ){
			$responce_code = 400;
		}
		
        define('RESPONCE_ERROR_CODE',$responce_code);
        define('REQUEST_FROM',"API");
		
        return $next($request);
    }
}
