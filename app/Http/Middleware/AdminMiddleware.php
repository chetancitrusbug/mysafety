<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        define('RESPONCE_ERROR_CODE',400);
        define('REQUEST_FROM',"WEB");
		
        if($request->user()->hasRole('AU')) {
            return $next($request);
        }
		
		if ($request->user()->hasRole('EU')) {
            return redirect()->to('admin/childs');
        }

        return redirect()->to('/');
    }
}
