<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class AdminEmployeeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        define('RESPONCE_ERROR_CODE',400);
        define('REQUEST_FROM',"WEB");
		
        if (trim($request->user()->roles[0]->name) == 'Admin' || trim($request->user()->roles[0]->name) == 'Parent') {
            return $next($request);
        }
		
		if ($request->user()->hasRole('EU')) {
            return $next($request);
        }

        return redirect()->to('/');
    }
}
