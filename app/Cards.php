<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cards extends Model
{
    
 //   
	
	protected $table = 'cards';
	
	protected $primaryKey = 'id';
	
	protected $guarded = ['id'];
	
	protected $appends = ['exp_month_year'];
	
	protected $hidden = [
        'address_line1', 'address_line2', 'address_city', 
        'address_state', 'address_country','address_zip','created_at','updated_at'
    ];
	
/*	protected $fillable = [
        'number', 'exp_month', 'exp_year', 'brand', 'card_id', 'address_line1', 'address_line2', 'address_city', 
        'address_state', 'address_country', 'stripe_id','address_zip'
    ];
*/
	
    public function user()
    {
        return $this->belongsTo('App\User', 'stripe_id', 'stripe_id');
    }
	

    public function getExpMonthYearAttribute()
    {
        return $this->exp_month . '/' . $this->exp_year;
    }

    public static function saveCard($cardRes,$stripe_id) {
		
		if(Cards::where("card_id",$cardRes['id'])->where("stripe_id",$stripe_id)->count() <=0){
		$idata = [];
					
					$idata['card_id'] =(string) $cardRes['id'];
					$idata['number'] = (string)$cardRes['last4'];
					$idata['stripe_id'] = $stripe_id;
					
					$idata['card_id'] = (string)$cardRes['id'];
					$idata['number'] = (string)$cardRes['last4'];
					$idata['exp_month'] = $cardRes['exp_month'];
					$idata['exp_year'] = $cardRes['exp_year'];
					$idata['brand'] = (string)$cardRes['brand'];
					$idata['address_line1'] = (string)$cardRes['address_line1'];
					$idata['address_line2'] = (string)$cardRes['address_line2'];
					$idata['address_city'] = (string)$cardRes['address_city'];
					$idata['address_state'] = (string)$cardRes['address_state'];
					$idata['address_zip'] = (string)$cardRes['address_zip'];
					$idata['address_country'] = (string)$cardRes['address_country'];
					if(isset($cardRes['default'])){
						$idata['default'] = $cardRes['default'];
					}else{
						$idata['default'] = 0;
					}
					
					
					
		return Cards::create($idata);		
		}
	}
  /*  public function setCardAttribute($carderDetails) {
        $carder = collect($carderDetails);
        $this->attributes['number'] = $carder->get('last4');
        $this->attributes['exp_month'] = $carder->get('exp_month');
        $this->attributes['exp_year'] = $carder->get('exp_year');
        $this->attributes['brand'] = $carder->get('brand');
        $this->attributes['card_id'] = $carder->get('id');

        $this->attributes['address_line1'] = $carder->get('address_line1');
        $this->attributes['address_line2'] = $carder->get('address_line2');
        $this->attributes['address_city'] = $carder->get('address_city');
        $this->attributes['address_state'] = $carder->get('address_state');
        $this->attributes['address_zip'] = $carder->get('address_zip');
        $this->attributes['address_country'] = $carder->get('address_country');
    }*/
}
