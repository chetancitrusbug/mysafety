<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
		/*if ($request->expectsJson() && (defined('REQUEST_FROM') && REQUEST_FROM == "API" )) {
			
			$message = $exception->getMessage()." -------- AT LINE : ".$exception->getLine()."--------- IN FILE : ". $exception->getFile()."--------Request Url : ".\Request::capture()->getUri();
			add_logs("error","exception_".$request->path(),$message,$exception->getCode(),$exception->getLine(),$exception->getFile());

			if(REQUEST_FROM == "API"){
				$ecode = RESPONCE_ERROR_CODE;
			}else{
				$ecode = 200;
			}
			return response()->json(['success' => false,'message' => 'Something went wrong please try again later !','status'  => 400],$ecode);
		}else{
			return parent::render($request, $exception);
		}*/
        return parent::render($request, $exception);
    }
}
