<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Refefile extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'refe_file';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['refe_table_field_name', 'ref_field_id', 'refe_file_path','refe_file_name', 'ref_code','ref_type'];
	
	protected $appends = ['file_url','created_formated'];
	
	public function getFileUrlAttribute()
    {
		if($this->refe_file_path && \File::exists(public_path()."/".$this->refe_file_path)){
			return url("/")."/".$this->refe_file_path;
		}else{
			return "";
		}
	}
	public function getCreatedFormatedAttribute()
    {
        if($this->created_at != "" && $this->created_at){
            return \Carbon\Carbon::parse($this->created_at)->format(session('setting.date_format',\config('admin.setting.date_format_on_app')));
        }
        return $this->created_at;
    }


   


}
