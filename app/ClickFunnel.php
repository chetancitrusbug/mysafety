<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClickFunnel extends Model
{
	protected $table = 'click_funnel';
	
	
    protected $primaryKey = 'id';

    protected $guarded = ['id'];
	
}
