<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    //
    protected $table = 'notification';
	
	protected $primaryKey = 'id';

    protected $fillable = ['sender_id','receiver_id','child_a_id','child_b_id','message','type','read_flag'];

    public function sender()
    {
        return $this->hasOne('App\User', 'id', 'child_a_id')->select(["first_name","last_name","gender","id","username","image","batch_id","dob"]);
    }
    public function receiver()
    {
        return $this->hasOne('App\User', 'id', 'child_b_id')->select(["first_name","last_name","gender","id","username","image","batch_id","dob"]);
    }
}
