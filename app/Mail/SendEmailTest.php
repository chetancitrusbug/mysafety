<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailTest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
	 
	public $event;
	
    public function __construct($event)
    {
        $this->event = $event;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      /*  return $this->from('hello@app.com', 'Your Application')
            ->subject('Event Reminder: ' . $this->event->name)
            ->view('emails.reminder');*/
			
		return $this->subject('Event Reminder: ' . $this->event['subject'])
			->view($this->event['view'])
			->with(['contactUs' => $this->event['contactUs']]);
			
		/*	\Mail::queue('email.complaint', compact('contactUs'), function ($message) use ($contactUs, $subject) {
			$message->to(\config('admin.mail_to_admin'))->subject($subject);
		});*/
    }
}
