<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transactionlogs extends Model
{
	protected $table = 'transaction_logs';
	
	
    protected $primaryKey = 'id';

    protected $guarded = ['id'];
	
	protected $appends = ['payment_created'];
    
	
	
	
	
    public function getPaymentCreatedAttribute()
    {
        if($this->created_at != ""){
            return \Carbon\Carbon::parse($this->created_at)->format(session('setting.date_format',\config('admin.setting.date_format_on_app')));
        }
		return $this->created_at;
    }
	
	public function creator()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }
	public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
