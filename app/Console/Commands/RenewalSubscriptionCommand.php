<?php

namespace App\Console\Commands;
use App\User;
use App\PlanUser;
use App\Order;
use App\Plan;
use Carbon\Carbon as Carbon;
use Illuminate\Console\Command;
use Cartalyst\Stripe\Laravel\Facades\Stripe;

class renewalSubscriptionCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'RenewalSubscriptionCommand:renewalSubscriptionCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		
		/*
        $stripe = \Stripe::make(env("STRIPE_SECRET"));

        // PlanUser::where("status",1)->where("end_date","<=",\Carbon\Carbon::now())->update(["status"=>"4"]);

        $billings = PlanUser::where('subscription_id','!=','')->where('status',"1")->where("end_date","<=",\Carbon\Carbon::now()->addHour(12))->get();
        
        foreach($billings as $billing){
            if($billing->subscription_id)
            {
                try {
                    $invoices = $stripe->invoices()->all(["subscription"=>$billing->subscription_id]);

                    if(isset($invoices['data'])){
                        $order = Order::where('parent_id',$billing->user_id)->where('subscription_id',$billing->subscription_id)->first();
                        // echo "<pre>"; print_r($order);
                        if($order){
                            $order = $order->toArray();

                            $plan = Plan::find($order['plan_id']);
                            if(!$plan){
                                return response()->json([
                                    'result' => (Object)[],
                                    'message' => 'Plan not exist.',
                                    'success' => false,
                                    'status' => 400,
                                ],RESPONCE_ERROR_CODE);
                            }

                            // order start
                            $order['start_date'] = Carbon::now()->format('Y-m-d');
                            $date = null;
                            if($plan->type == "monthly"){
                                $date = Carbon::now()->addMonths(1);
                            }elseif($plan->type == "yearly"){
                                $date = Carbon::now()->addYears(1);
                            }elseif($plan->type == "daily"){
                                $date = Carbon::now()->addDays(1);
                            }
                            $order['expiry_date'] = $date->subDays(1)->format('Y-m-d');
                            $order['next_pay'] = $date->addDays(1)->format('Y-m-d');
                            $order['auth_response'] = json_encode($invoices['data']);
                            $order['order_no'] = $this->getNextOrderNumber();
                            $order['trans_status'] = 'active';
                            $order['status'] = 1;
                            $newOrder = Order::create($order);

                            //plan user update
                            $childInput['end_date'] = $date->subDays(1)->format('Y-m-d');
                            $newOrder->plan = PlanUser::where('id',$billing->id)->update($childInput);
                        }
                    }
                }
                catch (\Exception $e) {
                    //dd($e);
                }
            }
        }
		*/
    }

    function getNextOrderNumber()
    {
        // Get the last created order
        $lastOrder =Order::orderBy('created_at', 'desc')->first();

        if ($lastOrder != null)
            $number = $lastOrder->id;
        else 
            $number =0;

        return 'PO' . sprintf('%05d', intval($number) + 1);
    }
}
