<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{	
	use SoftDeletes;
	
	protected $table = 'employee';
	
	
    protected $primaryKey = 'id';

    protected $guarded = ['id'];
	
	public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
	public function approvedlist()
    {
        return $this->hasMany('App\Badgeapprove', 'badge_approve_by','user_id');
    }
	
	
	
	
}
