<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seocontent extends Model
{
	protected $table = 'seo_content';
	
	
    protected $primaryKey = 'id';

    protected $guarded = ['id'];
	
	function getTitleAttribute(){
		$def=Seocontent::where("page_name","default")->first();
		if((!$this->attributes['title'] || $this->attributes['title'] == "") && $def){
			return $def->title;
		}else{
			return $this->attributes['title'];
		}
        
    }
	
	function getKeywordsAttribute(){
		$def=Seocontent::where("page_name","default")->first();
		if((!$this->attributes['keywords'] || $this->attributes['keywords'] == "") && $def){
			return $def->keywords;
		}else{
			return $this->attributes['keywords'];
		}
        
    }
	
	function getDescriptionAttribute(){
		$def=Seocontent::where("page_name","default")->first();
		if((!$this->attributes['description'] || $this->attributes['description'] == "") && $def){
			return $def->description;
		}else{
			return $this->attributes['description'];
		}
        
    }
	
	
}
