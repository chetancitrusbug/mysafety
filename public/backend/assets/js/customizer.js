function changeImage(id){
    $('.changeImage'+id).hide();
}

jQuery(document).ready(function($) {
    statusChange();
});

function statusChange() {
    $('.status').checkboxpicker({
        offLabel: 'Inactive',
        onLabel: 'Active',
    });
    $('.status1').checkboxpicker();
}

function statusChange() {
    $('.status').checkboxpicker({
        offLabel: 'Inactive',
        onLabel: 'Active',
    });

    $('.status1').checkboxpicker();

    $('.status_invoice').checkboxpicker({
        offLabel: 'Inprogress',
        onLabel: 'Confirm',
    });
}

$(document).on('change', '.status-change', function (e) {
    $("#loading").show();
    var id = $(this).data('id');
    var status = $(this).data('status');
    var table = $(this).data('table');
    var me = $(this);
    
    var url = $(this).data('url');

    url = url+ "/" + table + '/' + status + '/' + id;

    if(table == 'plan'){
        var type = $(this).data('type');
        url = url + '/' + type;
    }

    $.ajax({
        url: url,
        success: function (data) {
            $("#loading").hide();
            if(data.message == 'Plan already exist' || data.message == 'Plan assigned to user'){
                datatable.draw();
                
                toastr.error(data.message);
            }else{
                $('.success-text').html(data.message);
                datatable.draw();
                toastr.success(data.message);
            }
            return true;
        },
        error: function (xhr, status, error) {
            jQuery('.delete-confirm-text').html("");
            jQuery('.delete-title').html("Action not proceed!");
            $('#danger').niftyModal();
        }
    });
});
var url = id = '';
$(document).on('click', '.del-item', function (e) {
    $("#loading").show();
    var id = $(this).data('id');
    var url = $(this).data('url');
    var msg = $(this).data('msg');
    url = url + "/" + id;

    $('#delete-form').attr('action',url);
    
    jQuery('.modal-body').html("Are you sure you want to delete " + msg +" ?");
});

$(".delete-confirm").on('click',function(){
    $("#loading").show();
    
    $('#delete-form').submit();
});

$('.delete-cancel').on('click',function(){
    $('#delete-form').attr('action','#');
    $("#loading").hide();
    datatable.draw();
})