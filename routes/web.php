<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('test', 'HomeController@index');

use App\PlanUser;

Auth::routes();

Route::get('/', function () {
    return redirect('/login');
});



Route::get('call-to-verify', 'TwillioController@initVerificationCall');
Route::get('call-answered', 'TwillioController@askforotp');
Route::post('call-response', 'TwillioController@callresponce')->name('call-response');

Route::get('click-funnel-store-data', 'Admin\DlinkController@store')->name('click-funnel');

Route::get('app-static-page', 'BlogController@page');
Route::get('/employee/activation/{email}/{token}', 'Admin\EmployeeController@activationForm');
Route::post('/employee/activation', 'Admin\EmployeeController@activationSubmit');



Route::get('/test/mail', 'Admin\SettingController@testmail');

	Route::group(['prefix' => 'admin','middleware' => ['auth', 'admin-employee']], function() {
		
		Route::get('childs/approve-badge/{id}', 'Admin\ChildController@approveForm')->name('approve-badge');
		Route::post('childs/approve-badge', 'Admin\ChildController@approveSubmit');
		
		Route::get('childs/disapproved-badge/{id}', 'Admin\ChildController@approveForm')->name('disapproved-badge');
		
		
		Route::resource('/childs', 'Admin\ChildController');
		Route::post('/childs-unsubscribe/{id}', 'Admin\ChildController@unsubscribe');
        Route::get('/childs-data/{id?}', 'Admin\ChildController@datatable');
        Route::get('/childs-data-approveby/{id?}', 'Admin\ChildController@datatableapproveBy');
        Route::get('/childs-approveby/{id?}', 'Admin\ChildController@approvebychild');
		Route::get('/childs-document/{id}', 'Admin\ChildController@viewDocument');
		
		Route::get('/', 'Admin\AdminController@index');
        Route::get('/edit_profile', 'Admin\AdminController@edit_profile');
        Route::post('/update_profile', 'Admin\AdminController@update_profile');

        Route::get('/change_password', 'Admin\AdminController@change_password');
        Route::post('/post_password', 'Admin\AdminController@post_password');
		Route::get('/employee-payment', 'Admin\EmployeeController@showMyPayment');
		
		Route::get('/employee-payment/datatable/{uid}', 'Admin\EmployeeController@paymentdatatable');
		
	});	
    Route::group(['prefix' => 'admin','middleware' => ['auth', 'admin']], function() {

		Route::get('/employee/payment/{uid}', 'Admin\EmployeeController@paymentForm');
		Route::post('/employee/payment', 'Admin\EmployeeController@paymentSubmit');
		
		Route::get('/employee/datatable', 'Admin\EmployeeController@datatable');
		Route::get('/employee/activation-resend/{uid}', 'Admin\EmployeeController@resendmail');
		Route::resource('/employee', 'Admin\EmployeeController');
		
		
		Route::resource('/logs', 'Admin\LogsController');
        Route::get('/logs-data', 'Admin\LogsController@datatable');
		
		Route::resource('/responce-text', 'Admin\ResponcetextController');
        Route::get('/responce-text-data', 'Admin\ResponcetextController@datatable');
		
		Route::resource('/seo-data', 'Admin\SeoController');
        Route::get('/seo-data-data', 'Admin\SeoController@datatable');
		
        
        
        Route::resource('/roles', 'Admin\RolesController');
        Route::get('/roles-data', 'Admin\RolesController@datatable');

        Route::resource('/blog', 'Admin\BlogController');
        Route::get('/blog-data', 'Admin\BlogController@datatable');

        Route::resource('/orders', 'Admin\OrderController');
        Route::get('/orders-data', 'Admin\OrderController@datatable');

        Route::resource('/users', 'Admin\UsersController');
		Route::get('/update-badge-status/users/{status}/{id}', 'Admin\UsersController@updateBadgeStatus');
        Route::get('/users-data', 'Admin\UsersController@datatable');

		Route::get('/file-delete/{id}', 'Admin\ChildController@deletefile');
		
        

        Route::resource('/plan', 'Admin\PlanController');
        Route::get('/plan-data', 'Admin\PlanController@datatable');
        Route::get('plan/generate-stripe-plan/{id}', 'Admin\PlanController@generateStripePlan');

        Route::get('/setting', 'Admin\SettingController@create');
        Route::post('/setting', 'Admin\SettingController@store');
        //status change
	    Route::get('/change-status/{table}/{status}/{id}/{type?}', function($table,$status,$id,$type=""){
	        $status = ($status == 0)?1:0;
            if($table == 'plan'){
                //check plan exists and active
                if($status == '1'){
                    $plan = \DB::table($table)->where('type',$type)->where('status',"1")->first();
                    if($plan){
                        return response()->json(['message' => 'Plan already exist'],200);
                    }
                }else{
                    $user = PlanUser::select([
                            'plan_user.id',
                        ])
                        ->join('users',function($join){
                            $join->on('users.id','=','plan_user.user_id');
                        })
                        ->where('plan_id',$id)
                        ->whereNull('users.deleted_at')
                        ->get();

                    if(count($user)){
                        return response()->json(['message' => 'Plan assigned to user'],200);
                    }
                }
                \DB::table($table)->where('id',$id)->update(['status'=>$status]);

                return response()->json(['message' => 'Status Updated'],200);
            }elseif($table == 'users' && $status == 0){
                \DB::table($table)->where('id',$id)->update(['status'=>$status]);                
                \DB::table($table)->where('parent_id',$id)->update(['status'=>$status]);
                \DB::table('plan_user')->where('user_id',$id)->update(['status'=>'0']);

                return response()->json(['message' => 'Status Updated'],200);
            }
            elseif($table == 'users' && $status == 2){
    	        \DB::table($table)->where('id',$id)->update(['status'=>1]);

    	        return response()->json(['message' => 'Status Updated'],200);
            }
            else{
    	        \DB::table($table)->where('id',$id)->update(['status'=>$status]);

    	        return response()->json(['message' => 'Status Updated'],200);
            }
	    });

    });
	


Route::get('cronrun', 'Admin\CronController@cronrun');



