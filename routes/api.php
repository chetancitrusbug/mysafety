<?php

/*
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *, Origin, Content-Type, Authorization, X-Auth-Token, x-xsrf-token ');
header('Access-Control-Allow-Methods: GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS'); */

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => 'set_globle_value'], function () {

Route::post('call-response', 'Api\Parent\TwillioController@callresponce')->name('call-response');
Route::post('call-end', 'Api\Parent\TwillioController@callStatus')->name('call-end');

Route::get('get-faqs', 'Api\FaqsController@allFaqs');	
Route::get('get-page', 'Api\BlogController@page');
Route::get('all-pages', 'Api\BlogController@allPages');

Route::get('all-blogs', 'Api\BlogController@allBlogs');
Route::get('get-blog', 'Api\BlogController@blog');
Route::get('send_sms', 'Api\Child\RegisterController@send_sms');


Route::post('get-ebook', 'Api\ContactController@getEbook');
Route::post('contact-us', 'Api\ContactController@contact_us');
Route::post('complain', 'Api\ContactController@complain');
Route::post('referral-program', 'Api\ContactController@referral');

Route::get('get-seopage', 'Api\SeoController@getSeopage');

Route::group(['prefix' => 'parent'], function () {

    Route::post('register', 'Api\Parent\RegisterController@register');
    Route::post('special-register', 'Api\Parent\DlinkController@register');
    Route::post('parent-child-register', 'Api\Parent\DlinkController@ParentChildRegister');
	Route::post('unique-field/{field}', 'Api\Parent\DlinkController@uniqueField');
    Route::post('login', 'Api\Parent\LoginController@login');
    Route::post('forgot_password', 'Api\Parent\LoginController@forgot_password');
	Route::post('update_forgot_password', 'Api\Parent\LoginController@updateForgotPassword');

    //subscription plan list
    Route::get('subscription-list', 'Api\Parent\RegisterController@subscriptionList');

    Route::group(['middleware' => 'jwt.verify'], function () {
        Route::get('logout', 'Api\Parent\LoginController@logout');
        Route::post('change_password', 'Api\Parent\LoginController@change_password');
        Route::post('register/edit', 'Api\Parent\RegisterController@update');
        Route::post('updateChild', 'Api\Parent\RegisterController@updateChild');
        Route::get('view_child', 'Api\Parent\RegisterController@viewChild');
        Route::get('order_detail', 'Api\Parent\RegisterController@orderDetail');
        Route::get('detail', 'Api\Parent\RegisterController@parentDetail');
        Route::get('childDetail', 'Api\Parent\RegisterController@childDetail');
        Route::post('unique_child_user_name', 'Api\Parent\RegisterController@unique_child');
        Route::post('unique_child_number', 'Api\Parent\RegisterController@unique_child_number');
       
        Route::post('child-register', 'Api\Parent\RegisterController@childRegister');
        Route::post('unsubscribe', 'Api\Parent\RegisterController@unsubscribe');
        Route::get('get_notification', 'Api\Child\RegisterController@get_notification');
        Route::post('update_notification', 'Api\Child\RegisterController@update_notification');
		
		Route::post('upload-document', 'Api\Parent\DocumentController@addDocument');

        Route::group(['prefix' => 'plan'], function () {
            Route::get('plan_list', 'Api\PlanController@index');
        });

        Route::post('upgrade-subscription', 'Api\Parent\SubscriptionController@upgradeSubscription');
		Route::get('get-plans-for-child', 'Api\Parent\SubscriptionController@getPlanlistForUser');
		
        Route::post('set-default-card', 'Api\Parent\SubscriptionController@setDefaultCard');
		Route::post('delete-card', 'Api\Parent\SubscriptionController@removeCard');
		Route::post('add-card', 'Api\Parent\SubscriptionController@addCard');
        Route::get('get-my-cards', 'Api\Parent\SubscriptionController@getMyCards');
		
		Route::get('get-phoneverification-code', 'Api\Parent\TwillioController@getPhoneVerificationOtp');
		Route::post('phoneverification-call', 'Api\Parent\TwillioController@phoneVerificationCall');
    });

});


Route::group(['prefix' => 'child'], function () {

    Route::post('login', 'Api\Child\LoginController@login');
    Route::post('forgot_password', 'Api\Child\LoginController@forgotPassword');
    Route::post('update_forgot_password', 'Api\Child\LoginController@updateForgotPassword');

    Route::group(['middleware' => 'jwt.verify'], function () {
        Route::get('logout', 'Api\Child\LoginController@logout');
        Route::post('change_password', 'Api\Child\LoginController@change_password');
        Route::post('register/edit', 'Api\Child\RegisterController@update');
        Route::post('view_batch_child', 'Api\Child\RegisterController@viewChildFromBatch');
        Route::post('send_notification', 'Api\Child\RegisterController@sendNotification');
        Route::get('childDetail', 'Api\Child\RegisterController@childDetail');
        Route::get('get_notification', 'Api\Child\RegisterController@get_notification');
        Route::post('update_notification', 'Api\Child\RegisterController@update_notification');
    });

});

});

