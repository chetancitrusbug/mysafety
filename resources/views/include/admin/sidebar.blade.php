<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="./"><img src="{{asset('uploads').'/'.session('setting.logo_image')}}" alt="Logo" style="height: 50px;"></a>
            <a class="navbar-brand hidden" href="./"><img src="{{asset('backend/assets/images/logo2.png')}}" alt="Logo"></a>
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
				@if(\Auth::user()->hasRole('AU'))
				<li class="{{($route == 'employee')?'active':''}}">
                    <a href="{{ url('admin/employee') }}">Employee </a>
                </li>
                <li class="{{($route == 'parent')?'active':''}}">
                    <a href="{{ url('admin/users') }}">Parents </a>
                </li>
				@endif
				
				@if(\Auth::user()->hasRole('EU'))
				
				<li class="{{($route == 'child')?'active':''}}">
                    <a href="{{ url('admin/childs') }}">Children </a>
                </li>
				
				<li class="{{($route == 'employee')?'active':''}}">
                    <a href="{{ url('admin/employee-payment') }}">Payments </a>
                </li>
				@endif	
               
				
				@if(\Auth::user()->hasRole('AU'))
					<li class="{{($route == 'child')?'active':''}}">
                    <a href="{{ url('admin/childs') }}">Children </a>
                </li>
				<li class="{{($route == 'seo-data')?'active':''}}">
                    <a href="{{ url('admin/seo-data') }}">Seo-data </a>
                </li>
                <li class="{{($route == 'roles')?'active':''}}">
                    <a href="{{ url('admin/roles') }}">Roles </a>
                </li>
                <li class="{{($route == 'plan')?'active':''}}">
                    <a href="{{ url('admin/plan') }}">Plan </a>
                </li>
                <li class="{{($route == 'blog')?'active':''}}">
                    <a href="{{ url('admin/blog') }}">Blog/Page</a>
                </li>
                <li class="{{($route == 'orders')?'active':''}}">
                    <a href="{{ url('admin/orders') }}">Orders </a>
                </li>
                <li class="{{($route == 'setting')?'active':''}}">
                    <a href="{{ url('admin/setting') }}">Settings</a>
                </li>
				<li class="{{($route == 'responce-text')? 'active':''}}">
                    <a href="{{ url('admin/responce-text') }}">Response Text</a>
                </li>
				<li class="{{($route == 'logs')? 'active':''}}">
                    <a href="{{ url('admin/logs') }}">Logs</a>
                </li>
				@endif
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
</aside>