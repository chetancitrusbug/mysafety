<link rel="stylesheet" href="{{asset('backend/assets/css/normalize.css')}}">
<link rel="stylesheet" href="{{asset('backend/assets/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/assets/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/assets/css/themify-icons.css')}}">
<link rel="stylesheet" href="{{asset('backend/assets/css/flag-icon.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/assets/css/cs-skin-elastic.css')}}">
<!-- <link rel="stylesheet" href="{{asset('backend/assets/css/bootstrap-select.less')}}"> -->
<link rel="stylesheet" href="{{asset('backend/assets/scss/style.css')}}">
<link href="{{asset('backend/assets/css/lib/vector-map/jqvmap.min.css')}}" rel="stylesheet">

<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="{{asset('backend/assets/css/your-style.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('backend/assets/css/lib/datatable/dataTables.bootstrap.min.css')}}">
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
