<script src="{{asset('backend/assets/js/vendor/jquery-2.1.4.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
<script src="{{asset('backend/assets/js/plugins.js')}}"></script>
<script src="{{asset('backend/assets/js/main.js')}}"></script>
{{-- <script src="{{asset('backend/assets/js/lib/chart-js/Chart.bundle.js')}}"></script> --}}
{{-- <script src="{{asset('backend/assets/js/dashboard.js')}}"></script> --}}
{{-- <script src="{{asset('backend/assets/js/widgets.js')}}"></script> --}}
<script src="{{asset('backend/assets/js/lib/vector-map/jquery.vmap.js')}}"></script>
<script src="{{asset('backend/assets/js/lib/vector-map/jquery.vmap.min.js')}}"></script>
<script src="{{asset('backend/assets/js/lib/vector-map/jquery.vmap.sampledata.js')}}"></script>
{{-- <script src="{{asset('backend/assets/js/lib/vector-map/country/jquery.vmap.world.js')}}"></script> --}}
<script src="{{asset('backend/assets/js/lib/data-table/datatables.min.js')}}"></script>
<script src="{{asset('backend/assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('backend/assets/js/lib/data-table/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('backend/assets/js/lib/data-table/datatables-init.js')}}"></script>
<script src="{{ asset('backend/assets/js/bootstrap-checkbox.js') }}" type="text/javascript" defer></script>
<script src="{{ asset('backend/assets/js/customizer.js') }}" type="text/javascript" defer></script>
{{-- <script src="{{asset('backend/assets/js/summernote.js')}}"></script> --}}
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

<script src="{!! asset('backend/assets/js/moment.min.js')!!}"></script>
<script src="{!! asset('backend/assets/js/daterangepicker.js')!!}"></script>
