<!--Default Alerts-->
@if (Session::has('flash_success'))
    <div class="alert  alert-success alert-dismissible fade show" role="alert">
         {{ Session::get('flash_success') }}.
    </div>
@endif

@if (Session::has('flash_message'))
    <div class="alert  alert-primary alert-dismissible fade show" role="alert">
        {{ Session::get('flash_message') }}.
    </div>
@endif

@if (Session::has('flash_warning'))
    <div class="alert  alert-warning alert-dismissible fade show" role="alert">
        {{ Session::get('flash_warning') }}.
    </div>
@endif

@if (session('flash_error'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('flash_error') }}
    </div>
@endif