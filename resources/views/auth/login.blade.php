@extends('layouts.app')

@section('title','LOGIN')

@section('content')

<div class="login-form">
    <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
        @csrf
        <div class="form-group {{ $errors->has('email') ? ' has-error' : ''}}">
            <label>Email address</label>
            <input id="email" type="text" placeholder="Email" autocomplete="off" class="form-control input-sm" name="email" value="{{ old('email') }}">
            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('password') ? ' has-error' : ''}}">
            <label>Password</label>
            <input id="password" type="password" placeholder="Password" name="password" class="form-control input-sm">
            {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" id="remember" name="remember" {{ old( 'remember') ? 'checked' : '' }}>
                Remember Me
            </label>
            <label class="pull-right">
                <a href="{{ route('password.request') }}">Forgotten Password?</a>
            </label>
        </div>
        <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">Sign in</button>
        <div class="register-link m-t-15 text-center">
            {{-- <p>Don't have account ? <a href="#"> Sign Up Here</a></p> --}}
        </div>
    </form>
</div>
@endsection


