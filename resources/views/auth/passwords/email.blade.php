@extends('layouts.app')

@section('title','FORGOT PASSWORD')

@section('content')
@if (session('status'))
<div class="alert alert-success alert-dismissible" role="alert">
    {{ session('status') }}
</div>
@endif
<div class="login-form">
    <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
        @csrf
        <div class="form-group {{ $errors->has('email') ? ' has-error' : ''}}">
            <label>Email address</label>
            {{-- <input type="email" class="form-control" placeholder="Email"> --}}
            <input type="email" name="email" required="" placeholder="Your Email" autocomplete="off" class="form-control input-sm" value="{{ old('email') }}">
            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
        </div>
        <button type="submit" class="btn btn-primary btn-flat m-b-15">Submit</button>

    </form>
</div>
@endsection


