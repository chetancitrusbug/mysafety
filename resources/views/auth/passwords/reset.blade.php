@extends('layouts.app')

@section('title','FORGOT PASSWORD')

@section('content')

<div class="login-form">
    <form method="POST" action="{{ route('password.request') }}" aria-label="{{ __('Reset Password') }}">
        @csrf
        <input type="hidden" name="token" value="{{ $token }}">

        <div class="form-group {{ $errors->has('email') ? ' has-error' : ''}}">
            <input type="email" name="email" required="" placeholder="Your Email" autocomplete="off" class="form-control input-sm" value="{{ old('email') }}">
            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
        </div>

        <div class="form-group {{ $errors->has('password') ? ' has-error' : ''}}">
            <input id="password" type="password" placeholder="Password" name="password" class="form-control input-sm">
            {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('password') ? ' has-error' : ''}}">
            <input id="password_confirmation" type="password" placeholder="Confirm Password" name="password_confirmation" class="form-control input-sm">
            {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
        </div>

        <button type="submit" class="btn btn-primary btn-flat m-b-15">Submit</button>

    </form>
</div>
    {{-- <div class="panel panel-default panel-border-color panel-border-color-primary">
        <div class="panel-heading app-background remove-margin">
            <img src="{{asset('backend/img/logo.png')}}" alt="Avatar" height="50px"><span class="span-title">3Yapp<span><span class="splash-description">Reset password?</span>
        </div>
        @if (session('status'))
        <div class="alert alert-success alert-dismissible" role="alert">
            {{ session('status') }}
        </div>
        @endif
        <div class="panel-body">
            <form method="POST" action="{{ route('password.request') }}" aria-label="{{ __('Reset Password') }}">
                @csrf
                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group xs-pt-20 {{ $errors->has('email') ? ' has-error' : ''}}">
                    <input type="email" name="email" required="" placeholder="Your Email" autocomplete="off" class="form-control input-sm" value="{{ old('email') }}">
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {{ $errors->has('password') ? ' has-error' : ''}}">
                    <input id="password" type="password" placeholder="Password" name="password" class="form-control input-sm">
                    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {{ $errors->has('password') ? ' has-error' : ''}}">
                    <input id="password_confirmation" type="password" placeholder="Confirm Password" name="password_confirmation" class="form-control input-sm">
                    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group xs-pt-5">
                    <button type="submit" class="btn btn-block btn-primary btn-xl app-green app-dark-green">Reset Password</button>
                </div>
            </form>
        </div>
    </div> --}}
@endsection
