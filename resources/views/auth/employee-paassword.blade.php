@extends('layouts.app')

@section('title','Employee Account Activation')

@section('content')

<div class="login-form">
	<h4> Set password and activate your account </h4>
    <form method="POST" action="{{ url('employee/activation') }}" >
        @csrf
		<input type="hidden" name="_vtoken" value="{{ $employee->token }}">
        <div class="form-group {{ $errors->has('email') ? ' has-error' : ''}}">
            <label>Email address</label>
            <input id="email" type="text" placeholder="Email" autocomplete="off" class="form-control input-sm" name="email" value="{{ $user->email }}">
            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('password') ? ' has-error' : ''}}">
            <label>Password</label>
            <input id="password" type="password" placeholder="Password" name="password" class="form-control input-sm">
            {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
        </div>
		
		<div class="form-group {{ $errors->has('password') ? ' has-error' : ''}}">
			<label>Confirm Password</label>
            <input id="password_confirmation" type="password" placeholder="Confirm Password" name="confirm_password" class="form-control input-sm">
            {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
        </div>
        
        <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">SET PASSWORD</button>
        
    </form>
</div>
@endsection


