<div class="modal fade" id="danger" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Delete Confrim</h5>
                <button type="button" class="close delete-cancel" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="delete-confirm-text">

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary delete-cancel" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary delete-confirm">Confirm</button>
            </div>
        </div>
    </div>
</div>