@push('css')
    <style type="text/css">
        .btn-group{
            border: 1px solid #aaa;
        }
    </style>
@endpush



<div class="row form-group {{ $errors->has('type') ? ' has-error' : ''}}">
    <div class="col col-md-3">
        {!! Form::label('responce_type', '*Responce type: ', ['class' => 'form-control-label']) !!}
    </div>
    <div class="col-12 col-md-9">
        {!! Form::select('responce_type', [''=>'select type']+config('admin.responce_type'), null,['class' => 'form-control']) !!}
        {!! $errors->first('responce_type', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="row form-group {{ $errors->has('type') ? ' has-error' : ''}}">
    <div class="col col-md-3">
        {!! Form::label('slug', '*Slug: ', ['class' => 'form-control-label']) !!}
    </div>
    <div class="col-12 col-md-9">
        
        
		<select class="form-control" id="slug" name="slug">
		<option value="">Select Slug</option>
         @foreach(\config('constant.responce_msg') as $k => $val)
			@if(old('slug'))
            <option value="{{$k}}" @if(old('slug') == $k) selected="selected" @endif>{{$k}}</option>
			@elseif(isset($activity))
				<option value="{{$k}}" @if(old('slug') == $k) selected="selected" @endif>{{$k}}</option>
			@else
			<option value="{{$k}}" >{{$k}}</option>
			@endif
         @endforeach
		 </select>
		 {!! $errors->first('slug', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('desc') ? ' has-error' : ''}}">
    {!! Form::label('desc', '*Description: ', ['class' => 'form-control-label']) !!}
    {!! Form::textarea('desc', old('description',null) , ['class' => 'form-control','id'=>'desc']) !!}
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>

  <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Create
                </button>
            </div>