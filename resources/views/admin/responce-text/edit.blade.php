@extends('layouts.admin')
@section('title',"Edit Plan")
@section('content')
<div class="col-lg-6">
    <div class="card">
        <div class="card-header"><strong>Plan</strong><small>Edit</small></div>

        {!! Form::model($plan, ['method' => 'PATCH','url' => ['/admin/plan', $plan->id],'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}

            <div class="card-body card-block">
                @include ('admin.plan.form')
            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Update
                </button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
