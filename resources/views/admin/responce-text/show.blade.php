@extends('layouts.admin')
@section('title',"Show Response Message")
@section('content')

<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
		
			Response Message # <strong>{{ $item->responce_type }}</strong>
			
			
            <a href="{{ url('/admin/responce-text') }}" title="Child List">
                <button class="btn btn-sm btn-space btn-warning pull-right">Back To List</button>
            </a>
        </div>
        <div class="card-body card-block">
            <table class="table table-bordered">
                <tbody>
					<tr>
                        <th>ID </th>
                        <td> {{$item->id}} </td>
                    </tr>
                    <tr>
                        <th>Log Type</th>
                        <td> {{ $item->responce_type }} </td>
                    </tr>
                    <tr>
                        <th>Slug</th>
                        <td> {{ $item->slug }} </td>
                    </tr>
					<tr>
                        <th>Created</th>
                        <td> {{ $item->created_at }} </td>
                    </tr>
					<tr>
                        <th>Detail</th>
                        <td> {{ $item->desc }} </td>
                    </tr>
					
                   
                </tbody>
            </table>
        </div>
    </div>
</div>


@endsection
