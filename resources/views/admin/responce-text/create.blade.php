@extends('layouts.admin')
@section('title',"Response Text/Message")
@section('content')
<div class="col-lg-6">
    <div class="card">
        <div class="card-header"><strong>Response Text/Message</strong><small> Create</small></div>

        {!! Form::open(['url' => '/admin/responce-text', 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
            <div class="card-body card-block">
                @include ('admin.responce-text.form')
            </div>

          
        {!! Form::close() !!}
    </div>
</div>
@endsection
