@extends('layouts.admin')
@section('title',"Response Text/Message")
@push('css')
    <style type="text/css">
        .btn-group{
            border: 1px solid #aaa;
        }
    </style>
@endpush

@section('content')
@include('modals.delete')

<div class="animated fadeIn">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Response text and messages</strong>
                    <a href="{{ url('/admin/responce-text/create') }}" title="Create">
                        <button class="btn btn-sm btn-space btn-success pull-right">Create</button>
                    </a>
                </div>
                <div class="card-body">
                    <table id="blog-table" class="table table-striped table-hover table-fw-widget">
                        <thead>
                            <tr>
                                <th># Id</th>
                                <th>Type</th>
                                <th>Slug</th>
                                <th>Message</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div><!-- .animated -->
@endsection

@push('js')
    <script>
        var url ="{{ url('/admin/responce-text-data') }}";
        var edit_url = "{{ url('/admin/responce-text') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#blog-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'id',name : 'id',"searchable": true, "orderable": true},
                { data: 'responce_type',name : 'responce_type',"searchable": true, "orderable": true},
                { data: 'slug',name : 'slug',"searchable": true, "orderable": true},
                { data: 'desc',name : 'desc',"searchable": true, "orderable": true},
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e=""; var v=""; var d= "";
                        v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='fa fa-eye' ></i></button></a>&nbsp;";

                        e = "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" class='btn btn-info btn-sm'><i class='fa fa-pencil'></i></a>&nbsp;";

                        d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-log'  data-id="+o.id+" ><i class='fa fa-trash' aria-hidden='true'></i></a>&nbsp;";
                        return v+d;
                    }

                }
            ]
        });

		$(document).on('click', '.del-log', function (e) {
			var id = $(this).attr('data-id');
			var r = confirm("Are you sure to delete this message ?");
			if (r == true) {
				$.ajax({
					type: "DELETE",
					url: "{{ url('/admin/responce-text') }}" + "/" + id,
					headers: {
						"X-CSRF-TOKEN": "{{ csrf_token() }}"
					},
					success: function (data) {
						datatable.draw();
						toastr.success('Action Success!', data.message)
					},
					error: function (xhr, status, error) {
						toastr.error('Action Not Procede!',erro)
					}
				});
			}
		});
        
</script>
@endpush