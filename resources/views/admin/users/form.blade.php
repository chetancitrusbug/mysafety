@push('css')
    {{-- <link id="bsdp-css" href="bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet"> --}}
    <link rel="stylesheet" href="{{asset('backend/assets/css/date/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/assets/css/date/bootstrap-datepicker.standalone.min.css')}}">

    <style type="text/css">
        .btn-group{
            border: 1px solid #aaa;
        }
        .logo_setting{
            position: relative;
        }
        .logo-image{
            position: absolute;
            top: 0px;
            right: 16px;
        }
        .changeImage1{
            height: 40px;
        }
    </style>
@endpush
<div class="form-group {{ $errors->has('first_name') ? ' has-error' : ''}}">
    {!! Form::label('first_name', '* First Name: ', ['class' => 'form-control-label']) !!}
    {!! Form::text('first_name', null, ['class' => 'form-control input-sm']) !!}
    {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('last_name') ? ' has-error' : ''}}">
    {!! Form::label('last_name', '* Last Name: ', ['class' => 'form-control-label']) !!}
    {!! Form::text('last_name', null, ['class' => 'form-control input-sm']) !!}
    {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email', '* Email: ', ['class' => 'form-control-label']) !!}
    @if (isset($user))
        {!! Form::text('email', null, ['class' => 'form-control input-sm','disabled']) !!}
    @else
        {!! Form::text('email', null, ['class' => 'form-control input-sm']) !!}
    @endif
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>

@if (!isset($user) || $user->roles[0]->name == 'Admin')
    <div class="form-group {{ $errors->has('password') ? ' has-error' : ''}}">
        {!! Form::label('password', '* Password: ', ['class' => 'form-control-label']) !!}
        {!! Form::password('password', ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="form-group {{ $errors->has('confirm_password') ? ' has-error' : ''}}">
        {!! Form::label('confirm_password', '* Confirm Password: ', ['class' => 'form-control-label']) !!}
        {!! Form::password('confirm_password', ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('confirm_password', '<p class="help-block">:message</p>') !!}
    </div>
@endif

<div class="form-group {{ $errors->has('phone') ? ' has-error' : ''}}">
    {!! Form::label('phone', '* Phone: ', ['class' => 'form-control-label']) !!}
    {!! Form::text('phone', null, ['class' => 'form-control input-sm']) !!}
    {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
</div>

@if (isset($user) && $user->roles[0]->name != 'Admin')
    <div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
        {!! Form::label('status', 'Status: ', ['class' => 'control-label']) !!}
        {!! Form::checkbox('status',1, isset($user->status)?$user->status:1, ['class'=>['checkbox','status']]) !!}
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
@endif
