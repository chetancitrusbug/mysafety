@extends('layouts.admin')
@section('title',"Children")
@push('css')
    <style type="text/css">
        .btn-group{
            border: 1px solid #aaa;
        }
    </style>
@endpush

@section('content')
@include('modals.delete')


<div class="animated fadeIn">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
				
                    <strong class="card-title">Children</strong>
					
					 <div class="form-group pull-right margin-left-10">
                        <select class="form-control input-sm filter" id="filter_refrence" name="refrence">
							<option value="">All</option>
							<option value="site">Website</option>
							<option value="special_link">Clickfunnel</option>
                        </select>
                     </div>
										  
					@if(\Auth::user()->hasRole('AU'))
						@if(request()->has('approve'))
							<a href="{{ url('/admin/childs') }}" title="Create">
								<button class="btn btn-sm btn-space btn-success pull-right">View All Children</button>
							</a>
						@else
								<div class="form-group pull-right margin-left-10">
                                              <select class="form-control input-sm filter" id="filter_status" name="filter">
												<option value="2">In Process</option>
												<option value="1">Active</option>
                                                <option value="0">Inactive</option>
												<option value="">ALL</option>
                                            </select>
                                          </div>
										  
										 
						@endif					  
					@else	
						
						@if(request()->has('approve'))
							<a href="{{ url('/admin/childs') }}" title="Create">
								<button class="btn btn-sm btn-space btn-success pull-right">In progress children</button>
							</a>
						@else
							<a href="{{ url('/admin/childs-approveby/') }}" title="Create">
								<button class="btn btn-sm btn-space btn-success pull-right">Approved By Me</button>
							</a>		
						@endif
											
					@endif

						
                                      
                </div>
                <div class="card-body">
                    <table id="child-table" class="table table-striped table-hover table-fw-widget">
                        <thead>
                            <tr>
								 <th>#ID</th>
                                <th> Name</th>
                                <th>Username</th>
                                <th>Phone</th>
                                <th>Child Age</th>
                                <th>Badge Status</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div><!-- .animated -->
@endsection

@push('js')
    <script>
        var url ="{{ url('/admin/childs-data') }}";
        var edit_url = "{{ url('/admin/childs') }}";
        var auth_check = "{{ Auth::check() }}";
		var img_url_default = "{{asset('backend/assets/images/default.png')}}";

        datatable = $('#child-table').DataTable({
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            ajax: {
                url:url,
                type:"get",
				data: function (d) {
					@if(request()->has('approve'))
						d.approve = {{request()->get('approve')}};
					@elseif(\Auth::user()->hasRole('AU'))
						d.status = $('#filter_status').val();
					@endif
					
					d.refrence = $('#filter_refrence').val();
					
				}
            },
			"drawCallback": function( settings ) {
                statusChanged();
            },
            columns: [
				{ data: 'id',name : 'id',"searchable": false, "orderable": true},
				{
                    "data": null,
                    "name" : 'full_name',
                    "searchable": true,
                    "orderable": true,
                    "render": function (o) {
						var imgtag = "";
                        if(o.thumb_image && o.thumb_image !=""){
							imgtag = o.thumb_image;
						}else{
							imgtag = img_url_default;
						}
						
						var res = '<img class="rounded-circle" src="'+imgtag+'" height="50"> &nbsp;&nbsp;&nbsp; <b>'+o.full_name+' </b>';
						
						return res;
                    }
                },
                { data: 'username',name : 'username',"searchable": true, "orderable": true},
                { data: 'phone',name : 'phone',"searchable": true, "orderable": true},
                { data: 'child_age',name : 'child_age',"searchable": true, "orderable": true},
              
                {
                    "data": null,
                    "name" : 'badge_status',
                    "searchable": false,
                    "orderable": false,
                    "render": function (o) {
							var rurl = "{{url('admin/update-badge-status')}}";
							
                            if(o.badge_status == "approved"){
                                return "Approved";
                            }
                            else if(o.badge_status == "disapproved"){
                                return "Disapproved";
                            }else 
                            {
                                return "In-Proggress";
                            }
                           
                        
                    }
                },
                {
                    "data": null,
                    "name" : 'Current Status',
                    "searchable": false,
                    "orderable": false,
                    "render": function (o) {
                      
                            if(o.status == 1){
                                return "<span class='label label-success'>Active</span>";
                            }
                            else if(o.status == 2){
                                return "<span class='label label-success'>In Proggress</span>";
                            }
							else
                            {
                                return "<span class='label label-primary'>Inactive</span>";
                            }
                    }
                },
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e=""; var v=""; var d= "";
                        v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='fa fa-eye' ></i></button> </a>&nbsp;";
						
						@if(\Auth::user()->hasRole('AU'))
					  d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-item' data-toggle='modal' data-target='#danger' data-id="+o.id+" data-url={{url('admin/childs')}} data-msg='user' data-backdrop='static' data-keyboard='false'><i class='fa fa-trash' aria-hidden='true'></i></a>&nbsp;";

						@endif
					
                       
                        return v+e+d;
                    }

                }
            ]
        });

		$('.filter').change(function() {
			datatable.draw();
		});
        function statusChanged() {
             $('.status_badge').checkboxpicker({
                offLabel: 'In-Proggress',
                onLabel: 'Approved',
            });
			$('.status_child').checkboxpicker({
             offLabel: 'Inprogress',
             onLabel: 'Confirm',
            });
        }
		
		$(document).on('click', '.status-change', function (e) {
			var id = $(this).attr('data-id');
			
			var r = true;
			if (r == true) {
				$.ajax({
					type: "get",
					url: "{{url('admin/update-badge-status')}}" + "?user_id" + id,
					success: function (data) {
						
					},
					error: function (xhr, status, error) {
						
					}
				});
			}
		});


          $(document).on('click', '.del-item', function (e) {

            var msg = $(this).data('msg');

            jQuery('.modal-body').html("Are you sure you want to delete " + msg +" ?");

            $('#danger').modal('show');
        });
</script>
@endpush