@extends('layouts.admin')
@section('title',"Parents")
@push('css')
    <style type="text/css">
        .btn-group{
            border: 1px solid #aaa;
        }
    </style>
@endpush

@section('content')
@include('modals.delete')


<div class="animated fadeIn">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Parents List</strong>
                    <a href="{{ url('/admin/users/create') }}" title="Create">
                        <button class="btn btn-sm btn-space btn-success pull-right">Create</button>
                    </a>
                </div>
                <div class="card-body">
                    <table id="blog-table" class="table table-striped table-hover table-fw-widget">
                        <thead>
                            <tr>
                                <th>#ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Created</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div><!-- .animated -->
@endsection

@push('js')
    <script>
        var url ="{{ url('/admin/users-data') }}";
        var edit_url = "{{ url('/admin/users') }}";
        var child_url = "{{ url('/admin/childs') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#blog-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[1,"ASC"]],
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'id',name : 'id',"searchable": false, "orderable": true},
                { data: 'full_name',name : 'full_name',"searchable": true, "orderable": true},
                { data: 'email',name : 'email',"searchable": true, "orderable": true},
                {
                    "data": null,
                    "name" : 'phone',
                    "searchable": true,
                    "orderable": false,
                    "render": function (o) {
						if(o.phone_verified == 1){
				return "<span class='badge badge-pill badge-success' title='Approved Phone number'> <i class='fa fa-check-circle-o' aria-hidden='true'></i> "+o.country_code+" "+o.phone+"</span>";
						}else{
				return "<span class='badge badge-pill badge-danger' title='Phone number not approved yet'> <i class='fa fa-exclamation-circle' aria-hidden='true'></i> "+o.country_code+" "+o.phone+"</span>";			
						}
                    }
                },
                { data: 'registration_date',name : 'registration_date',"searchable": true, "orderable": true},
                {
                    "data": null,
                    "name" : 'status',
                    "searchable": false,
                    "orderable": false,
                    "render": function (o) {
                        if(o.id == 1){
                            return 'Active';
                        }else if(!o.email && o.status == 0){
                            return 'Inactive';
                        }else{
                            if(o.status == 1){
                                return "<input type='checkbox' class='status status-change' data-group-cls='btn-group-sm' checked data-table='users' data-status="+o.status+" onchange='statusChange()'  data-url={{url('admin/change-status')}} value="+o.id+" data-id="+o.id+">&nbsp;";
                            }
                            return "<input type='checkbox' class='status status-change' data-group-cls='btn-group-sm' data-url={{url('admin/change-status')}} data-table='users' data-status="+o.status+" value="+o.id+" data-id="+o.id+">&nbsp;";
                        }
                    }
                },
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e=""; var v=""; var d= "";
                        v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='fa fa-eye' ></i></button></a>&nbsp;";

                        // if(o.roles[0] && (o.roles[0].name == 'Parent' || o.roles[0].name == 'Admin')){
                        e = "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" class='btn btn-info btn-sm'><i class='fa fa-pencil'></i></a>&nbsp;";
                        // }

                        if(o.roles[0] && o.roles[0].name != 'Admin'){
                            d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-item' data-toggle='modal' data-target='#danger' data-id="+o.id+" data-url={{url('admin/users')}} data-msg='user' data-backdrop='static' data-keyboard='false'><i class='fa fa-trash' aria-hidden='true'></i></a>&nbsp;";
                        }

                        return v+e+d;
                    }

                }
            ]
        });

        function statusChange() {
            $('.status').checkboxpicker({
                offLabel: 'Inactive',
                onLabel: 'Active',
            });
            $('.status1').checkboxpicker();

            $('.status_invoice').checkboxpicker({
                offLabel: 'Inprogress',
                onLabel: 'Confirm',
            });
        }


          $(document).on('click', '.del-item', function (e) {

            var msg = $(this).data('msg');

            jQuery('.modal-body').html("Are you sure you want to delete " + msg +" ?");

            $('#danger').modal('show');
        });
</script>
@endpush