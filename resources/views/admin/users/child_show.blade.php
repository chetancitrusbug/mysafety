@extends('layouts.admin')
@section('title',"Show Children")
@section('content')



<div class="col-lg-6">
    <div class="card">
        <div class="card-header">
		
			<strong>{{ $user->full_name }} @if($user->batch_id) ( {{ $user->batch_id }} ) @endif | @if($user->status==1) ACTIVE @elseif($user->status==2) IN-PROGRESS @else INACTIVE @endif </strong>
			
			
            <a href="{{ url('/admin/childs') }}" title="Child List">
                <button class="btn btn-sm btn-space btn-warning pull-right">Back To Child Listing</button>
            </a>
        </div>
        <div class="card-body card-block">
            <table class="table table-bordered">
                <tbody>
				
					<tr>
                        <td>Badge Status</td>
                        <td> @if($user->status==1) ACTIVE @elseif($user->status==2) IN-PROGRESS @else INACTIVE @endif
						

						</td>
                    </tr>
					<tr>
                        <th> Parent Name </th>
                        <th> @if($user->parent) <a href="{{ url('admin/users/'.$user->parent->id) }}" title="View Parent Detail "> {{ $user->parent->full_name }} </a> @endif </th>
                    </tr>
					<tr>
                        <th> Parent email </th>
                        <th>  {{ $user->parent->email }}   </th>
                    </tr>
					<tr>
                        <th> Parent phone </th>
                        <th> 
						@if($user->parent->phone_verified== "1")
							<span class='badge badge-pill badge-success' title='Approved Phone number'> <i class='fa fa-check-circle-o' aria-hidden='true'></i>
								{{ $user->parent->country_code }} {{ $user->parent->phone }} 
							</span>
						@else
							<span class='badge badge-pill badge-danger' title='Phone number not approved yet'> <i class='fa fa-exclamation-circle' aria-hidden='true'></i>
								{{ $user->parent->country_code }} {{ $user->parent->phone }} 
							</span>

						@endif
						</th>
                    </tr>
					@if($user->approver)
					<tr>
                        <th> Approved By  </th>
                        <th> {{ $user->approver->full_name }}</th>
                    </tr>
                    @endif
					<tr>
                        <td>First Name</td>
                        <td> {{ $user->first_name }} </td>
                    </tr>
                    <tr>
                        <td>Last Name</td>
                        <td> {{ $user->last_name }} </td>
                    </tr>
                    <tr>
                        <td>Username</td>
                        <td> {{ $user->username }} </td>
                    </tr>
                    <tr>
                        <td>Gender</td>
                        <td> {{ ucfirst($user->gender) }} </td>
                    </tr>
                    <tr>
                        <td>Age</td>
                        <td> {{ $user->child_age }}  Year</td>
                    </tr>
                    <tr>
                        <td>Date Of Birth</td>
                        <td> {{ $user->dob }} </td>
                    </tr>
                    <tr>
                        <td>Phone No</td>
                        <td> {{ $user->phone }} </td>
                    </tr>
                    <tr>
                        <td>Batch Id</td>
                        <td> {{ $user->batch_id }} </td>
                    </tr>
					 <tr>
                        <td>Registration Reference From</td>
                        <td> 
							@if($user->regi_refrence && $user->regi_refrence =="site")
								Website
							@else
								Clickfunnel
							@endif
						</td>
                    </tr>
					<tr>
                        <td>Profile Image</td>
                        <td> 
							@if($user->thumb_image && $user->thumb_image !="")
								<img class="" src="{{$user->thumb_image}}" height="150">
							@else
								<img class="" src="{{asset('backend/assets/images/default.png')}}" height="150">
							@endif
						</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

@if($user->refefile)
	
<div class="col-lg-6">
    <div class="card">
        <div class="card-header">
		
			<strong>Documents</strong>
			@if($user->status==2)
			<a href="{{ url('admin/childs/approve-badge') }}/{{ $user->id }}" title="Approve child Badge">
                <button class="btn btn-sm btn-space btn-success pull-right">Approve child Badge</button>
            </a>
			
			<a href="{{ url('admin/childs/disapproved-badge') }}/{{ $user->id }}" title="Disapproved">
                <button class="btn btn-sm btn-space btn-danger pull-right">Disapprove</button>
            </a>
            @endif
        </div>
        <div class="card-body card-block">
            <table class="table table-bordered">
                <tbody>
				<tr>
                    <th>File name</td>
					<th>Action</td>
				</tr>
				
				@foreach($user->refefile as $refefile)
				@if($refefile->file_url)
					<tr>
                        <th> <a href="{{ url(''.$refefile->file_url) }}" title="View Document" target="_blank" > {{$refefile->refe_file_name}} </a> </th>
						<th> 
							<a href="{{ url('/admin/file-delete/'.$refefile->id) }}" onclick="return confirm('Are you sure to delete this document ?')">
								<button class="btn btn-warning btn-sm" title="Delete"><i class="fa fa-trash"></i></button>
							</a>
							<a href="{{ url(''.$refefile->file_url) }}" target="_blank" >
								<button class="btn btn-warning btn-sm" title="Download"  ><i class="fa fa-download"></i></button>
							</a>
						</th>
                    </tr>
					
				@endif
				@endforeach
					
                    
                </tbody>
            </table>
        </div>
    </div>
</div>

@endif
<div class="col-lg-6">
    <div class="card">
        <div class="card-header"><strong>Subscriptions / Orders</strong>
           
        </div>
        <div class="card-body card-block">
            <table class="table table-bordered">
                <tbody>
				<tr>
                    <th>Plan Name</td>
					<th>Status</td>
					<th>Duration</td>
					<th>Action</td>
				</tr>
				@foreach($user->childorders as $order)
                    <tr>
                        <td>
								@if($order->plan) <b> {{ $order->plan->title }} </b> | @if($order->plan->type != "free") $ {{ strtoupper($order->plan->actual_amount) }}-@endif {{ strtoupper($order->plan->type) }} @endif
						</td>
                        <td>   {{ strtoupper($order->trans_status) }} </td>
						<td>   {{ $order->start_date_formated }} -  {{ $order->expiry_date_formated }} </td>
						<td>
								@if(\Auth::user()->hasRole('AU'))
										{!! Form::open([
                                            'method'=>'POST',
                                            'url' => ['/admin/childs-unsubscribe', $order->id],
                                            'style' => 'display:inline'
                                        ]) !!}
                                        {!! Form::button('<i class="fa fa-undo"></i>', array(
                                                'type' => 'submit',
                                                'class' => 'btn btn-warning btn-sm',
                                                'title' => 'Unsubscribe',
                                                'onclick'=>'return confirm("Are you sure to unsubscribe child ? User will be Inactived on Unsubscribe.")'
                                        )) !!}
                                        {!! Form::close() !!}
							
							
							<a href="{{ url('/admin/orders/'.$order->id) }}" >
								<button class="btn btn-warning btn-sm" title="View"><i class="fa fa-eye"></i></button>
							</a>
							@else
								-
							@endif
							
						</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
	
</div>

@endsection
