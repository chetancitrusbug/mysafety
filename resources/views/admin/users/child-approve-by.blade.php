@extends('layouts.admin')
@section('title',"Children")
@push('css')
    <style type="text/css">
        .btn-group{
            border: 1px solid #aaa;
        }
    </style>
@endpush

@section('content')
@include('modals.delete')


<div class="animated fadeIn">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
					<div class="row">
						<div class="col-md-6">
                    <strong class="card-title">Children Processed By : {{$user->full_name}}</strong>
					</div>
					<div class="col-md-6">
					 <div style="width: 100%">
                                                    <div id="reportrange"  style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%;height: 32px;">
                                                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                                        <span></span> <b class="caret"></b>
                                                    </div>

                                                </div>		  
                                      {!! Form::hidden('from_date',\Request::get('from_date'), ['class' => 'form-control','id'=>'from_date']) !!}
							{!! Form::hidden('to_date',\Request::get('to_date'), ['class' => 'form-control','id'=>'to_date']) !!}
							</div>
							</div>
                </div>
                <div class="card-body">
                    <table id="child-table" class="table table-striped table-hover table-fw-widget">
                        <thead>
                            <tr>
								<th>#ID</th>
                                <th> Name</th>
                                <th>Username</th>
                                <th>Phone</th>
                                <th>Badge Status</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div><!-- .animated -->
@endsection

@push('js')
    <script>
        var url ="{{ url('/admin/childs-data-approveby') }}";
        var edit_url = "{{ url('/admin/childs') }}";
        var auth_check = "{{ Auth::check() }}";
		var img_url_default = "{{asset('backend/assets/images/default.png')}}";

        datatable = $('#child-table').DataTable({
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            ajax: {
                url:url,
                type:"get",
				data: function (d) {
					d.approve = {{$user->id}};
					d.to_date = $('#to_date').val();
					d.from_date = $('#from_date').val();
					
			
					
				}
            },
			"drawCallback": function( settings ) {
                statusChanged();
            },
            columns: [
				{ data: 'id',name : 'id',"searchable": false, "orderable": true},
				{
                    "data": null,
                    "name" : 'full_name',
                    "searchable": true,
                    "orderable": true,
                    "render": function (o) {
						var imgtag = "";
                        if(o.thumb_image && o.thumb_image !=""){
							imgtag = o.thumb_image;
						}else{
							imgtag = img_url_default;
						}
						
						var res = '<img class="rounded-circle" src="'+imgtag+'" height="50"> &nbsp;&nbsp;&nbsp; <b>'+o.full_name+' </b>';
						
						return res;
                    }
                },
                { data: 'username',name : 'username',"searchable": true, "orderable": true},
                { data: 'phone',name : 'phone',"searchable": true, "orderable": true},
				{
                    "data": null,
                    "name" : 'badge_status',
                    "searchable": false,
                    "orderable": false,
                    "render": function (o) {
							var rurl = "{{url('admin/update-badge-status')}}";
							
                            if(o.badge_status == "approved"){
                                return "Approved";
                            }
                            else 
                            {
                                return "In-Proggress";
                            }
                           
                        
                    }
                },
                {
                    "data": null,
					"name" : 'atime',
                    "searchable": false,
                    "orderable": true,
                    "width":150,
                    "render": function (o) {
                        return moment.utc(o.atime,'YYYY-MM-DD').format('MMM Do YYYY');
                    }

                }
            ]
        });

		
		 /*************************daterange selection*****************/

	var range_start = "";
    var range_end = "";

    var start = moment.utc('2017-01-01','YYYY-MM-DD');
    var end = moment();
	
	if($('#from_date').val() && $('#from_date').val() !="" && $('#to_date').val() && $('#to_date').val() !="" ){
		start = moment.utc($('#from_date').val(),'YYYY-MM-DD');
		end = moment.utc($('#to_date').val(),'YYYY-MM-DD');
	}
    
    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        if(range_start==""){
            range_start = start.format('YYYY-MM-DD');
            range_end = end.format('YYYY-MM-DD');
			
			$('#to_date').val(range_end);
			$('#from_date').val(range_start);
        }else{
            range_start = start.format('YYYY-MM-DD');
            range_end = end.format('YYYY-MM-DD');
			
			
			$('#to_date').val(range_end);
			$('#from_date').val(range_start);

            datatable.draw();
        }
		

    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            "All":[moment.utc('2017-01-01','YYYY-MM-DD'),moment()],
            "Today": [moment(), moment()],
            "This Week": [moment().startOf('week'), moment().endOf('week')],
            "Last Week": [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
            "This Month": [moment().startOf('month'), moment().endOf('month')],
            "Last Month": [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            
        }
    }, cb);

     cb(start, end);
	 
	 
	 
		$('.filter').change(function() {
			datatable.draw();
		});
        function statusChanged() {
             $('.status_badge').checkboxpicker({
                offLabel: 'In-Proggress',
                onLabel: 'Approved',
            });
			$('.status_child').checkboxpicker({
             offLabel: 'Inprogress',
             onLabel: 'Confirm',
            });
        }
		
		
</script>
@endpush