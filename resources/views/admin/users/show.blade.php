@extends('layouts.admin')
@section('title',"Show Parent")
@section('content')
<div class="col-lg-6">
    <div class="card">
        <div class="card-header"><strong>Parent</strong><small> Detail</small>
            <a href="{{ url('/admin/users') }}" title="Parent List">
                <button class="btn btn-sm btn-space btn-warning pull-right">Back To Parents</button>
            </a>
        </div>
        <div class="card-body card-block">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td>First Name</td>
                        <td> {{ $user->first_name }} </td>
                    </tr>
                    <tr>
                        <td>Last Name</td>
                        <td> {{ $user->last_name }} </td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td> {{ $user->email }} </td>
                    </tr>
                    <tr>
                        <td>Phone No</td>
                        <td>
						@if($user->phone_verified== "1")
							<span class='badge badge-pill badge-success' title='Approved Phone number'> <i class='fa fa-check-circle-o' aria-hidden='true'></i>
								{{ $user->country_code }} {{ $user->phone }} 
							</span>
						@else
							<span class='badge badge-pill badge-danger' title='Phone number not approved yet'> <i class='fa fa-exclamation-circle' aria-hidden='true'></i>
								{{ $user->country_code }} {{ $user->phone }} 
							</span>

						@endif	
						</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="col-lg-6">
    <div class="card">
        <div class="card-header"><strong>Child List</strong>
            <a href="{{ url('/admin/users') }}" title="Parent List">
                <button class="btn btn-sm btn-space btn-warning pull-right">|</button>
            </a>
        </div>
        <div class="card-body card-block">
            <table class="table table-bordered">
                <tbody>
				@foreach($user->childs as $child)
                    <tr>
                        <td>
						<a href="{{ url('admin/childs/'.$child->id) }}" title="View Child Detail ">
							@if($child->thumb_image && $child->thumb_image !="")
								<img class="rounded-circle" src="{{$child->thumb_image}}" height="50">
							@else
								<img class="rounded-circle" src="{{asset('backend/assets/images/default.png')}}" height="50">
							@endif
							
							&nbsp;&nbsp;&nbsp; <b>{{ $child->full_name }} </b>
							</a>
						</td>
                        <td>   {{ $child->child_age }} Year | {{ $child->username }} </td>
						<td>   {{ $child->device_type }}  </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
	
</div>
@endsection
