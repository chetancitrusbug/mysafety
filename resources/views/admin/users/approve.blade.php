@extends('layouts.admin')
@section('title',"Show Child")
@section('content')




<div class="col-lg-12">
    <div class="card">
		@if ($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
        <div class="card-header">
		
			<strong>{{ $user->full_name }} @if($user->batch_id) ( {{ $user->batch_id }} ) @endif | @if($user->status==1) ACTIVE @elseif($user->status==2) IN-PROGRESS @else INACTIVE @endif </strong>
			
			
            <a href="{{ url('/admin/childs') }}" title="Child List">
                <button class="btn btn-sm btn-space btn-warning pull-right">Back To Childs</button>
            </a>
        </div>
		
        
    </div>
</div>


@php( $image_count = 0 )

<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
		
			<strong>Uploaded Document</strong>
			
			
            <a href="#" title="Child List">
              
            </a>
        </div>
		<div class="card-body card-block">
		
			<div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

					@foreach($user->refefile as $refefile)
				@if($refefile->file_url)
					
				@php( $path_info = pathinfo($refefile->refe_file_name) )
				
					@if(isset($path_info['extension']) && in_array($path_info['extension'],['jpg','jpeg','png','PNG','JPEG','JPG']))
						@php( $image_count++ )
					@else	 
						
					
					<div class="col-md-4">
                        <div class="feed-box text-center">
                            <section class="card">
                                <div class="card-body">
                                    <div class="corner-ribon blue-ribon">
                                        <i class="fa fa-twitter"></i>
                                    </div>
									@php( $v_url = url('/admin/childs-document/'.$user->id)."?file=".$refefile->refe_file_name )
									
									@if(isset($path_info['extension']) && in_array($path_info['extension'],['jpg','jpeg','png','PNG','JPEG','JPG']))
					
										
										<a href="{{ $v_url }}" target="_blank">
											<img class="align-self-center  mr-3" style="width:85px; height:85px;" alt="" src="{{ url(''.$refefile->file_url) }}">
										</a>
								 
								 
									  @elseif(isset($path_info['extension']) && in_array($path_info['extension'],['docx','docm','docb','dotx','dotm','dotb']))
										<a href="{{ $v_url }}" target="_blank">
											<img class="align-self-center  mr-3" style="width:85px; height:85px;" alt="" src="{{asset('backend/assets/images/word.png')}}">
										</a>
									  @elseif(isset($path_info['extension']) && in_array($path_info['extension'],['pdf']))
										 <a href="{{ $v_url }}" target="_blank">
											<img class="align-self-center  mr-3" style="width:85px; height:85px;" alt="" src="{{asset('backend/assets/images/apdf.png')}}">
										</a>
									  @elseif(isset($path_info['extension']) && in_array($path_info['extension'],['xlsx','csv']))
										 <a href="{{ $v_url }}" target="_blank">
											<img class="align-self-center  mr-3" style="width:85px; height:85px;" alt="" src="{{asset('backend/assets/images/excel.png')}}">
										</a>
									  @else	 
											<a href="{{ $v_url }}" target="_blank">
												<img class="align-self-center  mr-3" style="width:85px; height:85px;" alt="" src="{{asset('backend/assets/images/file.png')}}">
											</a>
									  @endif
				  
				  
                                    
                                    <h2>{{$refefile->refe_file_name}}</h2>
                                    <p>Uploded on {{$refefile->created_formated}}</p>
                                </div>
                            </section>
                        </div>
						</div>
					 @endif
					
					@endif
				@endforeach
				</div>
			</div>
		</div>
			
        </div>
    </div>
</div>


<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
			<strong>Uploaded Image</strong>
		</div>
		<div class="card-body card-block">
		
			<div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
				
					@if($user->user_image && $user->user_image !="") 
						<div class="col-md-6">
                        <div class="feed-box text-center">
                            <section class="card">
                                <div class="card-body">
                                    <div class="corner-ribon blue-ribon">
                                        <i class="fa fa-twitter"></i>
                                    </div>
									
									<a href="{{ url('/admin/childs-document/'.$user->id) }}?file=profilepic" target="_blank">
											<img class="align-self-center  mr-3" style="width:100%;" alt="" src="{{$user->user_image}}">
									</a>
									<h2>{{ $user->full_name }}</h2>
                                    <p>Profile Image</p>
                                </div>
                            </section>
                        </div>
                    </div>
					@endif

					@foreach($user->refefile as $refefile)
				@if($refefile->file_url)
					
					@php( $path_info = pathinfo($refefile->refe_file_name) )
					
					@php( $v_url = url('/admin/childs-document/'.$user->id)."?file=".$refefile->refe_file_name )
					
					@if(isset($path_info['extension']) && in_array($path_info['extension'],['jpg','jpeg','png','PNG','JPEG','JPG']))
					<div class="col-md-6">
                        <div class="feed-box text-center">
                            <section class="card">
                                <div class="card-body">
                                    <div class="corner-ribon blue-ribon">
                                        <i class="fa fa-twitter"></i>
                                    </div>
									
									<a href="{{ $v_url }}" target="_blank">
											<img class="align-self-center  mr-3" style="width:100%;" alt="" src="{{ url(''.$refefile->file_url) }}">
									</a>
									<h2>{{$refefile->refe_file_name}}</h2>
                                    <p>Uploded on {{$refefile->created_formated}}</p>
                                </div>
                            </section>
                        </div>
                    </div>
					@endif
					
					@endif
				@endforeach
				</div>
			</div>
		</div>
			
        </div>
    </div>
</div>


@if($user->status==2)
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
				@if(\Request::route()->getName() == "disapproved-badge")
					<strong>Child's profile validation </strong>
				@else	
                    <strong>Child's profile validation and approve Badge</strong>
				@endif
			
			
			
            <a href="#" title="Child List">
              
            </a>
        </div>
		<div class="card-body card-block">
		{!! Form::open(['url' => ['admin/childs/approve-badge'],'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
		{!! Form::hidden('user_id',$user->id, ['class' => 'form-control','id'=>'user_id']) !!}
		
		<?php
			$default_file = null;
			if($user->refefile && $user->refefile->count() > 0){
				foreach($user->refefile as $refefile){
					if($refefile->file_url && $refefile->file_url!=""){
						$default_file = $refefile->file_url;
					}
				}
			}
		?>
		
		
		 
            <table class="table table-bordered">
                <tbody>
				<tr>
                    <th>
					<div class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0 ml-3 checkbox child_profile">
                                                <input type="checkbox" name="child_profile" class="custom-control-input "  id="child_profile" value='child_profile' {{ old('child_profile') ? 'checked' : '' }}>
                                                <label class="custom-control-label float-left white" for="child_profile"></label>
                                            </div>
					</th>
					<th>Review the Child Profile Picture   </td>
				</tr>
				<tr>
                    <th>
					<div class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0 ml-3 checkbox parent_id">
                                                <input type="checkbox" name="parent_id" class="custom-control-input "  id="parent_id" value='parent_id' {{ old('parent_id') ? 'checked' : '' }}>
                                                <label class="custom-control-label float-left white" for="parent_id"></label>
                                            </div>
					</td>
					<th>Review the Parent ID  </td>
				</tr>
				<tr>
                    <th>
					<div class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0 ml-3 checkbox child_id">
                                                <input type="checkbox" name="child_id" class="custom-control-input "  id="child_id" value='child_id' {{ old('child_id') ? 'checked' : '' }}>
                                                <label class="custom-control-label float-left white" for="child_id"></label>
                                            </div>
					</td>
					<th> Review the Child ID</td>
				</tr>
				<tr>
                    <th>
					<div class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0 ml-3 checkbox sign_permission_form">
                                                <input type="checkbox" name="sign_permission_form" class="custom-control-input "  id="sign_permission_form" value='Lincoln Sentry' {{ old('sign_permission_form') ? 'checked' : '' }}>
                                                <label class="custom-control-label float-left white" for="sign_permission_form"></label>
                                            </div>
					</td>
					<th>Review the Signed Parental Permission FORM </td>
				</tr>
				<tr>
                    <th>
					<div class=" {{ $errors->has('permission_form_child_parent') ? ' has-error' : ''}} custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0 ml-3 checkbox permission_form_child_parent">
                                                <input type="checkbox" name="permission_form_child_parent" class="custom-control-input "  id="permission_form_child_parent" value='Lincoln Sentry' {{ old('permission_form_child_parent') ? 'checked' : '' }}>
                                                <label class="custom-control-label float-left white" for="permission_form_child_parent"></label>
                                            </div>
					</td>
					<th>Review the picture of Child and Parent Holding Parental Permission FORM  </td>
				</tr>
				
				
					
                    
                </tbody>
            </table>
			
			

			@if(\Request::route()->getName() == "disapproved-badge")
			<div class="form-group{{ $errors->has('desc') ? ' has-error' : ''}}">
				{!! Form::label('desc', 'Reason for profile not approvable ', ['class' => 'form-control-label']) !!}
				{!! Form::textarea('desc', old('description',null) , ['rows'=>3,'class' => 'form-control','id'=>'desc']) !!}
				{!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
			</div>	
				{!! Form::hidden('badge_status','disapproved') !!}	
			@else
				{!! Form::hidden('badge_status','approved') !!}	
			@endif
				
			<button type="submit" class="btn btn-primary btn-sm">
				@if(\Request::route()->getName() == "disapproved-badge")
					Confirm and disapprove profile 
				@else	
                    Confirm and approve profile 
				@endif
            </button>
			{!! Form::close() !!}
        </div>
    </div>
</div>
@endif

	




@endsection
