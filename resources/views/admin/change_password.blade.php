@extends('layouts.admin')
@section('title',"Edit Profile")
@section('content')
<div class="col-lg-6">
    <div class="card">
        <div class="card-header"><strong>User Edit<strong></div>

         <form method="POST" action="{{ url('/admin/post_password') }}" class="form-horizontal" enctype="multipart/form-data">
            
            <div class="card-body card-block">
            {{ csrf_field() }}
            <div class="form-group {{ $errors->has('email') ? ' has-error' : ''}}">
                    {!! Form::label('email', '* Email: ', ['class' => 'form-control-label']) !!}
                    @if (isset($user))
                        {!! Form::text('email', $user->email, ['class' => 'form-control input-sm','disabled']) !!}
                    @else
                        {!! Form::text('email', null, ['class' => 'form-control input-sm']) !!}
                    @endif
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                </div>

                <div class="form-group {{ $errors->has('password') ? ' has-error' : ''}}">
                    {!! Form::label('password', '* Password: ', ['class' => 'form-control-label']) !!}
                    {!! Form::password('password', ['class' => 'form-control input-sm']) !!}
                    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                </div>

                <div class="form-group {{ $errors->has('confirm_password') ? ' has-error' : ''}}">
                    {!! Form::label('confirm_password', '* Confirm Password: ', ['class' => 'form-control-label']) !!}
                    {!! Form::password('confirm_password', ['class' => 'form-control input-sm']) !!}
                    {!! $errors->first('confirm_password', '<p class="help-block">:message</p>') !!}
                </div>
              
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Update
                </button>
            </div>
        </form>
    </div>
</div>
@endsection