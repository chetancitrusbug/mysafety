<div class="form-group {{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', '* Name: ', ['class' => 'form-control-label']) !!}
    @if(isset($role) && $role->name != '')
    {!! Form::text('name', null, ['class' => 'form-control input-sm','readonly'=>true]) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    @else
    {!! Form::text('name', null, ['class' => 'form-control input-sm']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    @endif
</div>

<div class="form-group">
    {!! Form::label('label', 'Label: ', ['class' => 'form-control-label']) !!}
    {!! Form::text('label', null, ['class' => 'form-control input-sm']) !!}
    {!! $errors->first('label', '<p class="help-block">:message</p>') !!}
</div>
