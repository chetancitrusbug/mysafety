@extends('layouts.admin')
@section('title',"Edit Role")
@section('content')
<div class="col-lg-6">
    <div class="card">
        <div class="card-header"><strong>Roles</strong><small> Edit</small></div>

        {!! Form::model($role, ['method' => 'PATCH','url' => ['/admin/roles', $role->id],'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}

            <div class="card-body card-block">
                    @include ('admin.roles.form')
            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Update
                </button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
