@extends('layouts.admin')
@section('title',"Create Role")
@section('content')
<div class="col-lg-6">
    <div class="card">
        <div class="card-header"><strong>Roles</strong><small> Create</small></div>

        {!! Form::open(['url' => '/admin/roles', 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
            <div class="card-body card-block">
                @include ('admin.roles.form')

            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Create
                </button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
