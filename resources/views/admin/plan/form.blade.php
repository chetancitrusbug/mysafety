@push('css')
    <style type="text/css">
        .btn-group{
            border: 1px solid #aaa;
        }
    </style>
@endpush
<div class="form-group {{ $errors->has('title') ? ' has-error' : ''}}">
    {!! Form::label('title', '*Title: ', ['class' => 'form-control-label']) !!}
    {!! Form::text('title', null, ['class' => 'form-control input-sm']) !!}
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('actual_amount') ? ' has-error' : ''}}">
    {!! Form::label('actual_amount', '* Actual Amount: ', ['class' => 'form-control-label']) !!}
    {!! Form::text('actual_amount', null, ['class' => 'form-control input-sm']) !!}
    {!! $errors->first('actual_amount', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('show_amount') ? ' has-error' : ''}}">
    {!! Form::label('show_amount', '*Show Amount: ', ['class' => 'form-control-label']) !!}
    {!! Form::text('show_amount', null, ['class' => 'form-control input-sm']) !!}
    {!! $errors->first('show_amount', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('description') ? ' has-error' : ''}}">
    {!! Form::label('description', '*Description: ', ['class' => 'form-control-label']) !!}
    {!! Form::textarea('description', old('description',null) , ['class' => 'form-control','id'=>'description']) !!}
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>

<div class="row form-group {{ $errors->has('type') ? ' has-error' : ''}}">
    <div class="col col-md-3">
        {!! Form::label('type', '*Type: ', ['class' => 'form-control-label']) !!}
    </div>
    <div class="col-12 col-md-9">
        {!! Form::select('type', [''=>'select type']+config('admin.plan_type'), null,['class' => 'form-control']) !!}
        {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
    {!! Form::label('status', 'Status: ', ['class' => 'control-label']) !!}
    {!! Form::checkbox('status',1, isset($plan->status)?$plan->status:1, ['class'=>['checkbox','status']]) !!}
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>