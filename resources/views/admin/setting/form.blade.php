@push('css')
    {{-- <link id="bsdp-css" href="bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet"> --}}
    <link rel="stylesheet" href="{{asset('backend/assets/css/date/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/assets/css/date/bootstrap-datepicker.standalone.min.css')}}">

    <style type="text/css">
        .btn-group{
            border: 1px solid #aaa;
        }
        .logo_setting{
            position: relative;
        }
        .logo-image{
            position: absolute;
            top: 0px;
            right: 16px;
        }
        .changeImage1{
            height: 40px;
        }
    </style>
@endpush

<div class="row form-group {{ $errors->has('date_format') ? ' has-error' : ''}}">
    <div class="col col-md-6">
        {!! Form::label('date_format', 'Date Format: ', ['class' => 'form-control-label']) !!}
    </div>
    <div class="col-12 col-md-6">
        {!! Form::select('date_format',config('admin.date_format'), isset($setting['date_format'])?$setting['date_format']:null,['class' => 'form-control']) !!}
        {!! $errors->first('date_format', '<p class="help-block">:message</p>') !!}
    </div>
</div>

{!! Form::hidden('old_image', isset($setting['logo_image'])?$setting['logo_image']:'') !!}
<div class="row form-group {{ $errors->has('logo_image') ? ' has-error' : ''}}">
    <div class="col col-md-6">
        {!! Form::label('logo_image', 'Logo Image (png format only): ', ['class' => 'form-control-label']) !!}
    </div>
    <div class="col-12 col-md-6">
        <input type="file" id="file-input" name="logo_image" class="form-control-file" data-ext="png">
        @if(isset($setting))
            <div class="logo-image"><img src="{{ asset('uploads').'/'.$setting['logo_image'] }}" class="changeImage1"></div>
        @endif
    </div>
    {!! $errors->first('logo_image', '<p class="help-block" style="margin-left:15px;">:message</p>') !!}
</div>

<div class="row form-group {{ $errors->has('pagination') ? ' has-error' : ''}}">
    <div class="col col-md-6">
        {!! Form::label('pagination', 'Pagination for API: ', ['class' => 'form-control-label']) !!}
    </div>
    <div class="col-12 col-md-6">
        {!! Form::number('pagination',$setting['pagination'] ,['class' => 'form-control','min'=>'1']) !!}
        {!! $errors->first('pagination', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="row form-group {{ $errors->has('amount_per_user_approve') ? ' has-error' : ''}}">
    <div class="col col-md-6">
        {!! Form::label('amount_per_user_approve', 'Total amount($) / children approve: ', ['class' => 'form-control-label']) !!}
    </div>
    <div class="col-12 col-md-6">
        {!! Form::number('amount_per_user_approve',isset($setting['amount_per_user_approve'])? $setting['amount_per_user_approve'] : 0 ,['class' => 'form-control','min'=>'0','step'=>'0.01']) !!}
        {!! $errors->first('amount_per_user_approve', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@push('js')
    <script src="https://cdn.ckeditor.com/4.10.1/standard/ckeditor.js"></script>
    <script src="{{asset('backend/assets/js/date/bootstrap-datepicker.min.js')}}"></script>
    <script>
        jQuery(document).ready(function($) {
            $('.datetimepicker').datepicker({
                format:'dd-mm-yyyy'
            });

            $('input[name="logo_image"]').on('change',function(){
                $('.changeImage1').hide();
            });
        });

        $(function () {
            $('input[type=file]').change(function () {
                if($(this).data('ext')){
                    var exp = "(.*?)\.("+ $(this).data('ext') +")$";

                    var val = $(this).val().toLowerCase(),
                        regex = new RegExp(exp);

                    if (!(regex.test(val))) {
                        $(this).val('');
                        alert('Please select correct file format');
                    }
                }
            });
        });
    </script>
@endpush
