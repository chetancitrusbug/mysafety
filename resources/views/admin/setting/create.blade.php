@extends('layouts.admin')
@section('title',"Settings")
@section('content')
<div class="col-lg-6">
    <div class="card">
        <div class="card-header"><strong>Setting</strong><small> Create</small></div>

        {!! Form::open(['url' => '/admin/setting', 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
            <div class="card-body card-block">
                @include ('admin.setting.form')
            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Update
                </button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
