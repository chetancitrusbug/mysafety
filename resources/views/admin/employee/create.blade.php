@extends('layouts.admin')
@section('title',"Create Employee")
@section('content')
<div class="col-lg-6">
    <div class="card">
        <div class="card-header"><strong>Employee Create<strong></div>

        {!! Form::open(['url' => '/admin/employee', 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
            <div class="card-body card-block">
                
				
	
<div class="form-group {{ $errors->has('first_name') ? ' has-error' : ''}}">
    {!! Form::label('first_name', '* First Name: ', ['class' => 'form-control-label']) !!}
    {!! Form::text('first_name', null, ['class' => 'form-control input-sm']) !!}
    {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('last_name') ? ' has-error' : ''}}">
    {!! Form::label('last_name', '* Last Name: ', ['class' => 'form-control-label']) !!}
    {!! Form::text('last_name', null, ['class' => 'form-control input-sm']) !!}
    {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email', '* Email: ', ['class' => 'form-control-label']) !!}
    @if (isset($user))
        {!! Form::text('email', null, ['class' => 'form-control input-sm','disabled']) !!}
    @else
        {!! Form::text('email', null, ['class' => 'form-control input-sm']) !!}
    @endif
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
    {!! Form::label('status', 'Status: ', ['class' => 'control-label']) !!}
    {!! Form::checkbox('status',1, isset($user->status)?$user->status:1, ['class'=>['checkbox','status']]) !!}
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>




            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Create
                </button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
