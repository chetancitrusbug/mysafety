@extends('layouts.admin')
@section('title',"Employee Payout")
@section('content')
<div class="col-lg-6">
    <div class="card">
        <div class="card-header"><strong>Employee Payout</strong> </div>

		{!! Form::open(['url' => ['admin/employee/payment'],'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}

		
		{!! Form::hidden('user_id',$user->id, ['class' => 'form-control','id'=>'user_id']) !!}
            <div class="card-body card-block">
              <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td>Employee Name</td>
                        <td> {{ $user->full_name }} </td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td> {{ $user->email }} </td>
                    </tr>
                    <tr>
                        <td>Number of Children's Profile Activation (Not Paid)</td>
                        <td> {{ $user->employee->approved_count }} </td>
                    </tr>
                    
                </tbody>
            </table>  
		

<div class="form-group {{ $errors->has('last_name') ? ' has-error' : ''}}">
    {!! Form::label('total_child', '* Payment for total child profile activation: ', ['class' => 'form-control-label']) !!}
    {!! Form::number('total_child', null, ['class' => 'form-control input-sm','min'=>1,'max'=>$user->employee->approved_count]) !!}
    {!! $errors->first('total_child', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('last_name') ? ' has-error' : ''}}">
    {!! Form::label('amount', '* Total amount : ( $'.$peruser.' / Profile activation ) ', ['class' => 'form-control-label']) !!}
    {!! Form::number('amount', null, ['class' => 'form-control input-sm','min'=>0,'step'=>'0.01']) !!}
    {!! $errors->first('amount', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('comment') ? ' has-error' : ''}}">
    {!! Form::label('comment', 'Comment : ', ['class' => 'form-control-label']) !!}
    {!! Form::text('comment', null, ['class' => 'form-control input-sm']) !!}
    {!! $errors->first('comment', '<p class="help-block">:message</p>') !!}
</div>


   


            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Payment
                </button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection


@push('js')
    <script>
        var peruser = {{ $peruser }};
        $("#total_child").on("change",function() {
			var calc = peruser * $(this).val();
			$("#amount").val(calc);
		});

       
</script>
@endpush
