@extends('layouts.admin')
@section('title',"Employee")
@push('css')
    <style type="text/css">
        .btn-group{
            border: 1px solid #aaa;
        }
    </style>
@endpush

@section('content')
@include('modals.delete')


<div class="animated fadeIn">

    <div class="row">
		
			<div class="col-md-12">
            <div class="card">
                <div class="card-header">
                   
				   	<div class="col-md-6">
                    <a href="{{ url('/admin/employee/create') }}" title="Create">
                        <button class="btn btn-sm btn-space btn-success ">Add Employee</button>
                    </a>
					</div>
					
					<div class="col-md-6">
						<form method="GET" action="{{ \Request::url() }}"  id="filter_form">
							
							{!! Form::hidden('from_date',\Request::get('from_date'), ['class' => 'form-control','id'=>'from_date']) !!}
							{!! Form::hidden('to_date',\Request::get('to_date'), ['class' => 'form-control','id'=>'to_date']) !!}
                                                <div style="width: 100%">
                                                    <div id="reportrange"  style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%;height: 32px;">
                                                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                                        <span></span> <b class="caret"></b>
                                                    </div>

                                                </div>
						</form>
					 </div>
						
                </div>
            </div>
        </div>
		<div class="content mt-3">
			<div class="col-sm-6 col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-one">
                            <div class="stat-icon dib"><i class="ti-money text-success border-success"></i></div>
                            <div class="stat-content dib">
                                <div class="stat-text">Amount Paid / Total Child</div>
                                <div class="stat-digit">${{$unit_sum}} / {{$for_user_sum}} <span class="font-size-12 duration_filter" > </span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			
			<div class="col-sm-6 col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-one">
                            <div class="stat-icon dib"><i class="ti-money text-primary border-primary"></i></div>
                            <div class="stat-content dib">
                                <div class="stat-text">Payable Amount / Total child</div>
                                <div class="stat-digit">${{$approve_count*$peruser}} / {{$approve_count}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			
			<div class="col-sm-6 col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-one">
                            <div class="stat-icon dib"><i class="ti-user text-primary border-primary"></i></div>
                            <div class="stat-content dib">
                                <div class="stat-text">Child Processed</div>
                                <div class="stat-digit">{{$total_approved_child}} <span class="font-size-12 duration_filter" > </span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			</div>
			
		
			<div class="content mt-3">
			@foreach($users as $user)
			
			<div class="col-md-4">
                        <aside class="profile-nav alt">
                            <section class="card">
                                <div class="card-header user-header alt bg-dark">
                                    <div class="media">
                                       
                                        <div class="media-body">
                                            <h4 class="text-light display-6">{{ $user->full_name }}</h4>
                                            <p>#{{ $user->id }} {{ $user->email }}</p>
                                        </div>
                                    </div>
                                </div>


                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">
										@if($user->employee->approve_count_total >0 )
											<a title="Child List Processed By Employee" href="{{ url('/admin/childs-approveby/') }}/{{$user->id}}"> 
												<i class="fa fa-bars"></i> Total Child Processed 
												<span class="badge badge-success pull-right">{{$user->employee->approve_count_total}}</span>
											</a>
										@else
											<a href="#"> <i class="fa fa-bars"></i> Total Child Processed 
												<span class="badge badge-primary pull-right">{{$user->employee->approve_count_total}}</span>
											</a>	
										@endif
                                        
                                    </li>
                                    <li class="list-group-item">
                                        <a href="#"> <i class="fa fa-tasks"></i> Child Processed(Not Paid) <span class="badge badge-danger pull-right">{{ $user->employee->approved_count }}</span></a>
                                    </li>
                                    @if($to_date && $from_date)
									<li class="list-group-item">
                                        <a href="#"> <i class="fa fa-calendar"></i>  Processed <br/>({{$from_date->format('M d,Y') }} - {{$to_date->format('M d,Y')}}) 
										<span class="badge badge-warning  pull-right">{{ $user->badgeapprove->count() }}</span>
										</a>
                                    </li>
									
									@endif
                                    <li class="list-group-item">
										@if($user->employee->approved_count > 0)
										<a href="{{url('admin/employee/payment', $user->id)}}" title="Click to pay employee" class="btn btn-success btn-sm " >
											<i class="fa fa-money" aria-hidden="true"></i>
										</a>&nbsp;
										@else
										<a href="javacript:void(0)" title="No enough credit for payment" class="btn btn-success btn-sm " >
											<i class="fa fa-money" aria-hidden="true"></i>
										</a>&nbsp;	
										@endif
                                        <a href="{{ url('admin/employee') }}/{{ $user->id }}">
										<button class="btn btn-warning btn-sm" title="View">
											<i class="fa fa-eye"></i></button>
										</a>&nbsp;
										<a href="{{ url('admin/employee') }}/{{ $user->id }}/edit" class="btn btn-info btn-sm">
											<i class="fa fa-pencil"></i>
										</a>&nbsp;
										<a href="javascript:void(0);" data-url={{url('admin/employee')}} data-msg='user' class="btn btn-danger btn-sm del-item" data-toggle="modal" data-target="#danger" data-id="{{ $user->id }}" 
										data-url="#" data-msg="user" data-backdrop="static" data-keyboard="false">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</a>&nbsp;
										
													
                                    </li>
                                </ul>

                            </section>
                        </aside>
                    </div>
			@endforeach
			</div>
		
	
        
    </div>
</div><!-- .animated -->
@endsection

@push('js')
    <script>
        var url ="{{ url('/admin/employee/datatable') }}";
        var edit_url = "{{ url('/admin/employee') }}";
        var auth_check = "{{ Auth::check() }}";

        /*************************daterange selection*****************/

	var range_start = "";
    var range_end = "";

    var start = moment.utc('2017-01-01','YYYY-MM-DD');
    var end = moment();
	
	if($('#from_date').val() && $('#from_date').val() !="" && $('#to_date').val() && $('#to_date').val() !="" ){
		start = moment.utc($('#from_date').val(),'YYYY-MM-DD');
		end = moment.utc($('#to_date').val(),'YYYY-MM-DD');
	}
	$('.duration_filter').html("("+start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY')+")");
    
    function cb(start, end) {
        $('#reportrange span').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
        if(range_start==""){
            range_start = start.format('YYYY-MM-DD');
            range_end = end.format('YYYY-MM-DD');
        }else{
            range_start = start.format('YYYY-MM-DD');
            range_end = end.format('YYYY-MM-DD');
			
			$('#to_date').val(range_end);
			$('#from_date').val(range_start);
			
			$('#filter_form').submit();
			
			

            //datatable.fnDraw();
        }
		$('#to_date').val(range_end);
		$('#from_date').val(range_start);

    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            "All":[moment.utc('2017-01-01','YYYY-MM-DD'),moment()],
            "Today": [moment(), moment()],
            "This Week": [moment().startOf('week'), moment().endOf('week')],
            "Last Week": [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
            "This Month": [moment().startOf('month'), moment().endOf('month')],
            "Last Month": [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            
        }
    }, cb);

       cb(start, end);


          $(document).on('click', '.del-item', function (e) {

            var msg = $(this).data('msg');

            jQuery('.modal-body').html("Are you sure you want to delete " + msg +" ?");

            $('#danger').modal('show');
        });
</script>
@endpush