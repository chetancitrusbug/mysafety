@extends('layouts.admin')
@section('title',"Show Employee")

@section('content')
<div class="col-lg-6">
    <div class="card">
        <div class="card-header"><strong>Employee Detail</strong>
            <a href="{{ url('/admin/employee') }}" title="Parent List">
                <button class="btn btn-sm btn-space btn-warning pull-right">Back To Employee</button>
            </a>
        </div>
        <div class="card-body card-block">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td>First Name</td>
                        <td> {{ $user->first_name }} </td>
                    </tr>
                    <tr>
                        <td>Last Name</td>
                        <td> {{ $user->last_name }} </td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td> {{ $user->email }} </td>
                    </tr>
                    <tr>
                        <td>Phone No</td>
                        <td> {{ $user->phone }} </td>
                    </tr>
					
					 <tr>
                        <td>Total Email Send (to set Password)</td>
                        <td> {{ $user->employee->try_count }} </td>
                    </tr>
					 <tr>
                        <td>Number of Child's Profile Activation (All)
						@if($user->employee->approve_count_total >0 )
							<a href="{{ url('/admin/childs-approveby/') }}/{{$user->id}}" class="pull-right" title="Child List Approve By Employee">
								view
							</a>
							@endif
						</td>
                        <td>
							{{ $user->employee->approve_count_total }}

						</td>
                    </tr>
                    <tr>
                        <td>Number of Child's Profile Activation (Not Paid)
						</td>
                        <td>
							{{ $user->employee->approved_count }}
							
							@if($user->employee->approved_count >0 )
							<a href="{{url('admin/employee/payment', $user->id)}}" title="Click to pay employee">
								<button class="btn btn-sm btn-space btn-info pull-right">Pay Now !</button>
							</a>
							@endif

						</td>
                    </tr>
					<tr>
                        <td>Status</td>
						<td>
						@if($user->status)
                         Active
						@else
						Inactive	
						@endif
						</td>
                    </tr>
					 <tr>
                        <td>Is Setup (Password)</td>
                        <td> {{ $user->employee->is_setup }} 
							@if($user->employee->is_setup == "no")
							<a href="{{url('admin/employee/activation-resend', $user->id)}}" title="Resend Password and activation email">
								<button class="btn btn-sm btn-space btn-info pull-right">Resend Email</button>
							</a>
							@endif
						</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="col-lg-6">
    <div class="card">
        <div class="card-header"><div class="col-sm-12"><strong>Child's Profile And Badge Approve</strong></div>
           
        </div>
        <div class="card-body card-block">
            <table class="table table-bordered">
                <tbody>
					<tr>
                        <td> 
							<form method="GET" action="{{ \Request::url() }}" >
							{!! Form::hidden('user_id',$user->id, ['class' => 'form-control','id'=>'user_id']) !!}
							{!! Form::hidden('from_date',\Request::get('from_date'), ['class' => 'form-control','id'=>'from_date']) !!}
							{!! Form::hidden('to_date',\Request::get('to_date'), ['class' => 'form-control','id'=>'to_date']) !!}
                                                <div style="width: 100%">
                                                    <div id="reportrange"  style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%;height: 32px;">
                                                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                                        <span></span> <b class="caret"></b>
                                                    </div>

                                                </div>

                                        <button type="submit"  class="btn btn-sm btn-space btn-info pull-right">View Approve Count</button>
										</form>
						</td>
                        
                    </tr>
					
					<tr>
                        <td>
							Approved Count : {{ $badgeapprove->count() }}
							
							@if($badgeapprove->count() >0 )
								
							@php( $q='' )
							@if(\Request::has('from_date') && \Request::has('to_date'))
								@php( $q="?from_date=".\Request::get('from_date')."&to_date=".\Request::get('to_date') )
							@endif
							<a href="{{ url('/admin/childs-approveby/') }}/{{$user->id}}{{ $q }}" class="pull-right" title="Child List Approve By Employee">
								view
							</a>
							@endif

						</td>
                    </tr>
                    
                    
					
					
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="col-lg-6"></div>

<div class="col-lg-12">
    <div class="card">
        <div class="card-header"><strong>Employee Payouts</strong>
            
        </div>
        <div class="card-body card-block">
            <table id="datatable" class="table table-striped table-hover table-fw-widget">
                        <thead>
                            <tr>
                                <th>#ID</th>
                                <th>Amount</th>
                                <th>For Child</th>
                                <th>Note </th>
                                <th>Created</th>
								<th>Created By</th>
                            </tr>
                        </thead>
                    </table>
        </div>
    </div>
	
</div>

@endsection


@push('js')
    <script>
        var url ="{{ url('/admin/employee-payment/datatable/'.$user->id) }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#datatable').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"DESC"]],
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
               // statusChange();
            },
            columns: [
                { data: 'id',name : 'id',"searchable": false, "orderable": true},
                { data: 'unit',name : 'unit',"searchable": true, "orderable": true},
                { data: 'for_user',name : 'for_user',"searchable": true, "orderable": true},
                { data: 'comment',name : 'comment',"searchable": true, "orderable": true},
                { data: 'payment_created',name : 'payment_created',"searchable": true, "orderable": true},
                {
                "data": null,
                "name": 'created_by',
                "searchable": false,
                "render": function (o) {
                    var creatorname = "";
                    if(o.creator){
                        creatorname = o.creator.full_name;
                    }
                    return creatorname;
                }
            },
                
            ]
        });

          /*************************daterange selection*****************/

	var range_start = "";
    var range_end = "";

    var start = moment.utc('2017-01-01','YYYY-MM-DD');
    var end = moment();
	
	if($('#from_date').val() && $('#from_date').val() !="" && $('#to_date').val() && $('#to_date').val() !="" ){
		start = moment.utc($('#from_date').val(),'YYYY-MM-DD');
		end = moment.utc($('#to_date').val(),'YYYY-MM-DD');
	}
    
    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        if(range_start==""){
            range_start = start.format('YYYY-MM-DD');
            range_end = end.format('YYYY-MM-DD');
        }else{
            range_start = start.format('YYYY-MM-DD');
            range_end = end.format('YYYY-MM-DD');
			
			
			

            //datatable.fnDraw();
        }
		$('#to_date').val(range_end);
		$('#from_date').val(range_start);

    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            "All":[moment.utc('2017-01-01','YYYY-MM-DD'),moment()],
            "Today": [moment(), moment()],
            "This Week": [moment().startOf('week'), moment().endOf('week')],
            "Last Week": [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
            "This Month": [moment().startOf('month'), moment().endOf('month')],
            "Last Month": [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            
        }
    }, cb);

     cb(start, end);
	 
	 
	$('.filter').change(function() {
        datatable.fnDraw();
    });
</script>
@endpush
