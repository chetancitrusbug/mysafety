@extends('layouts.admin')
@section('title',"Edit Employee")
@section('content')
<div class="col-lg-6">
    <div class="card">
        <div class="card-header"><strong>Employee Edit</strong> </div>

        {!! Form::model($user, ['method' => 'PATCH','url' => ['/admin/employee', $user->id],'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}

            <div class="card-body card-block">
                
				
				<div class="form-group {{ $errors->has('first_name') ? ' has-error' : ''}}">
    {!! Form::label('first_name', '* First Name: ', ['class' => 'form-control-label']) !!}
    {!! Form::text('first_name', null, ['class' => 'form-control input-sm']) !!}
    {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('last_name') ? ' has-error' : ''}}">
    {!! Form::label('last_name', '* Last Name: ', ['class' => 'form-control-label']) !!}
    {!! Form::text('last_name', null, ['class' => 'form-control input-sm']) !!}
    {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email', 'Email: ', ['class' => 'form-control-label']) !!}
    {!! Form::text('email', $user->email, ['class' => 'form-control input-sm','disabled']) !!}
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>


    <div class="form-group {{ $errors->has('password') ? ' has-error' : ''}}">
        {!! Form::label('password', 'Password: ', ['class' => 'form-control-label']) !!}
        {!! Form::password('password', ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="form-group {{ $errors->has('confirm_password') ? ' has-error' : ''}}">
        {!! Form::label('confirm_password', 'Confirm Password: ', ['class' => 'form-control-label']) !!}
        {!! Form::password('confirm_password', ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('confirm_password', '<p class="help-block">:message</p>') !!}
    </div>


<div class="form-group {{ $errors->has('phone') ? ' has-error' : ''}}">
    {!! Form::label('phone', 'Phone: ', ['class' => 'form-control-label']) !!}
    {!! Form::text('phone', null, ['class' => 'form-control input-sm']) !!}
    {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
</div>

@if (isset($user) && $user->roles[0]->name != 'Admin')
    <div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
        {!! Form::label('status', 'Status: ', ['class' => 'control-label']) !!}
        {!! Form::checkbox('status',1, isset($user->status)?$user->status:1, ['class'=>['checkbox','status']]) !!}
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
@endif


            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Update
                </button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
