@extends('layouts.admin')
@section('title',"Show Employee")

@section('content')
<div class="col-lg-6">
    <div class="card">
        <div class="card-header"><strong>Employee</strong><small> Detail</small>
		@if(\Auth::user()->hasRole('AU'))
            <a href="{{ url('/admin/employee') }}" title="">
                <button class="btn btn-sm btn-space btn-warning pull-right">Back To Employee</button>
            </a>
		@endif	
		@if(\Auth::user()->hasRole('EU'))
            <a href="{{ url('/admin/edit_profile') }}" title="Edit Profile">
                <button class="btn btn-sm btn-space btn-warning pull-right">Edit Profile</button>
            </a>
		@endif	
        </div>
        <div class="card-body card-block">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td>First Name</td>
                        <td> {{ $user->first_name }} </td>
                    </tr>
                    <tr>
                        <td>Last Name</td>
                        <td> {{ $user->last_name }} </td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td> {{ $user->email }} </td>
                    </tr>
                    <tr>
                        <td>Phone No</td>
                        <td> {{ $user->phone }} </td>
                    </tr>
					
					
					 <tr>
                        <td>Number of Children's Profile Activation (Not Paid)</td>
                        <td>
							{{ $user->employee->approved_count }}
							
							@if($user->employee->approved_count >0 && \Auth::user()->hasRole('AU'))
							<a href="{{url('admin/employee/payment', $user->id)}}" title="Resend Password and activation email">
								<button class="btn btn-sm btn-space btn-info pull-right">Pay Now !</button>
							</a>
							@else
								<a href="{{ url('/admin/childs-approveby/') }}" title="List of child approve by me">
								<button class="btn btn-sm btn-space btn-info pull-right">Child List Approved By Me</button>
							</a>
							@endif

						</td>
                    </tr>
					<tr>
                        <td>Status</td>
						<td>
						@if($user->status)
                         Active
						@else
						Inactive	
						@endif
						</td>
                    </tr>
					
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="col-lg-6"></div>

<div class="col-lg-12">
    <div class="card">
        <div class="card-header"><strong>Employee Payouts</strong>
            
        </div>
        <div class="card-body card-block">
            <table id="datatable" class="table table-striped table-hover table-fw-widget">
                        <thead>
                            <tr>
                                <th>#ID</th>
                                <th>Amount</th>
                                <th>For Child</th>
                                <th>Note </th>
                                <th>Created</th>
								<th>Created By</th>
                            </tr>
                        </thead>
                    </table>
        </div>
    </div>
	
</div>

@endsection


@push('js')
    <script>
        var url ="{{ url('/admin/employee-payment/datatable/'.$user->id) }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#datatable').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"DESC"]],
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
               // statusChange();
            },
            columns: [
                { data: 'id',name : 'id',"searchable": false, "orderable": true},
                { data: 'unit',name : 'unit',"searchable": true, "orderable": true},
                { data: 'for_user',name : 'for_user',"searchable": true, "orderable": true},
                { data: 'comment',name : 'comment',"searchable": true, "orderable": true},
                { data: 'payment_created',name : 'payment_created',"searchable": true, "orderable": true},
                {
                "data": null,
                "name": 'created_by',
                "searchable": false,
                "render": function (o) {
                    var creatorname = "";
                    if(o.creator){
                        creatorname = o.creator.full_name;
                    }
                    return creatorname;
                }
            },
                
            ]
        });

       
</script>
@endpush
