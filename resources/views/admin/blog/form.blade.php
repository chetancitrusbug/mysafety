@push('css')
    {{-- <link id="bsdp-css" href="bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet"> --}}
    <link rel="stylesheet" href="{{asset('backend/assets/css/date/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/assets/css/date/bootstrap-datepicker.standalone.min.css')}}">

    <style type="text/css">
        .btn-group{
            border: 1px solid #aaa;
        }
        .logo_setting{
            position: relative;
        }
        .logo-image{
            position: absolute;
            top: 0px;
            right: 16px;
        }
        .changeImage1{
            height: 40px;
        }
    </style>
@endpush
<div class="form-group {{ $errors->has('type') ? ' has-error' : ''}}">
    {!! Form::label('type', '*Type: ', ['class' => 'form-control-label']) !!}
    {!! Form::select('type', [''=>'select type']+config('admin.page_type'), null,['class' => 'form-control']) !!}
    {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('title') ? ' has-error' : ''}}">
    {!! Form::label('title', '* Title: ', ['class' => 'form-control-label']) !!}
    {!! Form::text('title', null, ['class' => 'form-control input-sm']) !!}
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('meta_keywords') ? ' has-error' : ''}}">
    {!! Form::label('meta_keywords', 'SEO Keywords: ', ['class' => 'form-control-label']) !!} {!! Form::text('meta_keywords', null, ['class'
    => 'form-control input-sm']) !!} {!! $errors->first('meta_keywords', '
    <p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('sort_desc') ? ' has-error' : ''}}">
    {!! Form::label('sort_desc', '* Sort Desc: ', ['class' => 'form-control-label']) !!} {!! Form::text('sort_desc', null, ['class'
    => 'form-control input-sm']) !!} {!! $errors->first('sort_desc', '
    <p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('detail') ? ' has-error' : ''}}">
    {!! Form::label('detail', '* Detail: ', ['class' => 'form-control-label']) !!}
    {!! Form::textarea('detail', old('detail',null) , ['class' => 'form-control','id'=>'editor1']) !!}
    {!! $errors->first('detail', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('post_date') ? ' has-error' : ''}} hide_date">
    {!! Form::label('post_date', '* Post Date: ', ['class' => 'form-control-label']) !!}
    {!! Form::text('post_date', null, ['class' => 'form-control input-sm datetimepicker']) !!}
    {!! $errors->first('post_date', '<p class="help-block">:message</p>') !!}
</div>

<div class="row form-group {{ $errors->has('image') ? ' has-error' : ''}} hide_image">
    {!! Form::hidden('old_image', isset($blog->image)?$blog->image:'') !!}
    <div class="col col-md-3">
        {!! Form::label('image', '* Image (jpg, jpeg, png format only): ', ['class' => 'form-control-label']) !!}
    </div>
    <div class="col-12 col-md-9">
        <input type="file" id="file-input" name="image" class="form-control-file" data-ext="jpg|jpeg|png">
        @if(isset($blog))
            <div class="logo-image"><img src="{{ asset('uploads/blog').'/'.$blog->image }}" class="changeImage1"></div>
        @endif
    </div>
    {!! $errors->first('image', '<p class="help-block" style="margin-left:15px;">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
    {!! Form::label('status', 'Status: ', ['class' => 'control-label']) !!}
    {!! Form::checkbox('status',1, isset($blog->status)?$blog->status:1, ['class'=>['checkbox','status']]) !!}
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>

@push('js')
    <script src="https://cdn.ckeditor.com/4.10.1/standard/ckeditor.js"></script>
    <script src="{{asset('backend/assets/js/date/bootstrap-datepicker.min.js')}}"></script>
    <script>
        CKEDITOR.replace( 'detail' );

        var format = "{{ strtolower(config('admin.date_format')[session('setting.date_format')]) }}";

        jQuery(document).ready(function($) {
            $('.datetimepicker').datepicker({
                format:format
            });
            $('input[name="image"]').on('change',function(){
                $('.changeImage1').hide();
            });

            changeField($('select[name="type"]').val());
        });

        $(function () {
            $('input[type=file]').change(function () {
                if($(this).data('ext')){
                    var exp = "(.*?)\.("+ $(this).data('ext') +")$";

                    var val = $(this).val().toLowerCase(),
                        regex = new RegExp(exp);

                    if (!(regex.test(val))) {
                        $(this).val('');
                        alert('Please select correct file format');
                    }
                }
            });
        });

        $('select[name="type"]').on('change',function(e){
            changeField($(this).val());
        });

        function changeField(type){
            if(type == 'page'){
                $('input[name="post_date"], .hide_date').hide();
                $('input[name="image"], .hide_image').hide();
            }else{
                $('input[name="post_date"], .hide_date').show();
                $('input[name="image"], .hide_image').show();
            }
        }
    </script>

@endpush
