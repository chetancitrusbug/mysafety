@extends('layouts.admin')
@section('title',"Blogs Or Static Pages")
@push('css')
    <style type="text/css">
        .btn-group{
            border: 1px solid #aaa;
        }
    </style>
@endpush

@section('content')
@include('modals.delete')


<div class="animated fadeIn">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Blogs Or Static Pages</strong>
                   
					
					<div class="form-group pull-right margin-left-10">
                        <select class="form-control input-sm filter" id="filter_refrence" name="refrence">
							<option value="">All</option>
							<option value="blog">Blogs</option>
							<option value="page">Pages</option>
                        </select>
                     </div>
					 
					  <a href="{{ url('/admin/blog/create') }}" title="Create">
                        <button class="btn btn-sm btn-space btn-success pull-right">Create</button>
                    </a>
                </div>
                <div class="card-body">
                    <table id="blog-table" class="table table-striped table-hover table-fw-widget">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Post Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div><!-- .animated -->
@endsection

@push('js')
    <script>
        var url ="{{ url('/admin/blog-data') }}";
        var edit_url = "{{ url('/admin/blog') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#blog-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            ajax: {
                url:url,
                type:"get",
				data: function (d) {
					d.dtype = $('#filter_refrence').val();
				}
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'title',name : 'title',"searchable": true, "orderable": true},
                { data: 'post_date',name : 'post_date',"searchable": true, "orderable": true},
                {
                    "data": null,
                    "name" : 'status',
                    "searchable": false,
                    "orderable": true,
                    "render": function (o) {
                        if(o.status == 1){
                            return "<input type='checkbox' class='status status-change' data-group-cls='btn-group-sm' checked data-table='blog' data-status="+o.status+" onchange='statusChange()'  data-url={{url('admin/change-status')}} value="+o.id+" data-id="+o.id+">&nbsp;";
                        }
                        return "<input type='checkbox' class='status status-change' data-group-cls='btn-group-sm' data-url={{url('admin/change-status')}} data-table='blog' data-status="+o.status+" value="+o.id+" data-id="+o.id+">&nbsp;";
                    }
                },
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e=""; var v=""; var d= "";
                        v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";

                        e = "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" class='btn btn-info btn-sm'><i class='fa fa-pencil'></i></a>&nbsp;";

                        d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-item' data-toggle='modal' data-target='#danger' data-id="+o.id+" data-url={{url('admin/blog')}} data-msg='blog' data-backdrop='static' data-keyboard='false'><i class='fa fa-trash' aria-hidden='true'></i></a>&nbsp;";
                        return e+d;
                    }

                }
            ]
        });


		$('.filter').change(function() {
			datatable.draw();
		});
        function statusChange() {
            $('.status').checkboxpicker({
                offLabel: 'Inactive',
                onLabel: 'Active',
            });
            $('.status1').checkboxpicker();

            $('.status_invoice').checkboxpicker({
                offLabel: 'Inprogress',
                onLabel: 'Confirm',
            });
        }
</script>
@endpush