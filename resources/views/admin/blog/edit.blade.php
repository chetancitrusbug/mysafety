@extends('layouts.admin')
@section('title',"Edit Blog")
@section('content')
<div class="col-lg-6">
    <div class="card">
        <div class="card-header"><strong>Blog</strong><small>Edit</small></div>

        {!! Form::model($blog, ['method' => 'PATCH','url' => ['/admin/blog', $blog->id],'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}

            <div class="card-body card-block">
                @include ('admin.blog.form')
            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Update
                </button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
