@extends('layouts.admin')
@section('title',"SEO Tools")
@section('content')
<div class="col-lg-6">
    <div class="card">
        <div class="card-header"><strong>SEO Data</strong></div>

        {!! Form::open(['url' => '/admin/seo-data', 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
            <div class="card-body card-block">
                @include ('admin.seo-data.form')
            </div>

          
        {!! Form::close() !!}
    </div>
</div>
@endsection
