@extends('layouts.admin')
@section('title',"SEO Tools")
@push('css')
    <style type="text/css">
        .btn-group{
            border: 1px solid #aaa;
        }
    </style>
@endpush

@section('content')
@include('modals.delete')

<div class="animated fadeIn">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">SEO Data(title,discription) for web page</strong>
                    
                </div>
                <div class="card-body">
                    <table id="blog-table" class="table table-striped table-hover table-fw-widget">
                        <thead>
                            <tr>
                                <th>Page</th>
                                <th>Title</th>
                                <th>Keywords</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                        </thead>
						<tbody>
						
						<?php
						 
						 $default = \App\Seocontent::where("page_name","default")->first();
							
						 ?>
						 @foreach(\config('constant.meta_key_page') as $k => $val)
						 
						 <?php
						 
						 $item = \App\Seocontent::where("page_name",$k)->first();
							if(!$item){
								$item = $default;
							}
						 ?>
						<tr>
						<td><b>{{$val}}</b></td>
						<td>@if($item) {{$item->title}} @endif</td>
						<td>@if($item) {{$item->keywords}} @endif</td>
						<td>@if($item) {{$item->description}} @endif</td>
						<td>
							<a href="{{ url('/admin/seo-data/'.$k) }}" ><button class='btn btn-warning btn-sm' title='View' ><i class='fa fa-eye' ></i></button></a>&nbsp;
							<a href="{{ url('/admin/seo-data/'.$k.'/edit') }}" ><button class='btn btn-info btn-sm' title='Edit' ><i class='fa fa-pencil' ></i></button></a>&nbsp;
						</td>
						</tr>
						@endforeach
						</tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div><!-- .animated -->
@endsection

@push('js')
    <script>
       

        
</script>
@endpush