@extends('layouts.admin')
@section('title',"SEO Tools")
@section('content')
<div class="col-lg-6">
    <div class="card">
        <div class="card-header"><strong>{{ \config('constant.meta_key_page.'.$page_id) }} page SEO data </strong></div>
		@if($item && isset($item) && isset($item->id))
        {!! Form::model($item, ['method' => 'PATCH','url' => ['/admin/seo-data', $page_id],'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
		@else
			{!! Form::open(['url' => '/admin/seo-data', 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
		@endif
            <div class="card-body card-block">
                @include ('admin.seo-data.form')
            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Update
                </button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
