@push('css')
    <style type="text/css">
        .btn-group{
            border: 1px solid #aaa;
        }
    </style>
@endpush


{!! Form::hidden('page_name', $page_id , ['class' => 'form-control','id'=>'page_name']) !!}

<div class="form-group{{ $errors->has('title') ? ' has-error' : ''}}">
    {!! Form::label('title', '*Meta-title: ', ['class' => 'form-control-label']) !!}
    {!! Form::text('title', null , ['class' => 'form-control','id'=>'title']) !!}
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group{{ $errors->has('description') ? ' has-error' : ''}}">
    {!! Form::label('description', '* Meta-Description: ', ['class' => 'form-control-label']) !!}
    {!! Form::textarea('description', null , ['rows'=>2,'class' => 'form-control','id'=>'description']) !!}
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group{{ $errors->has('keywords') ? ' has-error' : ''}}">
    {!! Form::label('keywords', '* Meta-keywords ( comma separated ): ', ['class' => 'form-control-label']) !!}
    {!! Form::textarea('keywords', null , ['rows'=>2,'class' => 'form-control','id'=>'keywords']) !!}
    {!! $errors->first('keywords', '<p class="help-block">:message</p>') !!}
</div>