@extends('layouts.admin')
@section('title',"SEO Tools")
@section('content')

<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
		
			SEO Data 
			
			
            <a href="{{ url('/admin/seo-data') }}" title="Child List">
                <button class="btn btn-sm btn-space btn-warning pull-right">Back To List</button>
            </a>
        </div>
        <div class="card-body card-block">
            <table class="table table-bordered">
                <tbody>
					<tr>
                        <th> Page </th>
                        <td> {{ \config('constant.meta_key_page.'.$page_id) }}  </td>
                    </tr>
					@if($item)
                    <tr>
                        <th>Title</th>
                        <td> {{ $item->title }} </td>
                    </tr>
                    <tr>
                        <th>Keywords</th>
                        <td> {{ $item->keywords }} </td>
                    </tr>
					<tr>
                        <th>Description</th>
                        <td> {{ $item->description }} </td>
                    </tr>
					
					
                   @endif
                </tbody>
            </table>
        </div>
    </div>
</div>


@endsection
