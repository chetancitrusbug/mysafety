@extends('layouts.admin')
@section('title',"Show Order")
@section('content')

<div class="col-md-12">
	        <div class="card">
	            
	           <div class="card-body p-3">
            <div id="invoice-template" class="card-block">
                <!-- Invoice Company Details -->
                <div id="invoice-company-details" class="row">
                    <div class="col-6 text-left">
                        <ul class="px-0 list-unstyled">
                            <li class="text-bold-800">Child : <strong> {{$order->child->full_name}}</strong></li>
                            <li>Subscription/Plan : <strong>{{ $order->plan->title }} | @if($order->plan->type != "free") ${{ $order->plan->actual_amount }}-@endif {{ strtoupper($order->plan->type) }} </strong> </li>
                            <li>Invoice Date : <strong>{{ \Carbon\Carbon::createFromFormat("Y-m-d H:i:s",$order->created_at)->format("jS M, Y g:i a") }}</strong> </li>
							<li>Duration : <strong> {{ $order->start_date_formated }} To {{ $order->expiry_date_formated }} </strong></li>
                            <li>Payment Type : <strong> @if($order->plan->type == "free") FREE @else STRIPE SUBSCRIPTION @endif</strong></li>
							<li>Activation Status : <strong> {{ strtoupper($order->trans_status) }} </strong></li>
							<li>Registration Reference From : <strong> 
								@if($order->child->regi_refrence && $order->child->regi_refrence =="site")
									Website
								@else
									Clickfunnel
								@endif
							</strong></li>
							
                        </ul>
                    </div>
                    <div class="col-6 text-right">
                        <h3>Invoice : # {{$order->order_no}}</h3>
                        <p class="pb-3">
                            <strong>{{ strtoupper($order->trans_status) }}</strong>
                        </p>
                        
                    </div>
                </div>
                 
                <div id="invoice-customer-details" class="row pt-2">
                    <div class="col-sm-12 text-left">
                        <p class="text-muted">To</p>
                    </div>
                    <div class="col-6 text-left">
					@if($order->parent)
                        <ul class="px-0 list-unstyled">
                            <li class="text-bold-800">{{$order->parent->full_name}}</li>
                            <li>{{$order->parent->email}}</li>
                            <li>{{ $order->parent->country_code }} {{$order->parent->phone}}</li>
                        </ul>
					@endif	
                    </div>
                    <div class="col-6 text-right">
					{{-- <strong>Package</strong> --}}
                    </div>
                </div>
                
              
                <div id="invoice-items-details" class="pt-2">
					@if($order->one_time_charge_id && $order->one_time_charge_id != "")
                    <div class="row">
                        <div class="table-responsive col-sm-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Product Name</th>
                                        <th class="text-right">Qty</th>
                                        <th class="text-right">Price</th>
                                        <th class="text-right"> Price total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   
                                        <tr>
                                          <td> One Time Fee / Registration Fee</td>
                                          <td class="text-right">
                                          1
                                          </td>
                                          <td class="text-right">
                                          $ {{ \config('admin.one_time_fee')}}
                                          </td>
                                          <td class="text-right">
                                          $ {{ \config('admin.one_time_fee')}}
                                          </td>
                                        </tr>
                                        
                                        
                                        
                                        <tr>
                                            <td colspan="3"></td>
                                            <td class="text-right"> Discount (%): 0</td>
                                        </tr>
                                        
                                        <tr>
                                            <td colspan="3"></td>
                                            <td class="text-right"> Paid Amount :  ${{$order->one_time_fee}}</td>
                                        </tr>
										@if($order->one_time_charge_id && $order->one_time_charge_id != "")
										<tr>
                                            <td colspan="3"></td>
                                            <td class="text-right"> Transaction ID :  {{$order->one_time_charge_id}}</td>
                                        </tr>
										@endif
                                </tbody>
                            </table>
                        </div>
                    </div>
					<hr/><hr/>
					@endif
					@if($order->billing && $order->billing->count() > 0)
					<div class="row">
                        <div class="table-responsive col-sm-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th colspan="4" class="text-center" >Billing Detail</th>
                                    </tr>
									<tr>
                                        <th class="text-center" >No</th>
										<th class="text-center" >Duration</th>
										<th class="text-center" >Transaction Id</th>
										<th class="text-center" >Amount</th>
                                    </tr>

                                </thead>
                                <tbody>
									@foreach($order->billing as $k => $bill)
                                   
                                        <tr>
                                          <td class="text-center" > {{$k}}</td>
                                          <td class="text-center" > {{$bill->start_date}} To {{$bill->end_date}}</td>
										  <td class="text-center" > {{$bill->charge_id}} </td>
										  <td class="text-center" > $ {{$bill->amount_paid}}</td>
                                        </tr>
                                        
                                    @endforeach    
                                       
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @endif
                    
                </div>
				
				
               
            </div>
        </div>
	        </div>
	    </div>
@endsection
