@extends('layouts.admin')
@section('title',"Edit Profile")
@section('content')
<div class="col-lg-6">
    <div class="card">
        <div class="card-header"><strong>User Edit<strong></div>

         <form method="POST" action="{{ url('/admin/update_profile') }}" class="form-horizontal" enctype="multipart/form-data">
            
            <div class="card-body card-block">
            {{ csrf_field() }}
                <div class="form-group {{ $errors->has('first_name') ? ' has-error' : ''}}">
                        {!! Form::label('first_name', '* First Name: ', ['class' => 'form-control-label']) !!}
                        {!! Form::text('first_name', $user->first_name, ['class' => 'form-control input-sm']) !!}
                        {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
                </div>

                <div class="form-group {{ $errors->has('last_name') ? ' has-error' : ''}}">
                    {!! Form::label('last_name', '* Last Name: ', ['class' => 'form-control-label']) !!}
                    {!! Form::text('last_name', $user->last_name, ['class' => 'form-control input-sm']) !!}
                    {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
                </div>

                <div class="form-group {{ $errors->has('email') ? ' has-error' : ''}}">
                    {!! Form::label('email', '* Email: ', ['class' => 'form-control-label']) !!}
                    @if (isset($user))
                        {!! Form::text('email', $user->email, ['class' => 'form-control input-sm','disabled']) !!}
                    @else
                        {!! Form::text('email', null, ['class' => 'form-control input-sm']) !!}
                    @endif
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                </div>

                <div class="form-group {{ $errors->has('phone') ? ' has-error' : ''}}">
                    {!! Form::label('phone', '* Phone: ', ['class' => 'form-control-label']) !!}
                    {!! Form::text('phone', $user->phone, ['class' => 'form-control input-sm']) !!}
                    {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
                </div>

              
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Update
                </button>
            </div>
        </form>
    </div>
</div>
@endsection