<!doctype html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title') {{ config('app.name') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta property="og:image" content="#" />

    <!-- Favicon CSS -->
    <link rel="shortcut icon" href="{{asset('backend/assets/img/logo-fav.png')}}">
    <!-- Fonts CSS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700" rel="stylesheet">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{{asset('static-app/assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('static-app/assets/css/custom.css')}}">
	<link rel="stylesheet" href="{{asset('static-app/assets/css/bootstrap.min.css')}}">

</head>
<body>
    <div class="web-page-div">

        <!-- =========== Reviews ============ -->
        <section class="web-page-section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title">
                            <h2>@yield('title')</h2>
                        </div>
                    </div>
                    <!-- section title end -->
                </div>
                <!-- section title row end -->



                <div class="row">
                    <div class="col-12 col-lg-12 mx-auto">

                        <div class="content-p-div clearfix">
                            @yield('content')
                        </div>


                    </div>
                </div>






            </div>
        </section>
        <!-- =========== Reviews ============ -->

    </div>

    <script src="{{asset('static-app/assets/js/script.js')}}"></script>

</body>
</html>
