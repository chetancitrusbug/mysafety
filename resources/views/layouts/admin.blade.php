<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="assets/img/logo-fav.png">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title') {{ config('app.name') }}</title>

        @include('include.admin.css')

        @stack('css')
    </head>

    <body>
        <div id="loading">
            <img id="loading-image" src="{{asset('backend/loading.gif')}}" alt="Loading..." />
        </div>
        @include('include.admin.sidebar')

        <div id="right-panel" class="right-panel">

            <!-- Header-->
            @include('include.admin.top-nav-bar')

            @include('include.admin.breadcrumb')

            @include('include.admin.alerts')

            <div class="content mt-3">

                @yield('content')

            </div>
        </div>

        <form id="delete-form" action="" method="POST" style="display: none;">
            {{ method_field('delete') }}
            {{ csrf_field() }}
        </form>

        @include('include.admin.js')

        <script type="text/javascript">

        $(document).ready(function(){
            //initialize the javascript
            // App.init();
            // App.dashboard();
          //  setTimeout(function(){
         //       jQuery('.alert-dismissible').hide();
          //  }, 300000);

        });
            $("#loading").hide();


        </script>
        @stack('js')
    </body>
</html>