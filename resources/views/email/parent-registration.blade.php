@extends('email.mailtemplate.cit')

@section('body')
    @if(isset($user))
        <h2 class="title">Hi {{$user->full_name}}</h2>
    @endif

<b>Welcome to MySafetyNet and MySafetyNet mobile App.</b>
<br/><br/>
<p>
MySafetyNet is in the business of protecting children from online predators. By our using dual-layered protection, this tool, which you can use on all online devices prevents your children from interacting with predators.
</p>

<p>
We only allow registered minor children on our website and we validate them through:
</p>

<p class="pr1">
Your child’s student ID,<br/>
And through your state's Child ID services aka State ID<br/>
After validating a profile, we give it a “My Safety Net” badge and let it be a live profile on our page, only viewable by other registered minor children.<br/>

</p>


    
@endsection