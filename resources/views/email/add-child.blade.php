@extends('email.mailtemplate.cit')

@section('body')
    @if(isset($user))
        <h2 class="title">Hi {{$user->full_name}}</h2>
    @endif

    <p>
Welcome to MySafetyNet:
We have received your payment. Please review this email and complete the validation process. We are pleased to be able to assist you in providing your child with today’s most integrated networking and validation tool for minor children’s OMO (online meets offline) safety. .
</p>

<p> This tool is geared to provide minor children with the most reduced possible threat assessment level from online predators. Our website and phone apps provide you and your child with the most safe environment on the Internet to date.
</p>

<br/>
 <b class="bold-1">Complete the validation process:</b>
<p>You are now ready to complete the validation process. MySafetyNet will need the following validation documents from you, uploaded in your child registration profile to make your child’s profile go live;
</p>

<ul>
<li>Digital photocopy of Completed “Parental PermissionForm” (attachment in this email)</li>
<li>Digital photocopy of Your “Parent” government ID (Acceptable forms of ID include State ID Card, State Driver’s License, and Passport)</li>
<li>Digital photocopy of Your minor child’s ID (Acceptable forms of ID include; School Student ID and State ID Card)</li>
<li>A digital photocopy of you and your child holding this Parental Permission Form (to avoid identity theft issues) </li>

</ul>

<p>In order to complete the registration process, once all the above files are prepared, please log into your  adult profile and click orders click on your child , then click upload files, then  upload them to the profile</p>
	
		
    
    
   
    
@endsection