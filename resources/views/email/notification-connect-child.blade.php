@extends('email.mailtemplate.cit')

@section('body')
    @if(isset($user))
        <h2 class="title">Hi {{$user->full_name}}</h2>
    @endif

	<p>This letter is to inform you that your child has interacted with another child. To view both parties to this
interaction, please click the below link for a one time viewing.

Kind Regards
MySafetyNet Team
		<p/>
    
    
  
    
@endsection