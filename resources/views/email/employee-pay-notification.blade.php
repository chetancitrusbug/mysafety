@extends('email.mailtemplate.cit')

@section('body')
    
    <h2 class="title">Hello @if($receiver_name == "") {{$user->full_name}} @else {{$receiver_name}} @endif  </h2>
	<p>
	@if($receiver_name == "")
		You are been paid for total {{ $transaction->for_user }} children with amount ${{ $transaction->unit }}.
	@else
	{{$user->full_name}} is been paid for total {{ $transaction->for_user }} children with amount ${{ $transaction->unit }}.

	@endif
	<br/>
	Clearing Note :- {{ $transaction->comment }}


    
@endsection

