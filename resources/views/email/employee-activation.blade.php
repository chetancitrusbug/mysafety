@extends('email.mailtemplate.cit')

@section('body')
    
    <h2 class="title">Hi {{$user->first_name}} {{$user->last_name}}   Welcome to {{ \config('app.name')}}  </h2>
	<p>
	Your {{ \config('app.name')}} employee account created succesfully, Please click on the below link to set your account password.
<br/>
<a href="{{url('employee/activation', $user->employee->email)}}/{{$user->employee->token}}">Set password and activate your {{ \config('app.name')}} employee account</a>
	</p>
    
	
	
    
    
    
@endsection

