@extends('email.mailtemplate.cit')

@section('body')
    @if(isset($user))
        <h2 class="title">Hi {{$user->full_name}}</h2>
    @endif

	MySafetyNet Parent,
	This letter is to acknowledge receipt of payment for the account of {{$child->full_name}} . Thank you for your
	payment. If you have any questions or concerns please email us at <a href="mailto:{{\config('admin.mail_to_admin')}}?subject={{\config('admin.mail_to_admin_defautl_subject')}}">{{\config('admin.mail_to_admin')}}</a>

	<br/>
    
    
    
    
@endsection