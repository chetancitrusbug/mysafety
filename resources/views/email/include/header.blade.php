	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>{{ config('app.name') }}</title>
		<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
		<style>
			body {
				font-family: 'Montserrat', sans-serif;
			}
			h2 {
				color:#000;
				font-size: 20px;
				text-align: center;
			}
			h4 {
				color: #333333;
				margin-left: 23px;
			}
			table {
				border-collapse: collapse;
				width: 95%;
				text-align: center;
				margin:0px auto 20px auto;
				border: 1px solid #ddd;
			}

			th, td {
				text-align: center;
				padding: 10px 20px;
				font-size: 15px;
			}

			tr:nth-child(odd){
                background-color: #f2f2f2;
            }

			th {
				background-color: #F47224;
				color: white;
			}
			
			p {
				margin:0px; 
				text-align:center; 
				font-size:15px;
			}
			table img {
				width: 80%;
			}
			img.brand_icon {
				width: 75px;
			}
			.footer a {
				text-align: center;
				color: #F47224;
				text-decoration: none;
			}
			.footer {
				text-align: center;
			}
			.footer p {
				margin-bottom: 10px;
			}
			table.buyer_detail th {
				background: transparent;
				border-right: 1px solid #ddd;
				color: #000;
			}
			table.buyer_detail td {
				text-align: left;
				padding-left: 50px;
			}
		
		</style>
	</head>
