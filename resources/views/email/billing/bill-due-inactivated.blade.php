@extends('email.mailtemplate.cit')

@section('body')
    
        <h2 class="title">Hi  {{ $order->parent->first_name}} {{$order->parent->last_name }} </h2>
		
		<p>We have not receive payment from subscription </p>
		<p>Your current subscription has been expired on {{ $order->expiry_date_formated }} </p>
		<p>Your child profile has been inactivated </p>
		



 <h4 style="margin-top: 15px;margin-bottom: 10px;">Subscription Detail</h4>
                                        <div class="table-responsive">
                                        <table class="buyer_detail email-table">
										
    <tr style="background-color:#f2f2f2" >
        <th>Child </th>
        <td>{{$order->child->full_name}}</td>
    </tr>
   
    <tr>
        <th>Subscription/Plan</th>
        <td>{{ $plan->title }} | @if($plan->type != "free") $ {{ $plan->actual_amount }}-@endif {{ strtoupper($plan->type) }}</td>
    </tr>
     
    <tr style="background-color:#f2f2f2">
        <th>Due Date</th>
        <td>{{ $order->expiry_date_formated }}</td>
    </tr>
	
	<tr>
        <th>Order Number</th>
        <td>{{$order->order_no}}</td>
    </tr>
	
	<tr style="background-color:#f2f2f2">
        <th>Payment Type</th>
        <td>@if($plan->type == "free") FREE @else STRIPE SUBSCRIPTION @endif</td>
    </tr>
    
	<tr>
        <th>Activation Status</th>
        <td>{{ strtoupper($order->trans_status) }} </td>
    </tr>
</table>

</div>

	<p style="margin:10px 0 0 0;">Please contact admin to restart services.</p>
    
@endsection