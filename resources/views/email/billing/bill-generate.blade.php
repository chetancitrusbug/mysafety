@extends('email.mailtemplate.cit')

@section('body')
    
        <h2 class="title">Hi  {{ $order->parent->first_name}} {{$order->parent->last_name }} </h2>
   

	<p>We have receive payment for following subscription.</p>
	
	<div class="table-responsive">
    <table class="buyer_detail email-table">
    <tr style="background-color:#f2f2f2">
        <th>Subscription</th>
		<th>Subscription Id</th>
        



    </tr>
    <tr>
        <td>{{ $plan->title }} | @if($plan->type != "free") $ {{ $plan->actual_amount }}-@endif {{ strtoupper($plan->type) }}</td>
		<td>{{ strtoupper($order->subscription_id) }}</td>
        
    </tr>
</table>
</div>
 
 <h4 style="margin-top: 20px;margin-bottom: 10px;">Child Detail</h4>
<div class="table-responsive">
  <table class="buyer_detail email-table">
    <tr style="background-color:#f2f2f2">
        <th>Name</th>
        <td>{{$order->child->full_name}} | {{$order->child->child_age}} Year</th>
    </tr>
    <tr>
        <th>Phone</th>
        <td>{{$order->child->phone}}</td>
    </tr>
    
</table>
</div>

<h4 style="margin-top: 20px;margin-bottom: 10px;">Payment Detail</h4>
                                        <div class="table-responsive">
                                        <table class="buyer_detail email-table">
    <tr style="background-color:#f2f2f2" >
        <th>Receive Amount</th>
        <td>$ {{$bill->amount_paid}}</td>
    </tr>
   
    <tr>
        <th>Billing Cycle</th>
        <td>{{$bill->start_date}} To {{$bill->end_date}}</td>
    </tr>
     
    <tr style="background-color:#f2f2f2">
        <th>Transaction Id</th>
        <td>{{$bill->charge_id}}</td>
    </tr>
    
</table>
</div>


		
    
   
@endsection