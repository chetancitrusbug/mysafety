@extends('email.mailtemplate.cit')

@section('body')
    @if(isset($user))
        <h2 class="title">Hi {{$user->full_name}}</h2>
    @endif

    <p>
        You have recently request to reset password  for MySafetyNet account.  <br/><br/>
        
		Your one time password (otp) to reset your password is : <b> {{$user->otp}} </b>
		
        <br/>
		<br/>
		
        
		If you didn't request this then ignor this email or let us know.
		
		<br/>
    </p>
    
    <hr>
    
@endsection