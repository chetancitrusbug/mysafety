@extends('email.mailtemplate.cit')

@section('body')
    
        <h2 class="title">Daily report {{ \Carbon\Carbon::now()->format("jS M, Y") }}</h2>
		
		
		
	
	<hr/>
@php($count =0)

<table class="buyer_detail">

	<tr  >
		<td>Type </td>
        <td>Order Id </td>
        <td>Mail Send To (Parent Email)</td>
    </tr>
	
@foreach($report as $k => $repo)
	
	<tr @if($count%2 == 0) style="background-color:#f2f2f2" @endif>
		<th colspan="3">
		@if($k == 'new_invoice')
			New Bill Generate For Subscription Notification
		@elseif($k == 'feature_invoice_mail')
			Feature Billing Notification
		@elseif($k == 'plan_upgrate_notification')
			Subscription Need To Upgrade Notification
		@elseif($k == 'plan_auto_upgrate_notification')
			Subscription Auto Upgrade From Free To Monthly	
		@elseif($k == 'payment_due_notification')
			Payment Not done (Due) Notification
		@elseif($k == 'payment_due_inactive_child')
			Child profile Inactivated Notification
		@else	
			{{$k}}
		@endif
		</th>
    </tr>
	
	
	
	@if(isset($repo['success']))
	@foreach($repo['success'] as $k => $re)
    <tr @if($count%2 == 0) style="background-color:#f2f2f2" @endif>
		<td>Success </td>
        <td>{{$re['id']}} </td>
        <td>{{$re['message']}}</td>
    </tr>
	
	@php($count =$count + 1)
	@endforeach
   @endif
   
   @if(isset($repo['warning']))
    @foreach($repo['warning'] as $k => $re)
    <tr @if($count%2 != 0) style="background-color:#f2f2f2" @endif>
		<td>Warning </td>
        <td>{{$re['id']}} </td>
        <td>{{$re['message']}}</td>
    </tr>
	@php($count =$count + 1)
	@endforeach
   @endif
   
  @if(isset($repo['error']))
   @foreach($repo['error'] as $k => $re)
    <tr @if($count%2 == 0) style="background-color:#f2f2f2" @endif>
		<td>Error </td>
        <td>{{$re['id']}} </td>
        <td>{{$re['message']}}</td>
    </tr>
	@php($count =$count + 1)
	@endforeach
   @endif



@endforeach
	
		</table>
    
    
    <hr>
    
@endsection