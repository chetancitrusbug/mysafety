@extends('email.mailtemplate.cit')

@section('body')
    @if(isset($user))
        <h2 class="title">Hi {{$user->full_name}}</h2>
    @endif

    <p>
MySafetyNet Parent,
We were unable to process our your payment for the account belonging to child’s name. To avoid
account suspension, please address any account or card changes. We will try to process payment in 5-7
business days. Thank You for your consideration in this matter.

Kind Regards
MySafetyNet Team
		<br/>
    
    
    <hr>
    
@endsection