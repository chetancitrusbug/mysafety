@extends('email.mailtemplate.cit')

@section('body')
    
    @if(isset($user))
        <h2 class="title">Hi {{$user->full_name}}</h2>
    @endif
    

	<b>Your child's profile has been Disapproved.</b>
	<hr/>
	
	Child Name :  {{$child->full_name}} <br/>
	Age : {{$child->child_age}} Year<br/>
	Username :  {{$child->username}} <br/>
	
	Phone : {{$child->phone}} <br/>
	<br/>
	
	@if($desc !="")
	Admin note : {{$desc}}
	@endif
	
    
@endsection

