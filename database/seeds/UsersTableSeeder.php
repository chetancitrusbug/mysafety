<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $admin = config('admin.admin_data');
        $users = new User();
        $users->name=$admin['name'];
        $users->email=$admin['email_id'];
        $users->password=bcrypt($admin['password']);
        $users->save();

        $role=Role::where('label','=','AU')->first();
        $users->roles()->attach($role->id);
    }
}
