<?php

use Illuminate\Database\Seeder;
use App\Setting;

class SettingPagination extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $setting = new Setting();
        $setting->key='pagination';
        $setting->value=10;
        $setting->save();
    }
}
