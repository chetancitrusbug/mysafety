<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('plan_id')->default(0)->nullable();
            $table->integer('order_id')->default(0)->nullable();
			$table->integer('user_id')->default(0)->nullable();
			
			$table->text('invoice_id')->nullable();
			$table->text('charge_id')->nullable();

            $table->float('amount_paid')->nullable()->default(0);
            $table->enum('price_currency', ['USD'])->default('USD');
            $table->enum('billing_type', ['paypal','stripe','trial'])->default('stripe');
            
           
			$table->longText('billing_ob')->nullable();
            $table->longText('package_ob')->nullable();

            $table->date('start_date')->nullable()->default(null);
            $table->date('end_date')->nullable()->default(null);
			
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing');
    }
}
