<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('user_id')->nullable();
            $table->string('email')->nullable();
            $table->string('token')->nullable();
            $table->integer('try_count')->nullable()->default(0);
            $table->integer('approve_count_total')->nullable()->default(0);
			$table->enum('is_setup', ['no','yes'])->default('no');
			$table->integer('approved_count')->nullable()->default(0);
			
			   $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee');
    }
}
