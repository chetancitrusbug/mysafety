<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClickFunnelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('click_funnel', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('click_funnel_id')->nullable()->default(0);
			$table->integer('child_id')->nullable()->default(0);
			$table->integer('parent_id')->nullable()->default(0);
			$table->integer('product_id')->nullable()->default(0);
			$table->float('product_price')->nullable()->default(0);
			$table->string('product_name')->nullable();
			$table->string('product_currency')->nullable();
			
			$table->integer('contact_id')->nullable()->default(0);
            $table->string('contact_first_name')->nullable();
            $table->string('contact_last_name')->nullable();
            $table->string('contact_name')->nullable();
            $table->string('contact_email')->nullable();
            $table->string('contact_phone')->nullable();
			$table->integer('contact_funnel_id')->nullable()->default(0);
            $table->string('stripe_customer_token')->nullable();
            $table->string('charge_id')->nullable();
            $table->string('payment_status')->nullable();
            $table->string('subscription_id')->nullable();
			
			$table->timestamp('create_date')->nullable();
            $table->string('event')->nullable();
			
			$table->longText('data_ob')->nullable();
			
            $table->softDeletes();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('click_funnel');
    }
}
