<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangesColumnTypeInPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("ALTER TABLE `plan` CHANGE `actual_amount` `actual_amount` FLOAT(8,2) NOT NULL DEFAULT '0'");
        \DB::statement("ALTER TABLE `plan` CHANGE `show_amount` `show_amount` FLOAT(8,2) NOT NULL DEFAULT '0'");

        Schema::table('plan', function (Blueprint $table) {
            $table->string('currency')->after('stripe_product_id')->default('USD');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("ALTER TABLE `plan` CHANGE `actual_amount` `actual_amount` FLOAT(8,2) NOT NULL DEFAULT '0'");
        \DB::statement("ALTER TABLE `plan` CHANGE `show_amount` `show_amount` FLOAT(8,2) NOT NULL DEFAULT '0'");

        Schema::table('plan', function (Blueprint $table) {
            $table->dropColumn('currency');
        });
    }
}
