<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBadgeApproveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('badge_approve', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('child_id')->nullable()->default(0);
			$table->integer('badge_approve_by')->nullable()->default(0);
			$table->text('checked_docs')->nullable();
			$table->enum('status', ['pending','approve'])->default('pending');
			$table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('badge_approve');
    }
}
