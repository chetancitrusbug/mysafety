<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_logs', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('user_id');
			$table->enum('unit_type', ['credit', 'debit','transfer'])->default('credit');
			$table->float('unit')->default(0)->comment("Unit amount");
			$table->enum('reference', ['self_debit','admin_credit','paypal_credit','daily_charge','admin_transfer']);
			$table->longText('comment')->nullable();
			$table->integer('created_by');
			$table->integer('updated_by');
            $table->integer('for_user')->default(0)->comment("for number of Child process");
            $table->longText('itemsobj')->nullable()->default(null);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_logs');
    }
}
