<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
			
			if(!\Schema::hasColumn("users", "phone_verified")){
				$table->boolean('phone_verified')->nullable()->default(0);
			}
			if(!\Schema::hasColumn("users", "badge_approve_by")){
				$table->integer('badge_approve_by')->nullable()->default(0);
			}
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
