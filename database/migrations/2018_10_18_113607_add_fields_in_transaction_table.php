<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsInTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('parent_id')->after('id');
            $table->integer('child_id')->after('parent_id');
            $table->integer('plan_id')->after('child_id');
            $table->date('start_date')->after('plan_id');
            $table->date('expiry_date')->after('start_date');
            $table->text('auth_response')->after('expiry_date');
            $table->string('trans_status')->after('amount');
            $table->string('next_pay')->after('trans_status')->nullable();
            $table->string('transaction_id')->after('next_pay')->nullable();
            $table->string('subscription_id')->after('transaction_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('parent_id');
            $table->dropColumn('child_id');
            $table->dropColumn('plan_id');
            $table->dropColumn('start_date');
            $table->dropColumn('expiry_date');
            $table->dropColumn('auth_response');
            $table->dropColumn('trans_status');
            $table->dropColumn('next_pay');
            $table->dropColumn('transaction_id');
            $table->dropColumn('subscription_id');
        });
    }
}
