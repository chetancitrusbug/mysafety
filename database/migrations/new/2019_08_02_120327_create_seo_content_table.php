<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeoContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seo_content', function (Blueprint $table) {
            $table->increments('id');
			$table->string('page_name')->nullable()->default("");
			$table->string('title')->nullable()->default(null);
			$table->longText('keywords')->nullable()->default(null);
			$table->longText('description')->nullable()->default(null);
			$table->string('author')->nullable()->default(null);
			$table->string('refresh')->nullable()->default(null);
			$table->string('type')->nullable()->default(null);
			$table->string('site_name')->nullable()->default(null);
			$table->string('url')->nullable()->default(null);
			$table->string('image')->nullable()->default(null);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seo_content');
    }
}
