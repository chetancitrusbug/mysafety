<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('first_name')->after('id')->nullable();
            $table->string('last_name')->after('first_name')->nullable();
            $table->integer('age')->after('dob')->nullable();
            $table->string('school_name')->after('batch_id')->nullable();
            $table->string('school_district_no')->after('school_name')->nullable();
            $table->string('state')->after('school_district_no')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('age');
            $table->dropColumn('school_name');
            $table->dropColumn('school_district_no');
            $table->dropColumn('state');
        });
    }
}
