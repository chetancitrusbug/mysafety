<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->increments('id');
			$table->enum('log_type', ['error'])->default('error');
			$table->string('status_code')->nullable()->default("");
			$table->integer('line_no')->nullable()->default(0);
			$table->text('file_name')->nullable();
			$table->integer('total_count')->default(1); 
			$table->string('slug')->nullable()->default("");
			$table->text('desc')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
