<?php

return [

	
	
	'responce_msg' => [
        'success_default' => 'Action Success !',
        'success_contact_request' => 'Thank you for your inquiry. A member of our support team will contact you soon.',
        'success_ebook_request' => 'Thank you for your inquiry. A member of our support team will contact you soon.',
        'success_complaint_request' => 'Thank you for your inquiry. A member of our support team will contact you soon.',
        'success_plan_upgration' => 'Success! You have upgrated subscription!',
        'success_document_upload' => 'Document uploaded Successfully.',
        'success_login' => 'Login Success.',
        'success_logout' => 'You have successfully logged out!',
        'success_forgot_password_send_to_email' => 'One time password (otp) to reset your password has been sent to your registered email address.',
        'success_forgot_password_send_to_phone_no' => 'One time password has been sent on your phone number.',
        'success_password_changed' => 'Success! Your Password has been changed!',
        'success_parent_register' => 'Thank you for your registration. please login and complete the registration process by adding your child and choosing a plan.',
        'success_parent_card_added_for_child' => 'Card added Successfully.',
        'success_parent_card_removed_for_child' => 'Card Removed Successfully.',
        'success_parent_card_updated_for_child' => 'Card Updated Successfully.',
        'success_parent_profile_updated' => 'Profile Updated Successfully.',
        'success_child_register_done' => "Success! Your child's profile is under verification!",
        'success_child_profile_update' => "Success! Profile updated!",
        'success_badge_request_sent' => "Success! Badge Request Sent!",
        'success_child_you_approve_badge_request' => "Success! Request approved!",
        'success_phone_verified' => "Your identity has been verified Successfully.",
		
		
        'warning_child_data_not_found' => 'User data not found .',
        'warning_user_data_not_found' => 'User data not found .',
        'warning_no_data_found' => 'No Record Found.',
        'warning_your_account_not_activated_yet' => 'Your account has not activated Yet.',
        'warning_incorrect_email_or_password' => 'Invalid email or password',
        'warning_require_unique_phone_number' => 'Phone number already exists with another account. Try with another number',
        'warning_require_unique_field' => ':field already exists with another account. Try with another number :field',
        'warning_require_different_phone_number' => 'Child phone number must be different than parent phone number',
        'warning_order_detail_not_found' => 'Orders detail not found.',
        'warning_require_unique_user_name' => 'Username already exists. please try another one.',
        'warning_user_not_found_for_given_badge' => 'User not found with given Badge id!',
		
		'error_default'=>'Something went wrong !',
		
        'mail_subject_upgrate_plan_success' => 'Mysafetynet subscription upgrated Successfully !',
        'mail_subject_stop_subscription_success' => 'Mysafetynet Unsubscription Successfully !',
        'mail_subject_parent_register_welcome' => 'MySafetyNet  Registration !',
        'mail_subject_child_register_success' => 'MySafetyNet Child Registration',
        'mail_subject_parent_reset_password' => 'Mysafetynet Reset password !',
        'mail_subject_parent_your_child_connected' => 'MySafetyNet child get connected',
        'mail_subject_contact_for_media_or_school' => 'MySafetyNet Contact media or schools',
        'mail_subject_contact_for_ebook' => 'MySafetyNet Request for eBook',
        'mail_subject_ebook' => 'MySafetyNet eBook',
		
		
		
        'mail_line1_parent_your_child_connected_with_other' => "This letter is to inform you that your child has interacted with another child. To view both parties to this
			interaction, please click the notification from MySafetyNet App.",
		
        'sms_child_you_receive_badge_request' => "You’ve just received a request to view your badge",
        'sms_child_you_just_request_for_badge' => "You just requested a badge view",
        'sms_parent_your_child_viewed_a_badge' => "Your child just viewed a badge, please check your Notification to review",
		
		//phone verification
		'verification_call_initiated' => "Verification call initiated.",
		'issue_to_connect_call' => "We are having difficulty contacting you or verifying the PIN you entered. you can retry the automated phone verification process or contact customer service to speak with a representative.",
        'enter_digit' => "Please answer the call and When Prompted, enter the 4-digit number from your phone keypad.",
		'we_have_initiated_verification_call' => "we have initiated a verification call",
		
    ],
	'phone_verification_data' => [
		
	],
	
	
	'meta_key_page' => [
        'default' => 'Default',
        'home' => 'Home',
		'contact' => 'Contact',
		'support' => 'Support',
		'anonymous-complaint' => 'Anonymous-complaint',
        'pricing' => 'Pricing',
		'privacy' => 'Privacy',
		'referral-program' => 'Referral-program',
		'blogs' => 'Blogs',
		'about' => 'About',
        'terms-conditions' => 'Terms-conditions',
        'secureaccess' => 'Secureaccess',
		'parent-signup' => 'Parent-signup',
		'parent-signin' => 'Parent-signin',
		'child-signin' => 'Child-signin',
        'child-forgot-password' => 'Child-forgot-password',
        'forgot-password' => 'Forgot-password',
        'parent' => 'Parent Profile',
        'parent-profile' => 'Parent Profile',
        'parent-add-child' => 'Add child',
        'parent-child' => 'Child List',
        'parent-child' => 'Child Detail',
        'parent-child-subscription' => 'Child subscription detail',
        'parent-child-documents' => 'Child document upload',
        'parent-orders' => 'Order listing',
        'parent-notification' => 'Child activity notifications',
        'parent-change-password' => 'Change-password',
        'parent-manage-stripe-account' => 'Manage stripe account',
        'parent-phone-verification' => 'Phone verification',
        'child' => 'Child profile',
        'child-badge' => 'Badge Request send',
        'child-request' => 'Badge Request list',
        'child-notification' => 'Notification on badge',
        'child-change-password' => 'Change password for child',
        
    ],
	
	
	

    
	
];
