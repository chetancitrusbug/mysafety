<?php

return [

	'mail_to_admin_cc' => env('mail_to_admin_cc', 'thomas@Mysafetynet.info'), //thomaskopec@hotmail.com
	'mail_to_admin' => env('mail_to_admin', 'accounting@mysafetynet.info'), 
	'mail_to_admin_support' => env('mail_to_admin_support', 'support@mysafetynet.info'), 
	'mail_to_ebook' => env('mail_to_ebook', 'thomas@Mysafetynet.info'), 
	
	'mail_to_admin_defautl_subject' => 'Mysafetynet query',
	'MAIL_SUBJECT_PREFIX' => env('MAIL_SUBJECT_PREFIX', ''),
	
	'WEB_URL' => env('WEB_URL', 'https://mysafetynet.info'),

	
    'admin_data' => [
        'name' => 'Admin',
        'email_id' => 'mysafety_admin@gmail.com',
        'password' => '123456'
    ],

    'plan_type'=>[
        'free'=>'Free',
        'monthly'=>'Monthly',
        'yearly'=>'Yearly',
		'weekly'=>'Weekly',
		'special_yearly'=>'Special Yearly',
		'special_lifetime'=>'Special Lifetime',
	],
	'responce_type'=>[
        'error'=>'Error',
        'success'=>'Success',
        'notification'=>'Notification',
    ],
	
    'page_type'=>[
        'page'=>'Page',
        'blog'=>'Blog',
    ],

    'date_format'=>[
        'd-m-Y'=>'DD-MM-YYYY',
        'm-d-Y'=>'MM-DD-YYYY',
        'Y-m-d'=>'YYYY-MM-DD',
    ],

    'setting'=>[
		'date_format_on_app' => 'm-d-Y',
	],

    'interval'=>[
        'yearly'=>'year',
        'monthly'=>'month',
        'daily'=>'day',
        'free'=>'day',
		'weekly'=>'week',
    ],
	
    'one_time_fee'=> 10,
    'free_trial_duration'=>183,
	
	'stripe_local_mayur' => [
        'SECRET_KEY'      => env('STRIPE_SECRET', 'sk_test_WuTaIxlor2gp8q4EeqHww6jb'),
        'PUBLISHABLE_KEY' => env('STRIPE_KEY', 'pk_test_K2ZO7DcZcQKuJ4PQjO9XOvRT')
    ],
	
	'stripe_stagg' => [
        'SECRET_KEY'      => env('STRIPE_SECRET', 'sk_test_rXL4jBy8qQYyquwRYhh4BjwH'),
        'PUBLISHABLE_KEY' => env('STRIPE_KEY', 'pk_test_iJLRAJToTtTWKSicsMj3YIhw')
    ],
	'stripe' => [
        'SECRET_KEY'      => env('STRIPE_SECRET', 'sk_live_t5Os9sgeJJNCtjEP3NzqmPOj'),
        'PUBLISHABLE_KEY' => env('STRIPE_KEY', 'pk_live_0KAGPmAFwdAh1rPRrHkDkCke')
    ],
	
	'stripe_citrus' => [
        'SECRET_KEY'      => 'sk_test_99gZgjwIk0zD8Feiuv8hFFPN',
        'PUBLISHABLE_KEY' => 'pk_test_7Barys4nP4dexRYSLTjN3gnI'
    ],
	
	'twilio' => [
        "SID" => "ACfbab16d53dea6ea5d2fb89397e937ca7",
		"TOKEN"=> "1eda2947ebc4a767716dbe8c397d2721",
		"FROM" => "+14342904443"
    ],
	
	
	
	
];
